#!/bin/bash

set -e


source ~/.bashrc

conda create -n cfm-env
conda activate cfm-env

conda install python=3.11

pip install numpy==1.26.4
pip install rdkit-pypi==2022.9.4
pip install cython
pip install pandas
pip install tqdm
pip install scipy
pip install matplotlib
pip install cirpy
pip install pubchempy
pip install orjson
pip install jsonpickle
pip install openpyxl
pip install requests
pip install pyopenms

pip install -I -e .