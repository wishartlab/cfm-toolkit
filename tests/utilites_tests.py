from cfmtoolkit.utils import *
import pytest

def test_get_min_mass_tol():
	#lower bound
	mass_tol = Utilities.get_min_mass_tol(100)
	assert mass_tol == 0.001

	#upper bound
	mass_tol = Utilities.get_min_mass_tol(10000)
	assert mass_tol == 0.01
	
	
def test_get_mass_tol():
	#lower bound
	mass_tol = Utilities.get_mass_tol(100)
	assert mass_tol == 0.01

	#upper bound
	mass_tol = Utilities.get_mass_tol(100, 0.1, 10)
	assert mass_tol == 0.1