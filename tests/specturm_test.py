from cfmtoolkit.datastructures import Spectrum, Peak
import pytest
'''

@pytest.fixture
def spectrum_one():
	adduct_type='[M+H]+'
	precursor_mz=10.00
	instrument_type='Q-TOF'
	ionization_type='ESI'
	charge_type='P'
	charge_count=1
	collision_energy=10
	peaks= [(1.0,2.0),(3.0,30.0),(100.0, 50)]
	is_nce=False
	data_source = 'NIST'
	data_source_id = '0'
	return Spectrum(adduct_type, 
						precursor_mz,
						instrument_type,
						ionization_type,
						charge_type,
						charge_count,
						collision_energy, 
						peaks,
						is_nce,
						data_source,
						data_source_id)


def test_spectrum_init(spectrum_one):
	assert spectrum_one.adduct_type == '[M+H]+'
	assert spectrum_one.precursor_mz == 10.00
	assert spectrum_one.instrument_type == 'Q-TOF'
	assert spectrum_one.ionization_type == 'ESI'
	assert spectrum_one.charge_type == 'P'
	assert spectrum_one.charge_count == 1
	assert spectrum_one.collision_energy == 10
	assert spectrum_one.is_nce == False
	assert spectrum_one.data_source == 'NIST'
	assert spectrum_one.data_source_id == '0'
	assert spectrum_one.peaks[0].mz == 1.0
	assert spectrum_one.peaks[0].intensity == 4.0
	assert spectrum_one.peaks[1].mz == 3.0
	assert spectrum_one.peaks[1].intensity == 60.0
	assert spectrum_one.peaks[2].mz == 100.0
	assert spectrum_one.peaks[2].intensity == 100.0

def test_spectrum_add_low_peak(spectrum_one):
	spectrum_one.add_peak(Peak(1.1, 2.0))
	assert spectrum_one.peaks[3].mz == 1.1
	assert spectrum_one.peaks[3].intensity == 2.0
	
	spectrum_one.normalize()
	assert spectrum_one.peaks[3].mz == 1.1
	assert spectrum_one.peaks[3].intensity == 2.0
	
	spectrum_one.sort_by_mz()
	assert spectrum_one.peaks[0].mz == 1.0
	assert spectrum_one.peaks[0].intensity == 4.0
	assert spectrum_one.peaks[1].mz == 1.1
	assert spectrum_one.peaks[1].intensity == 2.0
	assert spectrum_one.peaks[2].mz == 3.0
	assert spectrum_one.peaks[2].intensity == 60.0
	assert spectrum_one.peaks[3].mz == 100.0
	assert spectrum_one.peaks[3].intensity == 100.0
	
def test_spectrum_add_high_peak(spectrum_one):
	spectrum_one.add_peak(Peak(1.1, 200.0))
	assert spectrum_one.peaks[3].mz == 1.1
	assert spectrum_one.peaks[3].intensity == 200.0
	
	spectrum_one.normalize()
	spectrum_one.sort_by_mz()
	assert spectrum_one.peaks[0].mz == 1.0
	assert spectrum_one.peaks[0].intensity == 2.0
	assert spectrum_one.peaks[1].mz == 1.1
	assert spectrum_one.peaks[1].intensity == 100.0
	assert spectrum_one.peaks[2].mz == 3.0
	assert spectrum_one.peaks[2].intensity == 30.0
	assert spectrum_one.peaks[3].mz == 100.0
	assert spectrum_one.peaks[3].intensity == 50.0
'''