from cfmtoolkit.datastructures import Peak
import pytest

@pytest.fixture
def peaks():
	return [Peak(), Peak(1.0, 2.0), Peak(5.0, 6.0, 'TEST')]

def test_peak_init(peaks):
	assert peaks[0].mz == -1.0
	assert peaks[0].intensity == -1.0
	assert peaks[0].annotation == 'n/a'

	assert peaks[1].mz == 1.0
	assert peaks[1].intensity == 2.0
	assert peaks[1].annotation == 'n/a'

	assert peaks[2].mz == 5.0
	assert peaks[2].intensity == 6.0
	assert peaks[2].annotation == 'TEST'


def test_peak_annotate_with_mz(peaks):
	peaks[0].annotate_with_mz()
	assert peaks[0].annotation == ''
	peaks[1].annotate_with_mz()
	assert peaks[1].annotation == '1.000'
	peaks[2].annotate_with_mz()
	assert peaks[2].annotation == '5.000'
	
	