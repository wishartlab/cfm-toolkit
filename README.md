# README #

### What is this repository for? ###

* This is a toolkit for cfmid data processing
* For CFM-ID Machine Learning part, [Visit here](https://bitbucket.org/wishartlab/cfm-id-code/src/master/)
* For CFM-ID Rule Based part, [Visit here](https://bitbucket.org/wishartlab/msrb-fragmenter/src/master/)

### How do I get set up? ###

## Install Conda

[See here](https://www.anaconda.com/distribution/)

## Insatll Conda Build (Optional)

```bash
conda install conda-build
```

## Install PostgreSQL

[See here](https://www.postgresql.org/)

## Create a Virtual Environment and Install Dependencies

```bash
conda env create -f environment.yml
```

[See here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)

## Build Source Package

```
python setup.py sdist
```

## Build Binary Package[Optional]

```
python setup.py bdist_wheel
```

## Intall package in editable state

### Conda

```bash
conda develop .
```

## Install package via git

```
pip install git+https://7FeiW@bitbucket.org/wishartlab/cfm-toolkit.git#egg=cfmtoolkit
```
