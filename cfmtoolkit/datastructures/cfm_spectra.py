from typing import Dict, List, Optional

from cfmtoolkit.standardization import Standardization
from cfmtoolkit.datastructures import Spectrum

class CfmSpectra():
	_spectra_keys = ['low', 'med', 'high']

	def __init__(self, inchikey: str = Standardization.NA, smiles_or_inchi: str = Standardization.NA,
				 formula: str = Standardization.NA, compound_exact_mass: float = Standardization.NA,
				 adduct_type: str = Standardization.NA, ionization_type: str = Standardization.NA,
				 data_source: str = Standardization.NA, charge_type: str = Standardization.NA,
				 chromatography_type: str = Standardization.NA,
				 ms_level: str = Standardization.NA, compound_data_source: str = Standardization.NA,
				 compound_sample_frequency: float = -1, precursor_mz: float = Standardization.NA):
		self.inchikey = inchikey
	
		self.smiles_or_inchi = smiles_or_inchi
		self.formula = formula
		self.compound_exact_mass = compound_exact_mass
		self.adduct_type = adduct_type
		self.ionization_type = ionization_type
		self.data_source = data_source
		self.charge_type = charge_type
		self.chromatography_type = chromatography_type
		self.ms_level = ms_level
		self.compound_sample_frequency = compound_sample_frequency
		self.compound_data_source = compound_data_source
		self.spectra: dict[str, Spectrum] = {}
		self.precursor_mz = precursor_mz
		for key in self._spectra_keys:
			self.spectra[key] = Spectrum()

	@classmethod
	def get_spectra_keys(cls) -> List[str]:
		"""
		 Method to return a list of spectra keys
		"""
		return cls._spectra_keys

	@classmethod
	def default_ev_to_spectrum_key(cls, ev: float) -> str|None:
		"""
		Method to convert collision energy to spectrum key
		"""
		#print(type(ev))
		if ev == 10:
			return 'low'
		if ev == 20:
			return 'med'
		if ev == 40:
			return 'high'
		return None
	
	def add_spectrum(self, key: str, spectrum: Optional[Spectrum]):
		"""	Method to add spectrum to cfm spectra with a shadow copy
			   spectrum key must one of 'low','med','high'
		Args:
			key (str): spetrum key must one of 'low','med','high'
			spectrum (Spectrum): [description]

		Raises:
			Exception: [description]
		"""
		if key not in CfmSpectra._spectra_keys:
			raise Exception(f"spectra key must one of {CfmSpectra._spectra_keys}")
		self.spectra[key] = spectrum

	def get_spectra_as_list(self) -> List[Spectrum]:
		return [self.spectra['low'],
				self.spectra['med'],
				self.spectra['high']]

	def get_spectra(self) -> Dict:
		return self.spectra

	def get_spectrum(self, key) -> Optional[Spectrum]:
		if key not in CfmSpectra._spectra_keys:
			raise Exception(f"spectra key must one of {CfmSpectra._spectra_keys} not {key}")
		return self.spectra[key]

	def _copy_meta(self, other_spectra: 'CfmSpectra'):
		"""
			Method to copy meta data from give spectra
		Args:
			other_spectra (CfmSpectra): [description]
		"""
		self.inchikey = other_spectra.inchikey
		self.smiles_or_inchi = other_spectra.smiles_or_inchi
		self.formula = other_spectra.formula
		self.compound_exact_mass =  other_spectra.compound_exact_mass
		self.adduct_type =  other_spectra.adduct_type
		self.ionization_type =  other_spectra.ionization_type
		self.data_source =  other_spectra.data_source
		self.charge_type =  other_spectra.charge_type
		self.chromatography_type =  other_spectra.chromatography_type
		self.ms_level =  other_spectra.ms_level
		self.compound_sample_frequency =  other_spectra.compound_sample_frequency
		self.compound_data_source =  other_spectra.compound_data_source

	@staticmethod
	def union(base_spectra: 'CfmSpectra', additional_spectura: 'CfmSpectra',
			  intensity_aggregate_method='mean') -> 'CfmSpectra':
		"""
			Method that create new spectra by union two spectrums
			intensity_aggregate_method can be 'mean' , 'max' , or 'min'
		Returns:
			[type]: [description]
		"""
		merged_spectra = CfmSpectra()
		merged_spectra._copy_meta(base_spectra)
		for key in ['low', 'med', 'high']:
			merged_spectra.add_spectrum(key, Spectrum.union(base_spectra.spectra[key],
															additional_spectura.spectra[key],
															intensity_aggregate_method))

		return merged_spectra

	@staticmethod
	def intersect(base_spectra: 'CfmSpectra', additional_spectura: 'CfmSpectra',
				  intensity_aggregate_method='mean') -> 'CfmSpectra':
		"""
		Method that create new spectra by union two spectrums
		intensity_aggregate_method can be 'mean' , 'max' , or 'min'
		"""
		merged_spectra = CfmSpectra()
		merged_spectra._copy_meta(base_spectra)
		for key in ['low', 'med', 'high']:
			merged_spectra.add_spectrum(key, Spectrum.intersect(base_spectra.spectra[key],
																additional_spectura.spectra[key],
																intensity_aggregate_method))

		return merged_spectra
		# return  Spectrum._merged_spectrum(base_spectrum, additional_spectrum,'intersect', intensity_aggregate_method)

	def get_cfm_text_str(self, annotation_right: bool = False) -> str:
		"""
		Method export data to cfm txt input format
		"""
		ms_str = self.ionization_type
		if self.ms_level == Standardization.MS1:
			ms_str += '-MS'
		if self.ms_level == Standardization.MS2:
			ms_str += '-MS/MS'
		ms_str += ' ' + self.adduct_type

		if self.data_source.startswith('CFM-ID'):
			string = '#In-silico ' + ms_str + ' Spectra\n'
		else:
			string = '#' + ms_str + ' Spectra\n'

		if self.data_source.startswith('CFM-ID'):
			string += '#PREDICTED BY ' + self.data_source + '\n'
		else:
			string += '#DATASOURCE: ' + self.data_source + '\n'
		
		string += '#ID=' + self.compound_data_source + '\n'

		if self.smiles_or_inchi.startswith('InChI='):
			string += '#' + self.smiles_or_inchi + '\n'
		else:
			string += '#SMILES=' + self.smiles_or_inchi + '\n'
		string += '#InChiKey=' + self.inchikey + '\n'
		string += '#Formula=' + self.formula + '\n'
		if self.precursor_mz != Standardization.NA:
			string += '#PMass='+ str(self.precursor_mz) + '\n'
		string += '#ChargeType='+self.charge_type + '\n'
		string += 'energy0\n' + self.spectra['low'].get_cfm_text_str(annotation_right) + \
				  'energy1\n' + self.spectra['med'].get_cfm_text_str(annotation_right) + \
				  'energy2\n' + self.spectra['high'].get_cfm_text_str(annotation_right)
		# print(string)
		return string

	def annotate_with_mz(self, intensity_threshold = 0.0):
		"""
		Method annotate peak with m/z
		"""
		for key_world in self.spectra:
			if self.spectra[key_world] is not None:
				self.spectra[key_world].annotate_with_mz(intensity_threshold)

	def get_max_mz(self) -> float:
		"""
		Method get max m/z across all spectra
		"""
		max_mz = 0.0
		for key_world in self.spectra:
			max_mz = max(max_mz, self.spectra[key_world].max_mz)

		return max_mz

	def __str__(self):
		"""
		Method export data to cfm txt input format
		"""
		string = 'Low Energy Spectrum \n' + self.spectra['low'].to_string() + \
				 '\nMed Energy Spectrum\n' + self.spectra['med'].to_string() + \
				 '\nHigh Energy Spectrum\n' + self.spectra['high'].to_string()
		# print(string)
		return string

	def set_compound_sample_frequency(self, compound_sample_frequency):
		self.compound_sample_frequency = compound_sample_frequency
		for key_world in self.spectra:
			self.spectra[key_world].compound_sample_frequency = compound_sample_frequency

	def set_compound_data_source(self, compound_data_source):
		self.compound_data_source = compound_data_source
		for key_world in self.spectra:
			self.spectra[key_world].compound_data_source = compound_data_source

	def set_data_source(self, data_source):
		self.data_source = data_source
		for key_world in self.spectra:
			self.spectra[key_world].data_source = data_source

	def fill_meta(self, spectrum: Spectrum):
		self.inchikey = spectrum.inchikey
		self.smiles_or_inchi = spectrum.smiles_or_inchi
		self.formula = spectrum.formula
		self.compound_exact_mass = spectrum.compound_exact_mass
		self.adduct_type = spectrum.adduct_type
		self.ionization_type = spectrum.ionization_type
		self.data_source = spectrum.data_source
		self.charge_type = spectrum.charge_type
		self.chromatography_type = spectrum.chromatography_type
		self.ms_level = spectrum.ms_level
		self.compound_sample_frequency = spectrum.compound_sample_frequency
		self.compound_data_source = spectrum.compound_data_source
		self.precursor_mz = spectrum.precursor_mz

	def convert_intensity_to_log_scale(self):
		for key_world in self.spectra:
			self.spectra[key_world].convert_intensity_to_log_scale()

	def convert_intensity_to_linear_scale(self):
		for key_world in self.spectra:
			self.spectra[key_world].convert_intensity_to_linear_scale()

	def remove_noise(self, intensity_threshold=1.0, centroid=False, mz_distance=0.1, remove_noise_more_than_pmass = False):
		for key_world in self.spectra:
			self.spectra[key_world].remove_noise(intensity_threshold,centroid, mz_distance, remove_noise_more_than_pmass)
			#print(self.spectra[key_world])

	def remove_noise_cfm(self, remaining_total_intensity_ratio=0.8, centroid=False, mz_distance=0.1):
		for key_world in self.spectra:
			self.spectra[key_world].remove_noise_cfm(remaining_total_intensity_ratio, centroid, mz_distance)

	
	def normalize(self):
		for key_world in self.spectra:
			self.spectra[key_world].normalize()

	def get_nl_spectra(self) -> 'CfmSpectra':
		nl_spectra = CfmSpectra() 
		nl_spectra._copy_meta(self)
		nl_spectra.adduct_type = 'Neutral-Loss-' + nl_spectra.adduct_type
		for key_world in self.spectra:
			nl_spectra.spectra[key_world] = self.spectra[key_world].get_nl_spectrum()
		return nl_spectra

	def has_peak_at(self,mz, ppm_tol = 10, abs_tol = 0.01) -> bool:
		"""
		Method get max m/z across all spectra
		"""
		has_peak = False
		for key_world in self.spectra:
			has_peak = has_peak or self.spectra[key_world].has_peak_at(mz, ppm_tol, abs_tol)

		return has_peak
	
	def create_merged_spectrum(self, keys:Optional[List] = None) -> Spectrum:
		if keys is None:
			keys = self._spectra_keys

		merged_spectrum = self.spectra[keys[0]]
		if len(keys) == 1:
			return merged_spectrum
		for spectrum_key in keys[1:]:
			merged_spectrum = Spectrum.merge_spectrum(merged_spectrum, self.spectra[spectrum_key], normalize=False)
		merged_spectrum.normalize()
		merged_spectrum.collision_energy = '10,20,40eV'
		merged_spectrum.collision_energy_unit = 'merged'
		merged_spectrum.compound_sample_frequency = self.compound_sample_frequency
		return merged_spectrum

	def is_complete(self) -> bool:
		has_peaks = True
		for key_world in self.spectra:
			has_peaks = has_peaks and (self.spectra[key_world].get_num_peaks() > 0)
		return has_peaks

	def has_peaks(self) -> bool:
		has_peaks = False
		for key_world in self.spectra:
			has_peaks = has_peaks or (self.spectra[key_world].get_num_peaks() > 0)
		return has_peaks

	def rescale_peaks(self, scale_factor: float):
		for key_world in self.spectra:
			self.spectra[key_world].rescale_peaks(scale_factor)

	def get_dot_ms_str(self) -> str:
		dot_ms_str = ">compound {}\n".format(self.inchikey)
		for key_world in self.spectra:
			if self.spectra[key_world] is not None:
				dot_ms_str += self.spectra[key_world].get_dot_ms_str()
		dot_ms_str +="\n"
		return dot_ms_str

	def get_mgf_str(self) -> str:
		mgf_str = ""
		for key_world in self.spectra:
			mgf_str += self.spectra[key_world].get_mgf_str(self.inchikey)
		return mgf_str

	@classmethod
	def merge_cfm_spectra(cls, cfm_spectra: "CfmSpectra", other_cfm_spectra: "CfmSpectra", merge_method: str = 'union', intensity_aggregate_method: str = 'sum', normalize=True) -> "CfmSpectra":
		merged_cfm_spectra = CfmSpectra()
		for key_world in cls._spectra_keys:
			 merged_cfm_spectra.spectra[key_world] = Spectrum.merge_spectrum(cfm_spectra.spectra[key_world], other_cfm_spectra.spectra[key_world], merge_method, intensity_aggregate_method, normalize)
		merged_cfm_spectra._copy_meta(cfm_spectra)

		return merged_cfm_spectra


	@classmethod
	def merge_cfm_spectra_list(cls, cfm_spectra_list: List["CfmSpectra"], merge_method: str = 'union', intensity_aggregate_method: str = 'sum') -> "CfmSpectra":
		merged_cfm_spectra = CfmSpectra()
		for key_world in cls._spectra_keys:
			 merged_cfm_spectra.spectra[key_world] = Spectrum.merge_multi_spectrum([s.spectra[key_world] for s in cfm_spectra_list], merge_method, intensity_aggregate_method)
		#merged_cfm_spectra._copy_meta(cfm_spectra)

		return merged_cfm_spectra			
		
	def compute_precursor_mz(self):
		for key_world in self.spectra:
			self.spectra[key_world].compute_precursor_mz()
		self.precursor_mz = self.spectra[self._spectra_keys[0]].precursor_mz
	
	def round_spectra(self, decimal_place = 4, agg = 'sum'):
		for key_world in self.spectra:
			self.spectra[key_world].round_spectrum(decimal_place, agg)
	
	def to_prez_only_specrta(self):
		"""set current spectra to prez only specta
		"""
		for key_world in self.spectra:
			self.spectra[key_world].to_prez_only_spectrum()
	
	def to_random_specrta(self, num_peaks_min=1, num_peaks_max = 20, rng = None):
		"""set current spectra to spectra

		Args:
			num_peaks_min (int, optional): _description_. Defaults to 1.
			num_peaks_max (int, optional): _description_. Defaults to 20.
		"""
		for key_world in self.spectra:
			self.spectra[key_world].to_random_spectrum(num_peaks_min = num_peaks_min, num_peaks_max = num_peaks_max, rng = rng)
			
	def to_random_picked_spectra(self, selections, num_peaks_min=1, num_peaks_max = 20, rng = None):
		"""set current spectra to random spectra

		Args:
			num_peaks_min (int, optional): _description_. Defaults to 1.
			num_peaks_max (int, optional): _description_. Defaults to 20.
		"""
		for key_world in self.spectra:
			self.spectra[key_world].to_random_picked_spectrum(selections, num_peaks_min = num_peaks_min, num_peaks_max = num_peaks_max, rng = rng)