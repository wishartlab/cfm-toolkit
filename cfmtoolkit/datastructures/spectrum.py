import os
import time
from typing import List, Tuple, Optional

import numpy as np

from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities
import orjson
import uuid

class Spectrum(object):
	"""
	class for a mass spectrum
	"""
	_supported_intensity_aggregate_method = ['sum', 'max']
	_supported_spectrum_merge_method = ['union', 'intersect']
	_peak_mtype = [('mz','d'), ('intensity','d'),('annotation','object')]
	_peak_mtype_lite = [('mz','d'), ('intensity','d')]

	def __init__(self):
		# default value to empty and values
		# jsonb does not like null
		#self.peaks: List[Peak] = []
		self.peaks = None
		self.annotations = None
		self.is_normalized: bool = False
		self.is_sorted: bool = False
		self.chromatography_type: str = Standardization.NA
		self.adduct_type: str = Standardization.NA
		self.analyzer_type: str = Standardization.NA
		self.ionization_type: str = Standardization.NA
		self.charge_type: str = Standardization.NA
		self.ms_level: str = Standardization.NA

		self.collision_energy_prefix: str = Standardization.NA
		self.collision_energy_unit: str = Standardization.NA
		self.collision_method: str = Standardization.NA
		self.collision_gas_type: str = Standardization.NA
		self.instrument_model: str = Standardization.NA

		self.inchikey: str = Standardization.NA
		self.formula: str = Standardization.NA
		self.compound_name: str = Standardization.NA
		self.smiles_or_inchi: str = Standardization.NA

		self.data_source = Standardization.NA
		self.data_source_id = Standardization.NA
		self.compound_data_source = Standardization.NA
		self.compound_data_source_id = Standardization.NA

		self.mz_decimal_place: int = 0
		self.precursor_mz: float = -1.0
		self.compound_exact_mass: float = -1.0  # Monoisotopic mass
		self.collision_energy: float = -1.0

		self.compound_sample_frequency = -1.0

		#For EI-MS
		self.derivatization_type = Standardization.NA
		self.derivatization_mass = -1.0
		# self.equivalent_qtof_collision_energy: float = -1.0
		self.cfm2_normalized = False
		
		self.rt:float = -1.0
		self.rt_unit:str = Standardization.NA
		self.splash:str = Standardization.NA

	@property
	def min_mz(self):
		if self.peaks is not None:
			return np.min(self.peaks['mz'])
		return -1.0
	
	@property
	def max_mz(self):
		if self.peaks is not None and self.peaks.size > 0:
			return np.max(self.peaks['mz'])
		elif self.peaks is not None and self.peaks.size == 0:
			return 0.0
		return -1.0
	
	@property
	def min_intensity(self):
		if self.peaks is not None:
			return np.min(self.peaks['intensity'])
		return -1.0
	
	@property
	def max_intensity(self):
		if self.peaks is not None:
			return np.max(self.peaks['intensity'])
		return -1.0

	@property
	def peaks_count(self):
		if self.peaks is not None:
			return len(self.peaks)
		return 0
	
	def reconstruct_peaks(self, peak_data: List):
		"""reconstruct peaks from database query result

		Args:
			peak_data (List): [description]
		"""
		if peak_data is None or len(peak_data) == 0:
			print(f"empty peak data for inchikey: {self.inchikey}")
			return

		self.peaks = np.empty(len(peak_data), dtype=self._peak_mtype)
		for idx, peak_dict in enumerate(peak_data):
		   self.peaks[idx]['mz'] = float(peak_dict['mz'])
		   self.peaks[idx]['intensity'] = float(peak_dict['intensity'])
		   self.peaks[idx]['annotation'] = peak_dict['annotation']
		
		#self.min_intensity = np.min(self.peaks['intensity'])
		#self.max_intensity = np.max(self.peaks['intensity'])
		self.post_process()

	def reconstruct_peaks_from_np(self, np_peaks:np.ndarray):
		#self.peaks_count = len(np_peaks)
		self.peaks = np_peaks

		#self.min_intensity = np.min(self.peaks['intensity'])
		#self.max_intensity = np.max(self.peaks['intensity'])
		self.post_process()

	def reconstruct_peaks_json_str(self, json_str):
		data_list = orjson.loads(json_str)

		num_peaks = len(data_list[0])		
		peaks = np.empty((num_peaks), dtype = self._peak_mtype)
		peaks["mz"] = data_list[0]
		peaks["intensity"] = data_list[1]
		peaks["annotation"] = [x if x != '' else Standardization.NA for x in data_list[2]]
			
		self.peaks = peaks

	def get_peak_as_json_str(self, omit_annotation_na = True):
		"""_summary_

		Args:
			omit_annotation_na (bool, optional): _description_. Defaults to True.

		Returns:
			_type_: _description_
		"""
		if not omit_annotation_na:
			data = [self.peaks['mz'].tolist(), self.peaks['intensity'].tolist(), self.peaks['annotation'].tolist()]
		else:
			data = [self.peaks['mz'].tolist(), 
					self.peaks['intensity'].tolist(),
					[ x if x != Standardization.NA else '' for x in self.peaks['annotation'].tolist() ]
			]
		json_str = orjson.dumps(data)
		return json_str

	def add_peak(self, mz: float, intensity: float, annotation: str = Standardization.NA):
		""" add a peak

		Args:
			mz (float): _description_
			intensity (float): _description_
			annotation (str, optional): _description_. Defaults to Standardization.NA.
		"""

		if self.peaks is None:
			self.peaks = np.array([(mz, intensity, annotation)], dtype = self._peak_mtype)
		else:
			self.peaks = np.append(self.peaks, np.array((mz, intensity, annotation), dtype=self._peak_mtype))

		self.is_normalized = False
		self.is_sorted = False

	def add_peaks(self, mz_array: List[float], intensity_array: List[float], annotation_array:Optional[List[str]] = None,
		min_mz=None, max_mz=None, replace_annotation_na = True):

		if annotation_array is None:
			annotation_array = [Standardization.NA for _ in mz_array]
		
		valid_peak_list = []
		for mz, intensity,annotation in zip(mz_array, intensity_array, annotation_array):
			if min_mz is not None and mz < min_mz:
				continue
			if max_mz is not None and mz > max_mz:
				continue
			if annotation == "" and replace_annotation_na:
				annotation = Standardization.NA
			if intensity > 0:
				valid_peak_list.append((mz, intensity,annotation))
		if len(valid_peak_list):
			self.add_peaks_from_peak_list(valid_peak_list)

	def add_peaks_from_peak_list(self, peak_list: List[Tuple[float,float,str]]):
		"""add_peaks_from_peak_list
		Args:
			peak_list (List[Tuple): _description_
		"""
		if len(peak_list) == 0:
			return
		
		np_peak_list = np.array(peak_list, dtype=self._peak_mtype)
		if self.peaks is None:
			self.peaks = np_peak_list
		else:
			self.peaks = np.append(self.peaks, np_peak_list)

		self.is_normalized = False
		self.is_sorted = False

	def normalize(self, scale=100.0, cfm_style=False):
		"""	
		Method to normalize spectra
		normalized to 100 based intensity

		Args:
			scale (float, optional): [description]. Defaults to 100.0.
			cfm_style (bool, optional): [description]. cfm2 style normalize Defaults to False. 
		"""
		if type(self.peaks).__module__ == np.__name__ and len(self.peaks) > 0:
			divisor = self.max_intensity if not cfm_style else np.sum(self.peaks['intensity'])
			divisor /= scale
			self.cfm2_normalized = cfm_style
			self.peaks['intensity'] /= divisor

			self.is_normalized = True

	def sort_by_mz(self):
		"""Method to sort peaks by mass
		"""
		if self.peaks is None:
			return
		try:
			self.peaks = np.sort(self.peaks, order=['mz'])
			self.is_sorted = True
		except Exception as e:
			print("error whilesort_by_mz", e)
			raise

	def remove_noise(self, intensity_threshold=1.0, centroid=False, 
					centroid_mz_distance=0.1, remove_noise_more_than_pmass = True):
		""" 
			Method to remove noise
		Args:
			intensity_threshold (float, optional): [description]. Defaults to 1.0.
			cfm_style (bool, optional): [description]. Defaults to False.
			centroid (bool, optional): [description]. Defaults to False.
			centroid_mz_distance (float, optional): [description]. Defaults to 0.1.
		"""
		if self.peaks is None:
			return
		
		if not self.is_sorted:
			self.sort_by_mz()

		if not self.is_normalized:
			self.normalize(cfm_style=self.cfm2_normalized)

		self._remove_low_intensity(intensity_threshold=intensity_threshold)

		if remove_noise_more_than_pmass and self.precursor_mz not in [None, Standardization.NA, -1]:
			self.peaks = self.peaks[self.peaks['mz'] < self.precursor_mz + 0.1]

		if centroid:
			self._centroid(mz_distance=centroid_mz_distance)
			self.sort_by_mz()

		# we need to normalize again
		if self.cfm2_normalized:
			self.normalize(cfm_style=self.cfm2_normalized)
	
	def remove_noise_cfm(self, remaining_total_intensity_ratio=0.8, centroid=False, 
					centroid_mz_distance=0.1, remove_noise_more_than_pmass = True):
		""" 
			Method to remove noise using cfm style
			remvoe noise based on total intensity instead of intensity_threshold
		Args:
			intensity_threshold (float, optional): [description]. Defaults to 1.0.
			cfm_style (bool, optional): [description]. Defaults to False.
			centroid (bool, optional): [description]. Defaults to False.
			centroid_mz_distance (float, optional): [description]. Defaults to 0.1.
		"""
		if not self.is_sorted:
			self.sort_by_mz()

		if not self.is_normalized:
			self.normalize(False)

		self._remove_low_intensity_cfm(remaining_total_intensity_ratio = remaining_total_intensity_ratio)

		if remove_noise_more_than_pmass and self.precursor_mz is not None and self.precursor_mz != Standardization.NA:
			self.peaks = self.peaks[self.peaks['mz'] < self.precursor_mz + 0.1]

		if centroid:
			self._centroid(mz_distance=centroid_mz_distance)
			self.sort_by_mz()

		# we may need to normalize again
		self.normalize(cfm_style=True)

	def _remove_low_intensity_cfm(self, remaining_total_intensity_ratio = 0.8):
		"""Method to remove low intensity peak, intensity_threshold is measued in percent
		Args:
			intensity_threshold (float, optional): [description]. Defaults to 1.0.
		"""
		if not self.is_sorted:
			self.sort_by_mz()
		if not self.is_normalized:
			self.normalize()

		wanted_total_intensity = np.sum(self.peaks['intensity']) * remaining_total_intensity_ratio
		current_intensity_sum = 0

		cut_off = len(self.peak)
		while cut_off >= 0:
			current_intensity_sum += self.peaks[cut_off][1]
			if wanted_total_intensity > current_intensity_sum:
				break
		self.peaks = self.peaks[cut_off:]

	def _remove_low_intensity(self, intensity_threshold=1.0):
		"""Method to remove low intensity peak, intensity_threshold is measued in percent
		Args:
			intensity_threshold (float, optional): [description]. Defaults to 1.0.
		"""
		if self.peaks is not None:
			self.peaks = self.peaks[self.peaks['intensity'] >= intensity_threshold]

	def _centroid(self, mz_distance=0.1):
		"""	Method that remove duplicated peaks within given mz_distance
		Args:
			mz_distance (float, optional): [description]. Defaults to 0.1.
		"""
		if self.peaks is not None:
			saved_peaks_index = self._get_centroid_spectrum_idx(0, len(self.peaks) - 1, mz_distance)
			self.peaks = self.peaks[saved_peaks_index]

	def _get_centroid_spectrum_idx(self, mz_lbound_index, mz_upbond_index, mz_distance=0.1):
		if mz_lbound_index > mz_upbond_index:
			return []

		if mz_lbound_index == mz_upbond_index:
			# print(lower_bond, self.peaks[lower_bond].mz, self.peaks[lower_bond].intensity)
			return [mz_lbound_index]

		# get current highest peak idx
		index = mz_lbound_index + np.argmax(self.peaks['intensity'][mz_lbound_index:mz_upbond_index + 1])
		current_mass = self.peaks['mz'][index]

		saved_peaks_index = [index]

		# left part upper bond
		ls_upper_bond = index - 1
		# if ls_upper_bond > 0:
		while ls_upper_bond > -1:
			if self.peaks['mz'][ls_upper_bond] > current_mass - mz_distance:
				ls_upper_bond -= 1
			else:
				break
			
		# print('ls_upper_bond',ls_upper_bond)
		if ls_upper_bond > -1:
			left = self._get_centroid_spectrum_idx(mz_lbound_index, ls_upper_bond, mz_distance)
			saved_peaks_index += left

		# right part lower bond
		rs_lower_bound = index + 1
		while rs_lower_bound < len(self.peaks):
			if self.peaks['mz'][rs_lower_bound] < current_mass + mz_distance:
				rs_lower_bound += 1
			else:
				break
		if rs_lower_bound < len(self.peaks):
			right = self._get_centroid_spectrum_idx(rs_lower_bound, mz_upbond_index, mz_distance)
			saved_peaks_index += right

		return saved_peaks_index

	def __str__(self):
		return self.to_string()

	def _get_meta_string(self) -> str:
		string = 'smiles_or_inchi: ' + str(self.smiles_or_inchi) + '\n' + \
				 'Inchikey: ' + str(self.inchikey) + '\n' + \
				 'precursor_mz: ' + str(self.precursor_mz) + '\n' + \
				 'compound_exact_mass: ' + str(self.compound_exact_mass) + '\n' + \
				 'is_normalized: ' + str(self.is_normalized) + '\n' + \
				 'is_sorted: ' + str(self.is_sorted) + '\n' + \
				 'chromatography_type: ' + str(self.chromatography_type) + '\n' + \
				 'adduct_type: ' + str(self.adduct_type) + '\n' + \
				 'analyzer_type: ' + str(self.analyzer_type) + '\n' + \
				 'ionization_type: ' + str(self.ionization_type) + '\n' + \
				 'charge_type: ' + str(self.charge_type) + '\n' + \
				 'ms_level: ' + str(self.ms_level) + '\n' + \
				 'collision_energy: ' + str(self.collision_energy) + '\n' + \
				 'collision_method: ' + str(self.collision_method) + '\n' + \
				 'collision_gas_type: ' + str(self.collision_gas_type) + '\n' + \
				 'instrument_model: ' + str(self.instrument_model) + '\n' + \
				 'mz_decimal_place: ' + str(self.mz_decimal_place) + '\n' + \
				 'data_source: ' + str(self.data_source) + '\n' + \
				 'data_source_id: ' + str(self.data_source_id) + '\n' + \
				 'compound_data_source: ' + str(self.compound_data_source) + '\n' + \
				 'compound_data_source_id: ' + str(self.compound_data_source_id) + '\n' + \
				 'compound_sample_frequency: ' + str(self.compound_sample_frequency) + '\n' + \
				 'derivatization_type: ' + self.derivatization_type + '\n' + \
				 'derivatization_mass: ' + str(self.derivatization_mass) + '\n' + \
				 'peaks_count: ' + str(self.peaks_count) + '\n' +\
				 'rt: ' + str(self.rt) + ' ' +  str(self.rt_unit) + '\n'
		return string

	def to_string(self, simplified=False) -> str:
		string = ''
		if simplified:
			string += "#" + str(self.ionization_type) + ' ' + str(self.adduct_type) + ' ' + str(self.ms_level) + '\n'
			if self.ms_level == Standardization.MS2:
				string += "#" + str(self.analyzer_type) + '@' + str(self.collision_energy) + str(
					self.collision_energy_unit) + '\n'
			string += "#Inchikey: " + str(self.inchikey) + '\n'
			if self.smiles_or_inchi.startswith('InChI'):
				string += '#' + str(self.smiles_or_inchi) + '\n'
			else:
				string += '#Smiles: ' + str(self.smiles_or_inchi) + '\n'
			string += '#PrecursorMz: ' + str(self.precursor_mz) + '\n'
			string += '#DataSource: ' + str(self.data_source) + '\n'
			string += '#DataSourceID: ' + str(self.data_source_id) + '\n'
			string += '#CompoundDataSource: ' + str(self.compound_data_source) 
			if self.rt != -1:
				string += f'#RT: {self.rt} {self.rt_unit}\n'
			string += '\n\n'

		else:
			string += self._get_meta_string() + 'peaks:\n'

		if self.peaks is not None:
			for peak in self.peaks:
				if not simplified:
					string +=  "mz:{:.5f} intensity:{:.2f} annotation:{}\n".format(peak[0],peak[1], peak[2])
				else:
					string +=  "{:.5f} {:.2f}\n".format(peak[0],peak[1])
	
		return string

	def get_summary(self, include_inchikey=True) -> str:
		"""	Method Return spectrum summary

		Args:
			include_inchikey (bool, optional): [description]. Defaults to True.

		Returns:
			str: [description]
		"""
		string = ""
		if include_inchikey:
			string = str(self.inchikey) + '\n'

		string += str(self.chromatography_type) + '-' + str(self.ionization_type) + '-' + str(
			self.analyzer_type) + '-' + str(self.adduct_type) + '@' + \
				  str(self.collision_energy) + str(self.collision_energy_unit) + '\n'

		# if include_equivalent_qtof_cid:
		#	string += 'Equivalent QTOF CID: {:.1f} eV \n'.format(
		#		self.equivalent_qtof_collision_energy)

		string += 'Source: {}-{}'.format(self.data_source, self.data_source_id)
		return string

	def get_spectrum_compound_summary(self) -> str:
		"""
		Returns:
			str: Method Return spectrum compund summary
		"""
		string = "Name: {} \nInchIKey: {} \n".format(
			self.compound_name, self.inchikey)
		return string

	def get_spectrum_type_summary(self) -> str:
		"""	Method Return spectrum type summary
		Returns:
			str: [description]
		"""
		ms_level = "MS/MS" if self.ms_level == Standardization.MS2 else "MS"

		string = "{} {}@{}".format(self.data_source, ms_level,
										 str(self.collision_energy) + str(self.collision_energy_unit))

		return string

	def get_collision_energy_string(self) -> str:
		""" 
			Method Return collision energy string
		Returns:
			str: [description]
		"""
		return str(self.collision_energy) + str(self.collision_energy_unit)

	def set_mz_decimal_place(self):
		"""
		Method to set mz_decimal_place
		"""
		if self.peaks is None:
			return
		
		for peak in self.peaks:
			mass_str = str(peak[0])

			if mass_str.startswith('.'):
				curr_mz_decimal_place = len(mass_str) - 1
			else:
				data = mass_str.split('.')
				if len(data) == 1:
					curr_mz_decimal_place = 0
				else:
					curr_mz_decimal_place = len(data[1])
			if curr_mz_decimal_place > self.mz_decimal_place:
				self.mz_decimal_place = curr_mz_decimal_place

	def annotate_with_mz(self, intensity_threshold=0.0):
		"""Method annotate peak with m/z
		"""
		for idx, _ in enumerate(self.peaks):
			if self.peaks[idx][1] >= intensity_threshold:
				self.peaks[idx][2] = '{:.3f}'.format(self.peaks[idx][0])
			else:
				self.peaks[idx][2] = ''

	def post_process(self, cfm_style=False):
		"""
			Run all post process
			NOTE when cfm_style normalization enabled, noise remove will also use the same style spectrum
			You may want to remove noise on a regular normalization and then transfer to cfm style spectrum

		Args:
			cfm_style (bool, optional): [description]. Defaults to False.
		"""

		self.sort_by_mz()
		self.normalize(cfm_style=cfm_style)

	'''
	
	def compute_peaks_min_max(self):
		if self.peaks is None:
			return
		
		self._min_mz = self.peaks[0][0]
		self._max_mz = self.peaks[0][0]
		self._min_intensity = self.peaks[0][1]
		self._max_intensity = self.peaks[0][1]

		for peak in self.peaks[1:]:
			if peak[1] > self._max_intensity:
				self._max_intensity = peak[1]
			if peak[1] < self._min_intensity:
				self._min_intensity = peak[1]
			if peak[0] > self._max_mz:
				self._max_mz = peak[0]
			if peak[0] < self._min_mz:
				self._min_mz = peak[0]
	'''
	
	'''
	Method compute qtof ev from obitrap nce
	'''

	@staticmethod
	def nce_to_ev(isolation_center: float, nce: float, charge_state: float = 1.0) -> float:
		ev = nce * isolation_center / (500 * charge_state)
		return ev

	'''
	Method compute obitrap nce from qtof ev 
	'''

	@staticmethod
	def ev_to_nce(isolation_center: float, ev: float, charge_state: float = 1.0) -> float:
		nce = ev * (500 * charge_state) / isolation_center
		return nce

	@staticmethod
	def get_matched_peaks(ref_spectrum: 'Spectrum', query_spectrum: 'Spectrum',
						  abs_tol: float = 0.01, ppm_tol: float = 10,
						  include_miss_matches: bool = True) -> Tuple[int, np.ndarray]:
		"""
			Method to get matched peaks between two spectra, Return a list of matched peaks, empty match will be ignored
		Args:
			ref_spectrum (Spectrum): [description]
			query_spectrum (Spectrum): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (float, optional): [description]. Defaults to 10.
			include_miss_matches (bool, optional): [description]. Defaults to True.

		Returns:
			Tuple[int, np.ndarray]: num of macthed, matched peaks list [mz1, intensity1, mz2, intensity2]
		"""
		if ref_spectrum.is_sorted is False:
			ref_spectrum.sort_by_mz()
		if query_spectrum.is_sorted is False:
			query_spectrum.sort_by_mz()

		matches = []
		num_matched = 0
		sp1_idx, sp2_idx = 0, 0

		while sp1_idx < ref_spectrum.get_num_peaks() and sp2_idx < query_spectrum.get_num_peaks():
			sp1_peak = ref_spectrum.peaks[sp1_idx]
			sp1_mz = sp1_peak[0]

			sp2_peak = query_spectrum.peaks[sp2_idx]
			sp2_mz = sp2_peak[0]

			# get tolarnc

			allowed_tol = Utilities.get_mass_tol(max(sp1_mz, sp2_mz), abs_tol, ppm_tol)
			#print(allowed_tol)
			diff = abs(sp1_mz - sp2_mz)
			# print(allowed_tol , diff, sp1_mz,  sp2_mz)
			# if found a pair
			if diff <= allowed_tol:
				sp1_idx += 1
				sp2_idx += 1
				#matches.append((sp1_peak, sp2_peak))
				matches.append([sp1_peak[0], sp1_peak[1], sp2_peak[0], sp2_peak[1]])
				num_matched += 1
				#if annotate:
				#	sp1_peak.annotation = "#{}".format(num_matched)
				#	sp2_peak.annotation = "#{}".format(num_matched)

			elif sp1_mz < sp2_mz:
				# skip sp1 peak
				sp1_idx += 1
				if include_miss_matches:
					matches.append([sp1_peak[0], sp1_peak[1], sp1_peak[0], 0.0])
			else:
				# skip sp2 speak
				sp2_idx += 1
				if include_miss_matches:
					matches.append([sp2_peak[0], 0.0, sp2_peak[0], sp2_peak[1]])

			
		if include_miss_matches:
			while sp1_idx < ref_spectrum.get_num_peaks():
				# add all rest peak from spectrum 1
				sp1_peak = ref_spectrum.peaks[sp1_idx]
				matches.append([sp1_peak[0], sp1_peak[1], sp1_peak[0], 0.0])
				sp1_idx += 1
			while sp2_idx < query_spectrum.get_num_peaks():
				# add all rest peak from spectrum 2
				sp2_peak = query_spectrum.peaks[sp2_idx]
				matches.append([sp2_peak[0], 0.0, sp2_peak[0], sp2_peak[1]])
				sp2_idx+= 1

		return num_matched,  np.array(matches) 

	def get_peak_by_mz(self, mz, abs_tol=0.001):
		"""	get peak by mz with abs_tol
		Args:
			mz ([type]): [description]
			abs_tol (float, optional): [description]. Defaults to 0.001.

		Returns:
			Peak: [description]
		"""
		raise NotImplementedError
	
	def remove_all_peaks(self):
		self.peaks = None


	@staticmethod
	def merge_spectrum(base_spectrum: 'Spectrum', additional_spectrum: 'Spectrum', merge_method: str = 'union',
						 intensity_aggregate_method: str = 'sum', normalize = True) -> 'Spectrum':
		"""	Method that create new spectra by intersect two spectra, mz is based on base_spectrum in case of matching peaks
				merge_method:str = 'union' or 'intersect'
				intensity_aggregate_method can be 'sum' , 'max' 
		Raises:
			Exception: [description]

		Returns:
			[type]: [description]
		"""

		if merge_method not in Spectrum._supported_spectrum_merge_method:
			raise TypeError(f"spectrum merge method [union,intersect], Input was {intensity_aggregate_method}")

		if additional_spectrum.get_num_peaks() == 0:
			return base_spectrum

		include_miss_matches = (merge_method == 'union')

		_, matched_peaks = Spectrum.get_matched_peaks(
			base_spectrum, additional_spectrum, include_miss_matches=include_miss_matches)

		merged_spectrum = Spectrum()
		merged_spectrum.precursor_mz	= base_spectrum.precursor_mz
		merged_spectrum.adduct_type	 = base_spectrum.adduct_type
		merged_spectrum.ionization_type = base_spectrum.ionization_type
		merged_spectrum.analyzer_type   = base_spectrum.analyzer_type
		merged_spectrum.charge_type	 = base_spectrum.charge_type
		merged_spectrum.ms_level		= base_spectrum.ms_level
		merged_spectrum.inchikey		= base_spectrum.inchikey
		merged_spectrum.smiles_or_inchi = base_spectrum.smiles_or_inchi
		merged_spectrum.compound_data_source = base_spectrum.compound_data_source
		merged_spectrum.compound_data_source_id = base_spectrum.compound_data_source_id
		merged_spectrum.compound_exact_mass = base_spectrum.compound_exact_mass
		merged_spectrum.formula = base_spectrum.formula
		merged_spectrum.precursor_mz = base_spectrum.precursor_mz
		
		#copy.deepcopy(base_spectrum)
		#merged_spectrum.remove_all_peaks()

		mz_list = matched_peaks[:,0].tolist()
		if intensity_aggregate_method == "sum":
			int_list = matched_peaks[:,1] + matched_peaks[:,3]
		elif intensity_aggregate_method == "max":
			int_list = np.maximum(matched_peaks[:,1],matched_peaks[:,3])
		elif intensity_aggregate_method == "mean":
			int_list = (matched_peaks[:,1] + matched_peaks[:,3])/2.0

		int_list = int_list.tolist()

		merged_spectrum.add_peaks(mz_list, int_list)
		merged_spectrum.sort_by_mz()
	
		if normalize:
			merged_spectrum.normalize()

		return merged_spectrum


	@staticmethod
	def merge_multi_spectrum(spectrum_list:List['Spectrum'], merge_method: str = 'union',
						 intensity_aggregate_method: str = 'sum',) -> 'Spectrum':
		"""	Method that create new spectra by intersect two spectra, mz is based on base_spectrum in case of matching peaks
				merge_method:str = 'union' or 'intersect'
				intensity_aggregate_method can be 'sum' , 'max' , or 'min'
		Raises:
			Exception: [description]

		Returns:
			[type]: [description]
		"""

		if merge_method not in Spectrum._supported_spectrum_merge_method:
			raise TypeError(f"spectrum merge method [union,intersect], Input was {intensity_aggregate_method}")

		base_spectrum = spectrum_list[0]
		for additional_spectrum in spectrum_list[1:]:
			merged_spectrum = Spectrum.merge_spectrum(base_spectrum,additional_spectrum,merge_method,intensity_aggregate_method)
			base_spectrum = merged_spectrum

		return merged_spectrum
		
	def get_cfm_text_str(self, annotation_right: bool = False):
		string = ''
		for peak in self.peaks:
			if not annotation_right or peak.annotation == Standardization.NA:
				string += '{:.6f} {:.2f}\n'.format(peak[0],peak[1])
			else:
				string += '{:.6f} {:.2f} {}\n'.format(peak[0],peak[1], peak[2])
		return string

	def fill_formula(self):
		if self.smiles_or_inchi != Standardization.NA and self.formula == Standardization.NA:
			try:
				self.formula = Utilities.get_formula_by_chemid(self.smiles_or_inchi)
			except ValueError:
				print('ValueError,can not compute formula from', self.smiles_or_inchi)
	
	def fill_inchikey(self):
		if self.smiles_or_inchi != Standardization.NA and self.inchikey == Standardization.NA:
			try:
				self.inchikey = Utilities.get_inchikey_by_chemid(self.smiles_or_inchi)
			except ValueError:
				print('ValueError,can not compute inchikey from', self.smiles_or_inchi)
	def compute_compound_exact_mass(self):
		#print(self.smiles_or_inchi, self.compound_exact_mass)
		if self.smiles_or_inchi != Standardization.NA and (self.compound_exact_mass == Standardization.NA or self.compound_exact_mass == -1.0):
			try:
				self.compound_exact_mass = Utilities.get_mol_mw_by_chemid(self.smiles_or_inchi)
			except ValueError:
				print('ValueError,can not compute compound_exact_mass from', self.smiles_or_inchi)

		if (self.precursor_mz != -1.0 and self.precursor_mz != Standardization.NA)\
			  and (self.compound_exact_mass == Standardization.NA or self.compound_exact_mass == -1.0)\
			  and self.adduct_type != Standardization.NA: 
			self.compound_exact_mass = Utilities.ion_mass_to_neutral_mass(self.precursor_mz, self.adduct_type)
			

	def compute_precursor_mz(self):
		if self.compound_exact_mass == Standardization.NA and self.adduct_type == Standardization.NA:
			#print("Warning, compound_exact_mass is unknown")
			return
		self.precursor_mz = Utilities.neutral_mass_to_ion_mass(self.compound_exact_mass, self.adduct_type)
		 
	def get_data_for_spec2vec(self):

		mz = np.array([peak.mz for peak in self.peaks], dtype="float")
		intensities = np.array(
			[peak.intensity / self.max_intensity for peak in self.peaks], dtype="float")
		metadata = dict(precursor_mz=self.precursor_mz)

		return mz, intensities, metadata

	def get_peaks(self) -> np.ndarray:
		return self.peaks

	def get_mzs(self) -> List[float]:
		return self.peaks['mz']

	def convert_intensity_to_log_scale(self):
		"""[Covert peak intensity to log scale, such that peak.intensity = np.log(peak.intensity+1)]
		"""
		#for peak in self.peaks:
		self.peaks['intensity'] = np.log(self.peaks['intensity']  + 1)

		#self._max_intensity = np.max(self.peaks['intensity'] )
		#self._min_intensity = np.min(self.peaks['intensity'] )

		self.normalize()

	def convert_intensity_to_linear_scale(self):
		"""[Covert peak intensity to linear scale, such that peak.intensity = np.log(peak.intensity+1)]
		"""
		ratio = 100 / np.log(101)
		self.peaks['intensity'] = np.exp(self.peaks['intensity'] / ratio) - 1

		#self._max_intensity = np.max(self.peaks['intensity'] )
		#self._min_intensity = np.min(self.peaks['intensity'] )

		self.normalize()

	def get_splash_hash_input(self) -> List[Tuple[float,float]]:
		"""[summary]

		Returns:
			[type]: [description]
		"""
		rev_list = []
		for peak in self.peaks:
			rev_list.append((peak[0], peak[1]))
		return rev_list

	def get_num_peaks(self) -> int:
		""" Method to get number of peaks
		Returns: number of peaks
		"""
		return len(self.peaks) if self.peaks is not None else 0
		#return len(self.peaks)

	def get_nl_spectrum(self) -> 'Spectrum':
		"""Method to get create a NL spectrum

		Returns:
			Spectrum: NL spectrum
		"""
		nl_spectrum = Spectrum()
		nl_spectrum.ionization_type = self.ionization_type
		nl_spectrum.ms_level = self.ms_level
		nl_spectrum.charge_type = self.charge_type
		nl_spectrum.data_source = self.data_source
		nl_spectrum.compound_data_source = self.compound_data_source
		nl_spectrum.chromatography_type = self.chromatography_type
		nl_spectrum.adduct_type = 'Neutral-Loss-' + self.adduct_type
		nl_spectrum.collision_energy = self.collision_energy
		nl_spectrum.collision_energy_unit = self.collision_energy_unit
		nl_spectrum.analyzer_type = self.analyzer_type
		nl_spectrum.inchikey = self.inchikey
		nl_spectrum.smiles_or_inchi = self.smiles_or_inchi
		nl_spectrum.formula = self.formula

		nl_peak_list = []
		for peak in self.peaks:
			nl_annotation = Standardization.NA
			if peak.annotation != Standardization.NA:
				try:
					peak_formula = Utilities.get_formula_by_chemid(peak.annotation)
				except:
					continue
				else:
					nl_annotation = Utilities.get_nl_formula(self.formula, peak_formula)

			nl_peak_list.append((max(self.precursor_mz - peak.mz,0.0), peak.intensity, nl_annotation))
			#nl_spectrum.add_peak(max(self.precursor_mz - peak.mz,0.0), peak.intensity, nl_annotation)
		nl_spectrum.add_peaks_from_peak_list(nl_peak_list)
		return nl_spectrum

	def has_peak_at(self, mz,abs_tol = 0.01, ppm_tol = 10):
		"""Method to check if a peak exists at a given mz

		Args:
			mz (float): mz to check
			ppm_tol (int, optional): ppm tolerance. Defaults to 10.
			abs_tol (float, optional): absolute tolerance. Defaults to 0.1.

		Returns:
			bool: True if peak exists
		"""
		for peak_mz in self.peaks['mz']:
			if abs(peak_mz - mz) < Utilities.get_mass_tol(peak_mz,abs_tol,ppm_tol):
				return True

		return False

	def rescale_peaks(self, scale_factor):
		"""Method to rescale peaks by a given factor

		Args:
			factor (float): factor to rescale peaks by
		"""
		for peak in self.peaks:
			peak.intensity *= scale_factor
	
	def get_dot_ms_str(self) -> str:
		dot_ms_str = ""
		if self.peaks is not None:
			dot_ms_str +=">parentmass {}\n".format(self.precursor_mz)
			dot_ms_str +=">ionization {}\n".format(self.adduct_type)
			dot_ms_str +="#instrument ESI-QTOF\n\n"
			dot_ms_str +=">collision {}{}\n".format(self.collision_energy,self.collision_energy_unit)
			for peak in self.peaks:
				dot_ms_str +="{} {}\n".format(peak[0], peak[1])
			dot_ms_str += "\n"
		return dot_ms_str
	
	def get_mgf_str(self, title) -> str:
		mgf_str = "BEGIN IONS\n"
		mgf_str +="TITLE={}@{}{}\n".format(title, self.collision_energy,self.collision_energy_unit)
		mgf_str +="PEPMASS={}\n".format(self.precursor_mz)
		mgf_str +="CHARGE=1+\n" if self.charge_type == Standardization.Pos else "CHARGE=1-\n"
		mgf_str +=f"INSTRUMENT=ESI-{self.analyzer_type}\n"
		if Standardization.RT_SEC == self.rt_unit:
			mgf_str +=f"RTINSECONDS={self.rt}\n"
		elif Standardization.RT_MIN == self.rt_unit:
			mgf_str +=f"RTINSECONDS={self.rt * 60}\n"
		for peak in self.peaks:
			mgf_str +="{} {}\n".format(peak[0], peak[1])
		mgf_str += "END IONS\n\n"
		return mgf_str
	
	def get_bin_spectrum(self, bin_size = 0.01, max_mz = 1500.0):
		num_bins = int(max_mz / bin_size) + 1
		bin_spectrum = np.zeros(num_bins)
		for peak in self.peaks:
			bin_idx = int(peak[0] / bin_size)
			bin_spectrum[bin_idx] += peak[1]
		# bins = [np.sum(a[np.argwhere(bins_indices == i).flatten()]) for i in range(len(a))]
		return bin_spectrum
	
	def round_spectrum(self, decimal_place = 1, agg = 'sum'):
		peak_data = {}
		for peak in self.peaks:
			rounded_mz = round(peak[0], decimal_place)
			if rounded_mz not in peak_data:
				peak_data[rounded_mz] = peak[1]
			elif agg == 'sum':
				peak_data[rounded_mz] += + peak[1]
			elif agg == 'max':
				peak_data[rounded_mz] = max(peak_data[rounded_mz], peak[1])
				
		self.peaks = np.empty(len(peak_data), dtype=self._peak_mtype)
		for idx, (mz,intensity) in enumerate(peak_data.items()):
			#print(idx, peak)
			self.peaks[idx]['mz'] = mz
			self.peaks[idx]['intensity'] = intensity
			self.peaks[idx]['annotation'] = ''

	def to_prez_only_spectrum(self):
		"""Method to set current spectrum with only precursor mz, if current spectrum has no peaks do nothing
		"""
		if isinstance(self.precursor_mz, float) and self.precursor_mz > -1 and self.get_num_peaks():
			self.peaks = np.array([(self.precursor_mz, 100.0, '')], dtype=self._peak_mtype)
		
	def to_random_spectrum(self, num_peaks_min=1, num_peaks_max  = 50, rng = None):
		"""Method to set current spectrum  create a random spectrum, if current spectrum has no peaks do nothing
		"""
		if self.get_num_peaks():
			if rng is None:
				# parral safe seed
				seed = (uuid.uuid4().int + int(time.time()) + os.getpid()) % (2**32 - 1)
				rng = np.random.default_rng(seed=seed)
			num_peaks = rng.integers(low = num_peaks_min, high=num_peaks_max)
			rand_peaks_value = rng.random((num_peaks, 2)) 
			rand_peaks = [(x[0] * 1000, x[1] * 100,'') for x in rand_peaks_value]
			self.peaks = None
			self.add_peaks_from_peak_list(rand_peaks)
			self.sort_by_mz()
			self.normalize()
			self.remove_noise()
   
	def to_random_picked_spectrum(self, selections, num_peaks_min=1, num_peaks_max = 50, rng = None):
		"""Method to set current spectrum  create a random spectrum, if current spectrum has no peaks do nothing
		"""
		if self.get_num_peaks():
			if rng is None:
				# parral safe seed
				seed = (uuid.uuid4().int + int(time.time()) + os.getpid()) % (2**32 - 1)
				rng = np.random.default_rng(seed=seed)
			num_peaks = rng.integers(low = num_peaks_min, high= min(num_peaks_max, len(selections)))
			self.peaks = rng.choice(selections, num_peaks, replace=False)
			self.sort_by_mz()
			self.normalize()
			self.remove_noise(centroid=True)
   
	@staticmethod
	def get_matched_bin_peaks(ref_spectrum: 'Spectrum', query_spectrum: 'Spectrum',
						  bin_size: float = 0.01) -> Tuple[int, np.ndarray]:
		
		max_mz = max(ref_spectrum.max_mz, query_spectrum.max_mz)
		ref_spectrum_bin = ref_spectrum.get_bin_spectrum(bin_size, max_mz)
		query_spectrum_bin = query_spectrum.get_bin_spectrum(bin_size, max_mz)

		num_bins = len(ref_spectrum_bin)
		matched_array = np.zeros((num_bins, 4), dtype=float)

		matched_array[:, 0] = np.arange(0, num_bins) * bin_size  # First column as row index
		matched_array[:, 1] = ref_spectrum_bin  # Assign a's values to column index 1

		matched_array[:, 2] = np.arange(0, num_bins) * bin_size  # First column as row index
		matched_array[:, 3] = query_spectrum_bin  # Assign b's values to column index 2

		#print(matched_array)
		matched_mask = np.logical_and(ref_spectrum_bin != 0, query_spectrum_bin != 0)
		#matched_array = matched_array[matched_mask]
		num_matched = np.sum(matched_array[matched_mask])
		matched_array = matched_array[np.logical_or(ref_spectrum_bin != 0, query_spectrum_bin != 0)]
		return num_matched, matched_array 

	def	get_bin_spectrum(self, bin_size = 0.01, max_mz = 1500.0):
		num_bins = int(max_mz / bin_size) + 1 
		bin_spectrum = np.zeros(num_bins)
		for peak in self.peaks:
			bin_idx = int(peak[0] / bin_size)
			bin_spectrum[bin_idx] += peak[1]
		return bin_spectrum