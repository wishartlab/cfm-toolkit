import json
import re

from cfmtoolkit.datastructures import Spectrum
from cfmtoolkit.standardization import Standardization


class TMIC:
	"""
	Class to read TMIC JSON
	"""

	@classmethod
	def read_spectra_from_file(cls, file_path: str, meta_data: dict):

		with open(file_path) as json_infile:
			data = json.load(json_infile)
		# print(data)

		spectrum = Spectrum()
		spectrum.inchikey = data['inchi_key'][0]
		spectrum.smiles_or_inchi = data['smiles'][0]
		# spectrum.smiles_or_inchi = data['inchi'][0]
		spectrum.adduct_type = data['adduct_type'][0]
		spectrum.compound_name = data['name'][0]
		spectrum.data_source = data['source'][0]
		spectrum.data_source_id = data['source_id'][0]
		spectrum.instrument_model = data['instrument'][0]

		if spectrum.adduct_type.endswith('+'):
			spectrum.charge_type = Standardization.Pos
		elif spectrum.adduct_type.endswith('-'):
			spectrum.charge_type = Standardization.Neg
		spectrum.ms_level = meta_data['ms_level']

		instrument_type = data['instrument_type'][0]
		instrument_params = instrument_type.split('-')

		if len(instrument_params) == 3:
			spectrum.chromatography_type = instrument_params[0]
			spectrum.ionization_type = instrument_params[1]
			spectrum.analyzer_type = instrument_params[2]
		else:
			spectrum.chromatography_type = Standardization.LC
			spectrum.ionization_type = instrument_params[0]
			spectrum.analyzer_type = instrument_params[1]

		energy_str = data['collision_energy_original'][0]

		if '%' in energy_str or '(nominal)' in energy_str:
			spectrum.collision_energy_unit = '%'
			spectrum.collision_method = Standardization.HCD
		elif 'ev' in energy_str:
			spectrum.collision_energy_unit = 'ev'
			spectrum.collision_method = Standardization.CID
		else:
			return None

		pattern = re.compile(r"^[0-9]+")
		matches = re.search(pattern, energy_str)
		if matches is not None:
			spectrum.collision_energy = float(matches.group(0))

		for peak in data['spectrum']:
			# print(peak['m_z'],peak['intensity'],peak['peak_bruto_formula'])
			if "no bruto formula found" != peak['peak_bruto_formula']:
				spectrum.add_peak(
					Peak(peak['m_z'], peak['intensity'], peak['peak_bruto_formula']))
			else:
				spectrum.add_peak(Peak(peak['m_z'], peak['intensity']))

		# print(spectrum.inchikey, spectrum.smiles_or_inchi)
		spectrum.fill_formula()
		spectrum.compute_compound_exact_mass()
		spectrum.compute_precursor_mz()
		spectrum.post_process()

		return spectrum
