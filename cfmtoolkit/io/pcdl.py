
from cfmtoolkit.datastructures import Spectrum
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities

class PCDL:
	@classmethod
	def parse_pcdl(cls, text_list: list[str]):
		spectrum = Spectrum()
		for row in text_list:
			str_row = row.decode("utf-8")
			if str_row.startswith("ACCESSION: "):
				id_str = str_row.split(": ")[1].strip()
				spectrum.data_source_id = id_str
			# Compound
			elif str_row.startswith("CH$SMILES: "):
				smiles = str_row.split(" ")[1].strip()
				spectrum.smiles_or_inchi = smiles
			elif str_row.startswith("CH$IUPAC: InChI"):
				continue
			elif str_row.startswith("CH$LINK: INCHIKEY"):
				inchikey = str_row.split("INCHIKEY")[1].strip()
				spectrum.inchikey = inchikey
			elif str_row.startswith("CH$FORMULA:"):
				forumla = str_row.split()[1].strip()
				spectrum.formula = forumla
			elif str_row.startswith("CH$EXACT_MASS:"):
				extact_mass = str_row.split()[1].strip()
				extact_mass = float(extact_mass)
				spectrum.compound_exact_mass = extact_mass
			elif str_row.startswith("CH$LINK: HMDB "):
				hmdb_id = str_row.split()[-1].strip()
				spectrum.compound_data_source_id = hmdb_id
				#print(hmdb_id)
			# Acqusition
			elif str_row.startswith("AC$MASS_SPECTROMETRY: ION_MODE NEGATIVE"):
				spectrum.charge_type = Standardization.Neg
			elif str_row.startswith("AC$MASS_SPECTROMETRY: ION_MODE POSITIVE"):
				spectrum.charge_type = Standardization.Pos
			elif str_row.startswith("AC$MASS_SPECTROMETRY: COLLISION_ENERGY"):
				ce = str_row.split("COLLISION_ENERGY")[-1].strip()
				spectrum.collision_energy_unit = "eV"
				spectrum.collision_energy = float(ce)
			# MS
			elif str_row.startswith("MS$FOCUSED_ION: PRECURSOR_M/Z "):
				precursor_mz = float(str_row.split()[-1].strip())
				#print("precursor_mz", precursor_mz)
				spectrum.precursor_mz = float(precursor_mz)
			elif str_row.startswith("MS$FOCUSED_ION: PRECURSOR_TYPE "):
				precursor_type = str_row.split()[-1].strip()
				spectrum.adduct_type = precursor_type
				#print("precursor_type", precursor_type)
			# Peaks
			elif str_row.startswith(" "):
				mz, rel_int, nist_rel_int = [float(i) for i in str_row.split()]
				spectrum.add_peak(mz, rel_int)
			elif str_row.startswith("PK$SPLASH: "):
				spectrum.splash = str_row.split(": ")[1].strip()
			elif str_row.startswith("//"):
				continue

		return spectrum