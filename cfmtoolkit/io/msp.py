import re

from cfmtoolkit.datastructures import Spectrum
from cfmtoolkit.standardization import Standardization

from tqdm import tqdm
from rdkit.Chem import PandasTools
from rdkit.Chem import MolToSmiles
from typing import List,Dict

class NIST_14_MSP:
	"""
	Class to read NIST v14 format
	"""
	_analyzer_type_keywords_mapping = {
		'QqQ': Standardization.QqQ,
		'QTOF': Standardization.QTOF,
		'Q-TOF': Standardization.QTOF,
		'qTof': Standardization.QTOF,
		'BEqQ': Standardization.BEqQ,
		'HCD': Standardization.Orbitrap,
		'QqLIT': Standardization.QqLIT,
		'API': Standardization.API,
		'FT-ICR/FTMS': Standardization.ICR,
		'FT-ICR': Standardization.ICR,
		'EBEqEBE': Standardization.EBEqEBE,
		'EBE': Standardization.EBE,
		'BE': Standardization.BE,
		'IT-FT': Standardization.Orbitrap,
		'IT': Standardization.IT,
		'QFT': Standardization.QFT
	}

	_ionization_type_keywords_mapping = {
		'APCI': Standardization.APCI,
		'APPI': Standardization.APPI,
		'ESI': Standardization.ESI,
		'CI': Standardization.CI,
		'FAB': Standardization.FAB,
		'PI': Standardization.PI,
		'EI': Standardization.EI,
		'EI-B': Standardization.EI,
		'LSIMS': Standardization.LSIMS,
		'In-source CID': Standardization.In_source_CID
	}

	_chromatography_type_keywords_mapping = {
		'HPLC': Standardization.LC,
		'direct flow injection': Standardization.DI,
		'GC': Standardization.GC,
		'FAB/LSIMS probe': Standardization.FAB,
		'direct probe': Standardization.DI,
		'IC': Standardization.IC,
		'LC': Standardization.LC,
		'CE': Standardization.CE,
		'cleanup device': Standardization.FI,
		'ionized gas flow': Standardization.DI
	}

	_charge_type_keywords_mapping = {
		'P': Standardization.Pos,
		'POSITIVE': Standardization.Pos,
		'N': Standardization.Neg,
		'NEGATIVE': Standardization.Pos,
	}

	@classmethod
	def _get_ionization_type(cls, nist_synon: str):
		if 'or' in nist_synon or '/' in nist_synon:
			return Standardization.NA
		for key in cls._ionization_type_keywords_mapping:
			if key in nist_synon:
				return cls._ionization_type_keywords_mapping[key]
		return Standardization.NA

	@classmethod
	def _get_chromatography_type(cls, nist_synon: str):
		if 'or' in nist_synon or '/' in nist_synon:
			return Standardization.NA
		for key in cls._chromatography_type_keywords_mapping:
			if key in nist_synon:
				return cls._chromatography_type_keywords_mapping[key]
		return Standardization.NA

	@classmethod
	def _get_analyzer_type(cls, nist_synon: str):
		if 'or' in nist_synon or '/' in nist_synon:
			return Standardization.NA
		for key in cls._analyzer_type_keywords_mapping:
			if key in nist_synon:
				return cls._analyzer_type_keywords_mapping[key]
		return Standardization.NA

	@classmethod
	def _get_ce_level(cls, nist_synon: str):
		pattern = re.compile(
			r"^([^0-9]*)([0-9]+(\.[0-9]+)?)\s?(eV|V|v|keV|%|$)")
		matches = re.search(pattern, nist_synon)

		prefix, value, unit = Standardization.NA, -1.0, Standardization.NA
		matches = re.search(pattern, nist_synon)
		if matches is not None:
			prefix = matches.group(1).rstrip()
			value = matches.group(2)
			unit = matches.group(4)
			# print(nist_synon, prefix.lower(), value, unit.lower())
			if unit is None:
				unit = "ev"
			# print(prefix, value, unit)
			value = -1.0 if value is None else float(value)

		return prefix.lower(), value, unit.lower()

	'''
	Name: 3-Hydroxy-3-(2-(2-hydroxyphenyl)-2-oxoethyl)-1,3-dihydro-2H-indol-2-one
	Synon: 2H-Indol-2-one, 1,3-dihydro-3-hydroxy-3-[2-(2-hydroxyphenyl)-2-oxoethyl]-
	Synon: $:17mol_Vial_ID=1000001
	Synon: $:03[M+H]+
	Synon: $:00MS2
	Synon: $:04284.0917
	Synon: $:06IT/ion trap
	Synon: $:07Thermo Finnigan LCQ Deca
	Synon: $:09HPLC
	Synon: $:10ESI
	Synon: $:05resonant relative/normalized 35 %
	Synon: $:11P
	Synon: $:08wideband
	Synon: $:28UAEJHGCEFAFLIJ-UHFFFAOYSA-N
	Formula: C16H13NO4
	MW: 283.084457
	PrecursorMZ: 284.0917
	CASNO: 85778456
	NISTNO: 1000001
	Comment: Provided by Thermo Finnigan, measured by John Halket, Centre for Chemical Sciences, University of London
	Num peaks: 86
	81 9.99
	82 7.99
	83 7.99
	88 11.99
	'''

	@classmethod
	def read(cls, spectrum_strs, include_peak_annotation=False, post_process=True):
		spectrum = Spectrum()
		is_peak = False

		for row in spectrum_strs:
			# get peak data
			row_lower = row.lower()
			if is_peak:
				# data foramt: mass intensity annotation, where annotation is optional
				peak_data = row.split()
				mass_str = peak_data[0]
				intensity_str = peak_data[1]
				annotation_str = Standardization.NA
				if len(peak_data) > 2 and include_peak_annotation:
					annotation_str = ''.join(peak_data[2:]).replace('"', "")

				# add peak
				mass, intensity = float(mass_str), float(intensity_str)
				if intensity > 0.0:
					spectrum.add_peak(mass, intensity, annotation_str)

			# get synons and other meta data
			if row.startswith('Synon:'):
				synon_data = row.split(maxsplit=1)
				# we don't really need 'Synon:' part
				data = synon_data[1]
				if data.startswith('$'):
					key = data[0:4]
					payload = data[4:]

					if key in ['$:01', '$:02', '$:08']:
						# 01, 02 unkown and unused cases
						# 08 is unkown type
						pass
					elif key == '$:00':
						spectrum.ms_level = payload
					elif key == '$:03':
						spectrum.adduct_type = payload
					elif key == '$:04':
						# PrecursorMZ
						pass
					elif key == '$:05':
						prefix, value, unit = cls._get_ce_level(payload)
						spectrum.collision_energy_prefix = prefix
						spectrum.collision_energy = value
						spectrum.collision_energy_unit = unit
					elif key == '$:06':
						spectrum.analyzer_type = cls._get_analyzer_type(
							payload)
						# if(payload == 'HCD'):
						#	print(payload, spectrum.analyzer_type )
					elif key == '$:07':
						spectrum.instrument_model = payload
					elif key == '$:09':
						spectrum.chromatography_type = cls._get_chromatography_type(
							payload)
					elif key == '$:10':
						spectrum.ionization_type = cls._get_ionization_type(
							payload)
					elif key == '$:11':
						spectrum.charge_type = cls._charge_type_keywords_mapping[payload]
					elif key == '$:12':
						spectrum.collision_gas_type = payload
					elif key == '$:28':
						spectrum.inchikey = payload
				else:
					pass
			elif row_lower.startswith('name:'):
				payload = row.split()[1]
				spectrum.compound_name = payload
			elif row_lower.startswith('formula:'):
				payload = row.split()[1]
				spectrum.formula = payload
			elif row_lower.startswith('mw:'):
				payload = row.split()[1]
				if payload != None:
					spectrum.compound_exact_mass = float(payload)
			elif row_lower.startswith('precursormz:'):
				payload = row.split()[1]
				if payload != None:
					spectrum.precursor_mz = float(payload)
			elif row_lower.startswith('casno:'):
				pass
			elif row_lower.startswith('nistno'):
				payload = row.split()[1]
				spectrum.data_source_id = "NIST#" + payload
			elif row_lower.startswith('num peaks:'):
				# Peak data is after num of peaks
				is_peak = True

		if post_process:
			spectrum.post_process()
			spectrum.remove_noise(centroid = True, centroid_mz_distance=0.5)
		return spectrum

	@classmethod
	def read_msp_file(cls, msp_file, encoding: str='utf-8', post_process:bool= True) -> List[Spectrum]:

		num_lines = 0
		spectrum_list = []
		with open(msp_file, encoding=encoding) as msp_file_in:
			for _ in msp_file_in:
				num_lines += 1

		#num_lines = sum(1 for _ in open('myfile.txt','r'))
		with open(msp_file, encoding=encoding) as msp_file_in:
			spectrum_lines = []
			# read all lines
			for _, line in enumerate(tqdm(msp_file_in, total=num_lines, desc=msp_file)):
				line = line.rstrip()
				#print(line)
				if line != "":
					spectrum_lines.append(line,)
				else:
					spectrum = cls.read(spectrum_lines, post_process)
					spectrum_list.append(spectrum)
					spectrum_lines = []
		return spectrum_list

class NIST_20_MSP(NIST_14_MSP):
	'''
	Name: 1',3'-Bis[1,2-dilinoleoyl-sn-glycero-3-phospho]-sn-glycerol
	Notes: Consensus spectrum; Water/isopropanol/Formic acid; added_Vial_ID=4487
	Precursor_type: [M+NH4]+
	Spectrum_type: MS2
	PrecursorMZ: 1467.0061
	Instrument_type: Q-TOF
	Instrument: Agilent QTOF 6530
	Sample_inlet: direct flow injection
	Ionization: ESI
	In-source_voltage: 150
	Collision_gas: N2
	Collision_energy: 10
	Ion_mode: P
	InChIKey: LSHJMDWWJIYXEM-XGJIDDIWSA-N
	Synon: Heart CA, main component
	Synon: 8,11-Octadecadienoic acid, 5,8,11-trihydroxy-5,11-dioxido-4,6,10,12-tetraoxa-5,11-diphosphapentadecane-1,2,14,15-tetrayl ester
	Formula: C81H142O17P2
	MW: 1448
	ExactMass: 1448.97223
	CAS#: 746600-94-2;  NIST#: 1035167
	DB#: 2
	Comments: NIST Mass Spectrometry Data Center
	Num Peaks: 10
	597.50 50.95
	'''

	@classmethod
	def _get_deriv_info(cls, comment: str):
		pattern = re.compile(
			r'derivatization type=([^"]+)')
		matches = re.search(pattern, comment)
		derivatization_type = Standardization.NA
		if matches is not None:
			derivatization_type = matches.group(1).rstrip()

		derivatization_mass = -1.0
		pattern = re.compile(
			r'derivatization mass=([^"]+)')
		matches = re.search(pattern, comment)
		if matches is not None:
			text = matches.group(1).rstrip()
			if text.isnumeric():
				derivatization_mass = float(text)

		return derivatization_type, derivatization_mass

	@classmethod
	def read(cls, spectrum_strs, include_peak_annotation=False, post_process=True):
		'''
		TODO
		Add code for CID and HCD
		Add code for CE level and unit
		check code for GC nand LC
		'''
		spectrum = Spectrum()
		is_peak = False
		# print(spectrum_strs)
		for row in spectrum_strs:
			row_lower = row.lower()
			# print(row_lower)
			# get peak data
			if is_peak == True:
				# data foramt: mass intensity annotation, where annotation is optional
				peak_data = row.split()
				mass_str = peak_data[0]
				intensity_str = peak_data[1]
				annotation_str = Standardization.NA
				if len(peak_data) > 2 and include_peak_annotation:
					annotation_str = ''.join(peak_data[2:]).replace('"', "")

				# add peak
				mass, intensity = float(mass_str), float(intensity_str)
				if intensity > 0.0:
					spectrum.add_peak(mass, intensity, annotation_str)

			if row_lower.startswith('name:'):
				payload = cls.extract_playload(row)
				spectrum.compound_name = payload
			elif row_lower.startswith('precursor_type:'):
				payload = cls.extract_playload(row)
				#print(row, payload)
				spectrum.adduct_type = payload
				#print(spectrum.adduct_type)
			elif row_lower.startswith('spectrum_type:'):
				payload = cls.extract_playload(row)
				if payload == 'MS1':
					spectrum.ms_level = Standardization.MS1
				if payload == 'MS2':
					spectrum.ms_level = Standardization.MS2
			elif row_lower.startswith('instrument_type:'):
				payload = cls.extract_playload(row)
				spectrum.instrument_model = payload
				spectrum.analyzer_type = cls._get_analyzer_type(payload)
				if spectrum.chromatography_type == Standardization.NA:
					spectrum.chromatography_type = cls._get_chromatography_type(payload)
				if spectrum.ionization_type == Standardization.NA:
					spectrum.ionization_type = cls._get_ionization_type(payload)
			elif row_lower.startswith('sample_inlet:'):
				payload = cls.extract_playload(row)
				if spectrum.chromatography_type == Standardization.NA:
					spectrum.chromatography_type = cls._get_chromatography_type(payload)
			elif row_lower.startswith('ionization:'):
				payload = cls.extract_playload(row)
				spectrum.ionization_type = cls._get_ionization_type(payload)
			elif row_lower.startswith('collision_gas'):
				payload = cls.extract_playload(row)
				spectrum.collision_gas_type = payload
			elif row_lower.startswith('collision_energy:'):
				payload = cls.extract_playload(row)
				prefix, value, unit = cls._get_ce_level(payload)
				spectrum.collision_energy_prefix = prefix
				spectrum.collision_energy = value
				spectrum.collision_energy_unit = unit
			elif row_lower.startswith('ion_mode:'):
				payload = cls.extract_playload(row)
				spectrum.charge_type = cls._charge_type_keywords_mapping[payload.upper()]
			elif row_lower.startswith('inchikey:'):
				payload = cls.extract_playload(row)
				spectrum.inchikey = payload
			elif row_lower.startswith('smiles:') or row_lower.startswith('inchi:'):
				payload = cls.extract_playload(row)
				spectrum.smiles_or_inchi = payload
			elif row_lower.startswith('formula:'):
				payload = cls.extract_playload(row)
				spectrum.formula = payload
			elif row_lower.startswith('exactmass:'):
				payload = cls.extract_playload(row)
				try:
					spectrum.compound_exact_mass = float(payload)
				except:
					spectrum.compound_exact_mass = Standardization.NA
			elif row_lower.startswith('precursormz:'):
				payload = cls.extract_playload(row)
				try:
					spectrum.precursor_mz = float(payload)
				except:
					spectrum.precursor_mz = -1.0
			elif row_lower.startswith('cas#:'):
				_, cas_id, _, nist_id = row.split()
				#spectrum.compound_data_source_id = "NIST#" + nist_id
				spectrum.compound_data_source_id = 'CAS#:'+ cas_id
				spectrum.data_source_id = "NIST#" + nist_id
			elif row_lower.startswith('casno:'):
				pass
			elif row_lower.startswith('nistno') or row_lower.startswith('nist#'):
				payload = row.split()[1]
				spectrum.data_source_id = "NIST#" + payload
			#elif row_lower.startswith('db#:') and spectrum.data_source_id == Standardization.NA:
			#	_, database_id= row.split()
			#	spectrum.data_source_id = database_id
			elif row_lower.startswith('num peaks:'):
				# Peak data is after num of peaks
				is_peak = True
				# print(is_peak)
			elif row_lower.startswith('comments'):
				derivatization_type, derivatization_mass = cls._get_deriv_info(row)
				spectrum.derivatization_type = derivatization_type
				spectrum.derivatization_mass = derivatization_mass
				
		if post_process:
			spectrum.post_process()
			spectrum.remove_noise(centroid = True, centroid_mz_distance=0.5)

		#spectrum.set_mz_decimal_place()
		return spectrum

	@classmethod
	def extract_playload(cls, row):
		payload = Standardization.NA
		items = row.split(' ', 1)
		if len(items) == 2:
			payload = items[1]
		return payload

	@classmethod
	def read_sdf(cls, sdf_file, post_process=True, data_source = None):
		nist_df = PandasTools.LoadSDF(sdf_file, molColName='structure')
		spectra = []
		for _, row in nist_df.iterrows():
			if row['CHARGE'] not in ['1', '-1']:
				continue
			if row['IONIZATION'] not in [Standardization.ESI, Standardization.APPI, Standardization.APCI]:
				continue

			spectrum = Spectrum()
			spectrum.inchikey = row['INCHIKEY']
			spectrum.formula = row['FORMULA']
			spectrum.smiles_or_inchi = MolToSmiles(row['structure'])
			spectrum.adduct_type = row['PRECURSOR TYPE']
			spectrum.charge_type = row['ION MODE']
			spectrum.precursor_mz = row['PRECURSOR M/Z']
			spectrum.ms_level = row['SPECTRUM TYPE']
			spectrum.ionization_type = cls._get_ionization_type(row['IONIZATION'])

			spectrum.data_source = data_source if data_source is not None else Standardization.NA
			#spectrum.data_source_id = 'HR MSMS #' + row['ID']
			#print(row['ID'])
			spectrum.compound_data_source_id = row['NISTNO']
			
			spectrum.compound_exact_mass = row['EXACT MASS']
			spectrum.compound_data_source = 'NIST'

			# PRECURSOR TYPE
			spectrum.instrument_model = row['INSTRUMENT']
			spectrum.analyzer_type = cls._get_analyzer_type(row['INSTRUMENT TYPE'])
			if spectrum.chromatography_type == Standardization.NA:
				spectrum.chromatography_type = cls._get_chromatography_type(row['INSTRUMENT TYPE'])

			# Sample Inlet
			if spectrum.chromatography_type == Standardization.NA:
				spectrum.chromatography_type = cls._get_chromatography_type(row['SAMPLE INLET'])
			
			# IONIZATION
			spectrum.ionization_type = cls._get_ionization_type(row['IONIZATION'] )

			# COLLISION GAS
			spectrum.collision_gas_type = row['COLLISION GAS']
			# CE LEVEL
			collision_energy_str = row['COLLISION ENERGY']
			prefix, value, unit = cls._get_ce_level(collision_energy_str)
			spectrum.collision_energy_prefix = prefix
			spectrum.collision_energy = value
			spectrum.collision_energy_unit = unit

			peaks_str_list = row['MASS SPECTRAL PEAKS'].split('\n')
			for peaks_str in peaks_str_list:
				
				items = peaks_str.split(' ')
				mz, intensity =  items[0], items[1]
				spectrum.add_peak(float(mz), float(intensity))


			if post_process:
				spectrum.remove_noise(centroid = True, centroid_mz_distance=0.5)
				spectrum.post_process()

			#spectrum.set_mz_decimal_place()
			spectra.append(spectrum)
			
		return spectra

class GNPS_MSP(NIST_20_MSP):
	'''
	PEPMASS: (940.25, None)
	CHARGE: 1
	MSLEVEL: 2
	SOURCE_INSTRUMENT: LC-ESI-qTof
	FILENAME: 20111105_Anada_Ger_HoiamideB_MH940_qb.1.1..mgf
	SEQ: *..*
	IONMODE: positive
	ORGANISM: GNPS-LIBRARY
	NAME: Hoiamide B M+H
	PI: Gerwick
	DATACOLLECTOR: Amanda
	SMILES: CCC[C@@H](C)[C@@H]([C@H](C)[C@@H]1[C@H]([C@H](Cc2nc(cs2)C3=N[C@](CS3)(C4=N[C@](CS4)(C(=O)N[C@H]([C@H]([C@H](C(=O)O[C@H](C(=O)N[C@H](C(=O)O1)[C@@H](C)O)[C@@H](C)CC)C)O)[C@@H](C)CC)C)C)OC)C)O
	INCHI: InChI=1S/C45H73N5O10S3/c1-14-17-24(6)34(52)26(8)37-25(7)30(58-13)18-31-46-29(19-61-31)39-49-45(12,21-62-39)43-50-44(11,20-63-43)42(57)48-32(22(4)15-2)35(53)27(9)40(55)59-36(23(5)16-3)38(54)47-33(28(10)51)41(56)60-37/h19,22-28,30,32-37,51-53H,14-18,20-21H2,1-13H3,(H,47,54)(H,48,57)/t22-,23-,24+,25-,26-,27+,28+,30-,32-,33-,34-,35-,36-,37-,44+,45+/m0/s1
	INCHIAUX: N/A
	PUBMED: N/A
	SUBMITUSER: mwang87
	LIBRARYQUALITY: 1
	SPECTRUMID: CCMSLIB00000001548
	SCANS: 1
	COMPOUND_NAME: Hoiamide B
	ADDUCT: [M+H]+
	PRECURSOR_MZ: 940.25
	PARENT_MASS: [939.242724]
	INCHIKEY: KNGPFNUOXXLKCN-ZNCJFREWSA-N
	278.049927	  35793.0
	278.957642	  47593.0
	281.258667	  95495.0
	291.996094	  115278.0
	293.827637	  91752.0
	'''

	@classmethod
	def read(cls, spectrum_strs, include_peak_annotation=False, post_process=True):
		'''
		TODO
		Add code for CID and HCD
		Add code for CE level and unit
		check code for GC nand LC
		'''
		spectrum = Spectrum()
		is_peak = False
		# print(spectrum_strs)
		for row in spectrum_strs:
			row_lower = row.lower()
			# print(row_lower)

			try:
				peak_data = row.split()
				mass_str = peak_data[0]
				intensity_str = peak_data[1]
				annotation_str = Standardization.NA
				mass, intensity = float(mass_str), float(intensity_str)
			except:
				pass
			else:
				spectrum.add_peak(mass, intensity, annotation_str)

			if row_lower.startswith('compound_name:'):
				payload = cls.extract_playload(row)
				spectrum.compound_name = payload
			elif row_lower.startswith('adduct:'):
				payload = cls.extract_playload(row)
				spectrum.adduct_type = payload
			elif row_lower.startswith('mslevel:'):
				payload = cls.extract_playload(row)
				if payload == '1':
					spectrum.ms_level = Standardization.MS1
				if payload == '2':
					spectrum.ms_level = Standardization.MS2
			elif row_lower.startswith('source_instrument:'):
				payload = cls.extract_playload(row)
				spectrum.instrument_model = payload
				spectrum.analyzer_type = cls._get_analyzer_type(payload)
				if spectrum.chromatography_type == Standardization.NA:
					spectrum.chromatography_type = cls._get_chromatography_type(payload)
				spectrum.ionization_type = cls._get_ionization_type(payload)
			elif row.startswith('ionmode:'):
				payload = cls.extract_playload(row)
				spectrum.charge_type = cls._charge_type_keywords_mapping[payload.upper()]
			elif row_lower.startswith('inchikey:'):
				payload = cls.extract_playload(row)
				spectrum.inchikey = payload
			elif row_lower.startswith('smiles:') or row_lower.startswith('inchi:'):
				payload = cls.extract_playload(row)
				spectrum.smiles_or_inchi = payload
			elif row_lower.startswith('precursor_mz:'):
				payload = cls.extract_playload(row)
				try:
					spectrum.precursor_mz = float(payload)
				except:
					spectrum.precursor_mz = Standardization.NA
			elif row_lower.startswith('organism:'):
				# Peak data is after num of peaks
				payload = cls.extract_playload(row)
				spectrum.data_source = payload
			elif row_lower.startswith('spectrumid:'):
				# Peak data is after num of peaks
				payload = cls.extract_playload(row)
				spectrum.data_source_id = payload

		if post_process:
			spectrum.post_process()
			spectrum.remove_noise(centroid = True, centroid_mz_distance=0.5)

		#spectrum.set_mz_decimal_place()
		return spectrum


class PNNL_MSP(NIST_14_MSP):
	@classmethod
	def read(cls, spectrum_strs, include_peak_annotation=True, post_process=True):
		spectrum = super.read(spectrum_strs, include_peak_annotation, post_process)
		for row in spectrum_strs:
			row_lower = row.lower()
			if row_lower.startswith('name:'):
				payloads = row.split('; ')
				spectrum.compound_name = payloads[0]
				spectrum.adduct_type = payloads[1]
				# print(payloads)
				# spectrum.compound_name = payload

			if row_lower.startswith('comment:'):
				payloads = row.split('; ')
				collision_method_sr = payloads[0].lower()

				if 'cid' == collision_method_sr:
					spectrum.collision_method = Standardization.CID
				elif 'hcd' == collision_method_sr:
					spectrum.collision_method = Standardization.HCD

				spectrum.instrument_model = payloads[1]

		return spectrum