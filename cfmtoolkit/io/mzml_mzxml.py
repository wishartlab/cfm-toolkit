from cfmtoolkit.datastructures import Spectrum
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities

class MzmlMzxml:
	@classmethod
	def get_ms2(cls,spectra_file:str,
					  target_mz: float = None,
					  abs_tol=0.01,
					  ppm_tol=10, 
					  target_rt: float = None,
					  adduct_type = None,
					  postprocess = True,
					  min_peak_count = 0):
		"""_summary_

		Args:
			spectra_file (str): _description_
			target_mz (float, optional): filter by 0.01Da +- target_mz.  Ignored if None, Defaults to None.
			target_rt (float, optional):  filter by 0.1TimeUnit +- target_rt. Ignored if None, Defaults to None.
			min_peak_count (int, optional): _description_. Defaults to 3.

		Raises:
			TypeError: _description_

		Returns:
			_type_: _description_
		"""
		import pyopenms as oms
		exp = oms.MSExperiment()
		if spectra_file.lower().endswith("mzxml"):
			oms.MzXMLFile().load(spectra_file, exp)
		elif spectra_file.lower().endswith("mzml"):
			oms.MzMLFile().load(spectra_file, exp)
		else:
			raise TypeError("file ext need in mzXML or mzml")
		
		#print(f"> target_mz {target_mz}, target_rt {target_rt}")
		spectra_list = []
		spectra = exp.getSpectra()
		#print(target_mz, target_rt)
		for ms_spectrum in spectra:
			ms_level = ms_spectrum.getMSLevel()
			#print(ms_level)
			if ms_level != 2:
				continue
			if ms_spectrum.size() <= min_peak_count:
				continue
			
			precursor = ms_spectrum.getPrecursors()[0]
			precursor_mz = precursor.getMZ()
			rt = ms_spectrum.getRT()

			mz_tol = Utilities.get_mass_tol(target_mz, abs_tol, ppm_tol)
			if target_mz is not None and abs(target_mz - precursor_mz) > mz_tol:
				continue
			
			if target_rt is not None and abs(target_rt - rt) > 0.01:
				#print(precursor_mz, rt, target_rt)
				continue
			#print(precursor_mz, rt, target_rt)
			peaks_data = ms_spectrum.get_peaks()
			# assume we only have one precursor

			spectrum = Spectrum()
			spectrum.add_peaks(peaks_data[0].tolist(),peaks_data[1].tolist())
			spectrum.rt = rt
			spectrum.rt_unit = Standardization.RT_SEC
			spectrum.precursor_mz = precursor_mz

			try:
				spectrum.collision_energy = float(precursor.getMetaValue("collision energy"))
			except Exception as e:
				spectrum.collision_energy = Standardization.NA
				print("MzmlMzxml, get_ms2, get collision_energy",e)
				
			if postprocess:
				spectrum.normalize()
				spectrum.remove_noise(1.0, centroid = True)

			if adduct_type is not None:
				spectrum.adduct_type = adduct_type
			spectrum.compute_compound_exact_mass()
			if spectrum.get_num_peaks() >= min_peak_count:
				spectra_list.append(spectrum)
		#print(f"{len(spectra_list)} spectra found")
		return spectra_list