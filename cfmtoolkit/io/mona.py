import json
from cfmtoolkit.utils import Utilities
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.datastructures import Spectrum
import re

class MoNA(object):
	@staticmethod
	def parse_mona_json(mona_json_file, analyzer_type, default_ce_unit, adduct_filters = None) -> list[Spectrum]:
		mona_json = json.load(open(mona_json_file,  encoding='utf-8'))
		spectra_list = []

		for spectrum_json in mona_json:
			#print(spectrum_json)
			inchikey = None
			inchi = None
			
			try:
				inchikey = spectrum_json["compound"][0]['inchiKey']
			except KeyError as e:
				pass

			try:
				inchi = spectrum_json["compound"][0]['inchi']
			except KeyError as e:
				pass

			exact_mass = None
			precursor_mass = None
			precursor_type = None
			collision_energy = None
			fragmentation_mode = None

			metadata_list = spectrum_json["compound"][0]['metaData'] + spectrum_json["metaData"]
			#print(metadata_list)
			
			for meta in metadata_list:
				#print(meta['name'])
				if meta['name'] == 'InChI':
					inchi = meta['value']
					#print(inchi)
				if meta['name'] == 'InChIKey':
					inchikey = meta['value']
				if meta['name'] == 'SMILES':
					smiles = meta['value']
				if meta['name'] == 'exact mass':
					exact_mass = meta['value']
				if meta['name'] == 'total exact mass' and exact_mass == None:
					exact_mass = meta['value']
				if meta['name'] == 'precursor mass':
					precursor_mass = meta['value']
				if meta['name'] == 'precursor m/z':
					precursor_mass = meta['value']
				if meta['name'] == 'precursor type':
					precursor_type = meta['value']
				if meta['name'] == 'collision energy':
					collision_energy =  meta['value']
				if meta['name'] == 'fragmentation mode':
					fragmentation_mode =  meta['value']

			if inchi is None:
				inchi = ""
			elif not inchi.startswith('InChI=') and inchi !="" :
				inchi = 'InChI=' + inchi

			spectrum_id = spectrum_json["id"]
	
			if precursor_mass is None and precursor_type is not None and exact_mass is None:
				exact_mass = Utilities.ion_mass_to_netural_mass(precursor_mass, precursor_type)

			if exact_mass is None and inchi != "":
				try:
					exact_mass = Utilities.get_mol_mw_by_chemid(inchi[1:-1])
				except:
					exact_mass = -1
			elif exact_mass is None and inchi == "":
				exact_mass = -1

			if precursor_type is None or inchikey is None or collision_energy is None:
				continue
			
			if adduct_filters is not None and precursor_type not in adduct_filters:
				continue
			
			if "ramp" in collision_energy.lower() or 'stepped'  in collision_energy.lower():
				continue

			spectrum = Spectrum()
			if precursor_type.endswith('+'):
				spectrum.charge_type = Standardization.Pos
			else:
				spectrum.charge_type = Standardization.Neg

			spectrum.smiles_or_inchi = inchi
			spectrum.inchikey = inchikey
			spectrum.compound_exact_mass = exact_mass
			spectrum.data_source_id = spectrum_id
			spectrum.adduct_type = precursor_type
			formula = Utilities.get_formula_by_chemid(inchi)
			spectrum.formula = formula
			spectrum.analyzer_type = analyzer_type

			#print(collision_energy)
			if analyzer_type == Standardization.QTOF:
				spectrum.collision_method = Standardization.CID
			if analyzer_type == Standardization.Orbitrap:
				spectrum.collision_method = Standardization.HCD

			ce_found = False
			ce_value_strs = re.findall(r'^(\d+\.?\d?)\s?(eV|ev)', collision_energy)
			if len(ce_value_strs):
				spectrum.collision_energy = float(ce_value_strs[0][0])
				spectrum.collision_energy_unit = 'eV'
				ce_found = True

			if not ce_found:
				ce_value_strs = re.findall(r'^(\d+|\d+\.\d+)\s?(\(NCE\)|HCD|\s?\(nominal\)|%\s?\(nominal\)|%)', collision_energy)
				if len(ce_value_strs):
					#print(ce_value_strs)
					spectrum.collision_energy = float(ce_value_strs[0][0])
					spectrum.collision_energy_unit = '%'
					ce_found = True

			if not ce_found and default_ce_unit is not None:
				try:
					cv_value = float(collision_energy)
				except:
					pass
				else:
					spectrum.collision_energy = cv_value
					spectrum.collision_energy_unit = default_ce_unit

			# add peaks
			mzs = []
			intens = []
			for peak_str in spectrum_json['spectrum'].split():
				mz,inten = peak_str.split(":")
				mzs.append(float(mz))
				intens.append(float(inten))
			spectrum.add_peaks(mzs,intens)
			spectra_list.append(spectrum)

		return spectra_list