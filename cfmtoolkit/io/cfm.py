

import copy
import os.path
import csv
from typing import Dict, List, Optional

from rdkit import Chem
from rdkit.Chem.rdMolDescriptors import CalcMolFormula
from rdkit.Chem.Descriptors import ExactMolWt

from cfmtoolkit.standardization import Standardization
from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from cfmtoolkit.utils import Utilities

'''
Class to read Cfm format 
'''
class CFM:
	def __init__(self):
		self._id_mol_map = {}

	_ev_str_map = {
		"energy0": 10,
		"low": 10,
		"energy1": 20,
		"med": 20,
		"energy2": 40,
		"high": 40
	}

	_spectrum_key_map = {
		'energy0': 'low',
		'energy1': 'med',
		'energy2': 'high',
		'low': 'low',
		'med': 'med',
		'high': 'high',
	}

	def get_mols_from_file(self, filename, skip_first_row = True):
		with open(filename, 'r') as mol_list_input:
			for i, row in enumerate(mol_list_input):
				if i == 0 and skip_first_row:
					continue
				data = row.split()
				id, smiles_or_inchi = data[0], data[1]
				self._id_mol_map[id] = smiles_or_inchi

	def get_mols_from_csv_file(self, filename, skip_first_row = True):
		with open(filename, 'r') as mol_list_input:
			csv_reader = csv.reader(mol_list_input)
			for i, row in enumerate(csv_reader):
				if i == 0 and skip_first_row:
					continue
				mol_id, smiles_or_inchi = row[0], row[-2]
				self._id_mol_map[mol_id] = smiles_or_inchi

	@classmethod
	def _get_ev_from_str(cls, inpur_str: str) -> float:
		return cls._ev_str_map[inpur_str]

	def read_cfmid2_esi_spectra(self, file_path: str, meta_data: dict = None, check_id_map=True) -> Optional[CfmSpectra]:
		"""[Legacy Method that read CFM data and check also check id map, use read_esi_spectra_without_meta instead]

		Args:
			file_path (str): [description]
			meta_data (dict, optional): [description]. Defaults to None.
			check_id_map (bool, optional): [description]. Defaults to True.

		Returns:
			CfmSpectra: [description]
		"""

		# Determine chem id for this mol
		spectra_id = os.path.basename(file_path).rsplit('.', 2)[0]
		if '_' in spectra_id:
			items = spectra_id.split('_')
			spectra_id = items[0]

		if spectra_id not in self._id_mol_map and check_id_map is True:
			print('Warning:', spectra_id, 'not exists')
			return None
		
		if meta_data is None:
			meta_data = {}
		else:
			meta_data = copy.deepcopy(meta_data)

		smiles_or_inchi = Standardization.NA
		if spectra_id in self._id_mol_map:
			smiles_or_inchi = self._id_mol_map[spectra_id]
			meta_data['smiles_or_inchi'] = smiles_or_inchi

		# read file
		spectra = self.read_esi_spectra_with_meta(
			file_path, meta_data)

		return spectra

	'''
	Class Method that read CFM training data and check also check id map
	'''
	# Metlin only methods

	def read_metlin_esi_spectra(self, file_path: str, meta_data: dict = None, check_id_map=True) -> Optional[CfmSpectra]:
		"""[Legacy Method that read Metlin Training Data and check also check id map]
		Args:
			file_path (str): [description]
			meta_data (dict): [description]
			check_id_map (bool, optional): [description]. Defaults to True.

		Returns:
			CfmSpectra: [description]
		"""
		# Determine chem id for this mol
		spectra_id = os.path.basename(file_path).rsplit('.', 2)[0]

		# print(id)
		if spectra_id not in self._id_mol_map and check_id_map is True:
			return None

		smiles_or_inchi = Standardization.NA
		if spectra_id in self._id_mol_map:
			smiles_or_inchi = self._id_mol_map[spectra_id]

		# setup meta_data
		if meta_data is None:
			meta_data = {}
		else:
			meta_data = copy.deepcopy(meta_data)

		# read file
		meta_data['data_source_id'] = spectra_id.split('_')[1]
		meta_data['smiles_or_inchi'] = smiles_or_inchi
		if 'Metlin' in spectra_id:
			meta_data['data_source_id'] = spectra_id

		spectra = self.read_esi_spectra_with_meta(
			file_path, meta_data)

		return spectra

	# read file
	@classmethod
	def read_esi_spectra_without_meta(cls, file_path: str, sort_peaks = False, normalize_peaks = False) -> CfmSpectra:
		"""[summary]

		Args:
			file_path (str): [description]

		Returns:
			CfmSpectra: [description]
		"""
		cfm_spectra = CfmSpectra()
		with open(file_path, 'r') as cfm_file:
			input_strs = cfm_file.read().splitlines()
			spectra_list = cls._parse_esi_spectra_from_str(input_strs, sort_peaks = sort_peaks, normalize_peaks = normalize_peaks)
			for spectrum_key in spectra_list:
				cfm_spectra.add_spectrum(
					spectrum_key, spectra_list[spectrum_key])
		return cfm_spectra

	# read file
	@classmethod
	def read_esi_spectra_with_meta(cls, file_path: str, meta_data: Dict = None, sort_peaks = False, normalize_peaks = False) -> CfmSpectra:
		"""[summary]

		Args:
			file_path (str): [description]
			meta_data (Dict, optional): [description]. Defaults to None.

		Returns:
			CfmSpectra: [description]
		"""
		cfm_spectra = CfmSpectra()
		if meta_data is None:
			meta_data = {}
		else:
			meta_data = copy.deepcopy(meta_data)

		with open(file_path, 'r') as cfm_file:
			input_strs = cfm_file.read().splitlines()
			spectra_list = cls._parse_esi_spectra_from_str(
				input_strs, meta_data, sort_peaks, normalize_peaks)
			for spectrum_key in spectra_list:
				cfm_spectra.add_spectrum(
					spectrum_key, spectra_list[spectrum_key])
				
		cfm_spectra = cls._fill_meta(cfm_spectra, meta_data)
		return cfm_spectra

		# read file
	@classmethod
	def parse_esi_spectra_str_with_meta(cls, cfm_str: str | list[str], meta_data: Dict = None, sort_peaks = False, normalize_peaks = False) -> CfmSpectra:
		"""[summary]

		Args:
			file_path (str): [description]
			meta_data (Dict, optional): [description]. Defaults to None.

		Returns:
			CfmSpectra: [description]
		"""
		cfm_spectra = CfmSpectra()
		if meta_data is None:
			meta_data = {}
		else:
			meta_data = copy.deepcopy(meta_data)

		#with open(file_path, 'r') as cfm_file:
		if type(cfm_str) is str:
			input_strs = cfm_str.split()
		else:
			input_strs = cfm_str
		
		spectra_list = cls._parse_esi_spectra_from_str(
			input_strs, meta_data, sort_peaks, normalize_peaks)
		for spectrum_key in spectra_list:
			cfm_spectra.add_spectrum(
				spectrum_key, spectra_list[spectrum_key])

		cfm_spectra = cls._fill_meta(cfm_spectra, meta_data)
		return cfm_spectra
	
	@classmethod
	def _fill_meta(cls, cfm_spectra: CfmSpectra, meta_data: Dict) -> CfmSpectra:

		cfm_spectra.adduct_type = meta_data['adduct_type'] if 'adduct_type' in meta_data else Standardization.NA
		cfm_spectra.ionization_type = meta_data['ionization_type'] if 'ionization_type' in meta_data else Standardization.NA
		cfm_spectra.data_source = meta_data['data_source'] if 'data_source' in meta_data else Standardization.NA
		cfm_spectra.charge_type = meta_data['charge_type'] if 'charge_type' in meta_data else Standardization.NA
		cfm_spectra.chromatography_type = meta_data[
			'chromatography_type'] if 'chromatography_type' in meta_data else Standardization.NA
		cfm_spectra.ms_level = meta_data[
			'ms_level'] if 'ms_level' in meta_data else Standardization.NA

		smiles_or_inchi = Standardization.NA if 'smiles_or_inchi' not in meta_data else meta_data['smiles_or_inchi']		
		inchikey = Standardization.NA if 'inchikey' not in meta_data else meta_data['inchikey']
		formula = Standardization.NA  if 'formula' not in meta_data else meta_data['formula']
		theoretical_precursor_mz = Standardization.NA  if 'precursor_mz' not in meta_data else meta_data['precursor_mz']
		compound_exact_mass = Standardization.NA if 'compound_exact_mass' not in meta_data else meta_data['compound_exact_mass']

		if theoretical_precursor_mz is Standardization.NA and compound_exact_mass is not Standardization.NA:
			theoretical_precursor_mz = Utilities.neutral_mass_to_ion_mass(compound_exact_mass, cfm_spectra.adduct_type)		
		if theoretical_precursor_mz is not Standardization.NA and compound_exact_mass is Standardization.NA:
			compound_exact_mass = Utilities.ion_mass_to_neutral_mass(theoretical_precursor_mz, cfm_spectra.adduct_type)
			
		if smiles_or_inchi != Standardization.NA and \
			(inchikey == Standardization.NA \
				or formula == Standardization.NA \
				or theoretical_precursor_mz == Standardization.NA \
				or compound_exact_mass == Standardization.NA):
			mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
			if inchikey == Standardization.NA:
				inchikey = Chem.inchi.MolToInchiKey(mol)
			if formula == Standardization.NA:
				formula = CalcMolFormula(mol)
			if compound_exact_mass == Standardization.NA:
				compound_exact_mass = ExactMolWt(mol)
			if  cfm_spectra.adduct_type != Standardization.NA and theoretical_precursor_mz == Standardization.NA:
				theoretical_precursor_mz = Utilities.neutral_mass_to_ion_mass(compound_exact_mass, cfm_spectra.adduct_type)
		
		cfm_spectra.smiles_or_inchi = smiles_or_inchi
		cfm_spectra.inchikey = inchikey
		cfm_spectra.formula = formula
		cfm_spectra.compound_exact_mass = compound_exact_mass
		cfm_spectra.precursor_mz = theoretical_precursor_mz
		cfm_spectra.compound_data_source = meta_data['compound_data_source'] if 'compound_data_source' in meta_data else Standardization.NA
		cfm_spectra.compound_sample_frequency = meta_data['compound_sample_frequency'] if 'compound_sample_frequency' in meta_data else 1
		   
		# sign meta_data
		for spectrum_key in CfmSpectra._spectra_keys:
			spectrum: Spectrum = cfm_spectra.get_spectrum(spectrum_key)

			spectrum.adduct_type = meta_data['adduct_type'] if 'adduct_type' in meta_data else Standardization.NA
			spectrum.analyzer_type = meta_data['analyzer_type'] if 'analyzer_type' in meta_data else Standardization.NA
			spectrum.ms_level = meta_data['ms_level'] if 'ms_level' in meta_data else Standardization.NA
			spectrum.data_source = meta_data['data_source'] if 'data_source' in meta_data else Standardization.NA
			spectrum.data_source_id = meta_data['data_source_id'] if 'data_source_id' in meta_data else Standardization.NA
			spectrum.charge_type = meta_data['charge_type'] if 'charge_type' in meta_data else Standardization.NA
			spectrum.chromatography_type = meta_data[
				'chromatography_type'] if 'chromatography_type' in meta_data else Standardization.NA
			spectrum.ionization_type = meta_data['ionization_type'] if 'ionization_type' in meta_data else Standardization.NA
			spectrum.compound_data_source = meta_data['compound_data_source'] if 'compound_data_source' in meta_data else Standardization.NA
			spectrum.compound_data_source_id = meta_data['compound_data_source_id'] if 'compound_data_source_id' in meta_data else Standardization.NA
			spectrum.compound_sample_frequency = meta_data['compound_sample_frequency'] if 'compound_sample_frequency' in meta_data else 1
			
			spectrum.inchikey = inchikey
			spectrum.smiles_or_inchi = smiles_or_inchi
			spectrum.formula = formula
			spectrum.compound_exact_mass = compound_exact_mass
			spectrum.precursor_mz = theoretical_precursor_mz
			#spectrum.fill_formula()
			#spectrum.compute_compound_exact_mass()
			#spectrum.compute_precursor_mz()

		return cfm_spectra

	@classmethod
	def read_esi_spectra_list_with_meta(cls, file_path: str, meta_data: Dict = None, sort_peaks = False, normalize_peaks = False) -> List[CfmSpectra]:
		"""[summary]

		Args:
			file_path (str): [description]

		Returns:
			List[CfmSpectra]: [description]
		"""
		cfm_spectra_list = []

		def process_block(input_strs, meta_data,sort_peaks, normalize_peaks):
			if meta_data is None:
				current_meta_data = {}
			else:
				current_meta_data = copy.deepcopy(meta_data)

			spectra_list = cls._parse_esi_spectra_from_str(input_strs, current_meta_data,sort_peaks, normalize_peaks)
			
			cfm_spectra = CfmSpectra()
			for spectrum_key in spectra_list:
				cfm_spectra.add_spectrum(spectrum_key, spectra_list[spectrum_key])
			cfm_spectra = cls._fill_meta(cfm_spectra, current_meta_data)
			return cfm_spectra
			#cfm_spectra_list.append(cfm_spectra)
			 
		with open(file_path, 'r') as cfm_file:
			input_strs = []
			for row in cfm_file:
				if len(input_strs) != 0 and "spectra" in row.lower():
					cfm_spectra = process_block(input_strs, meta_data,sort_peaks, normalize_peaks)
					cfm_spectra_list.append(cfm_spectra)
					input_strs = []
				input_strs.append(row.rstrip())
			
			if len(input_strs) != 0:
				cfm_spectra = process_block(input_strs, meta_data,sort_peaks, normalize_peaks)
				cfm_spectra_list.append(cfm_spectra)
				
		return cfm_spectra_list


	@classmethod
	def _parse_esi_spectra_from_str(cls, input_strs: List[str], meta_data: dict = None, sort_peaks = False, normalize_peaks = False) -> Dict:
		spectrums = {}
		input_strs.append('')
		spectrum_key = None

		spectrum_end = False
		if meta_data is not None:
			if 'adduct_type' not in meta_data:
				meta_data['adduct_type'] = Standardization.NA
			if 'charge_type' not in meta_data:
				meta_data['charge_type'] = Standardization.NA
			if 'ms_level' not in meta_data:
				meta_data['ms_level'] = Standardization.NA
			if 'ionization_type' not in meta_data:
				meta_data['ionization_type'] = Standardization.ESI
			if 'smiles_or_inchi' not in meta_data:
				meta_data['smiles_or_inchi'] = Standardization.NA
			if 'formula' not in meta_data:
				meta_data['formula'] = Standardization.NA
			if 'precursor_mz' not in meta_data:
				meta_data['precursor_mz'] = Standardization.NA
			if 'compound_exact_mass' not in meta_data:
				meta_data['compound_exact_mass'] = Standardization.NA
			if 'compound_data_source' not in meta_data:
				meta_data['compound_data_source'] = Standardization.NA

		fragment_annotations = {}
		spectrum_strs = {}
		peak_strs = [] 
		for row in input_strs:
			# comments
			if not spectrum_end:
				if row.startswith('#'):
					if meta_data is None:
						continue
					if row.lower().startswith('#in-silico') or row.lower().endswith('spectra'):
						# NOTE THIS IS FOR MSRB
						row = row.replace('<',' ').replace('>',' ')
						items = row.split()
						adduct_type = items[-2]
						meta_data['adduct_type']  = adduct_type
						if adduct_type.endswith('+'):
							meta_data['charge_type'] = Standardization.Pos
						elif adduct_type.endswith('-'):
							meta_data['charge_type'] = Standardization.Neg

						# Fill rest of data
						if 'MS/MS' in items[-3].upper():
							meta_data['ms_level'] = Standardization.MS2
						elif 'MS' in items[-3].upper():
							meta_data['ms_level'] = Standardization.MS1

						if 'ESI' == items[-3].upper():
							meta_data['ionization_type'] = Standardization.ESI
						elif 'EI'  in items[-3].upper():
							meta_data['ionization_type'] = Standardization.EI

					# Only fill if no info is provided
					def need_fill_meta(meta_data, keyword):
						#print(keyword, keyword in meta_data and meta_data[keyword] in [Standardization.NA, None,''])
						return (keyword not in meta_data or (keyword in meta_data and meta_data[keyword] in [Standardization.NA, None,'']))
					if row.startswith('#PREDICTED BY') and need_fill_meta(meta_data, 'data_source'):
						meta_data['data_source'] = row[len('#PREDICTED BY '):]
					elif row.startswith('#DATASOURCE=') and need_fill_meta(meta_data, 'data_source'):
						meta_data['data_source'] = row[len('#DATASOURCE: '):]
					if row.startswith('#SMILES=') and need_fill_meta(meta_data, 'smiles_or_inchi'): 
						meta_data['smiles_or_inchi'] = row[len('#SMILES='):]
					if row.startswith('#InChiKey=') and need_fill_meta(meta_data, 'inchikey'):
						meta_data['inchikey'] = row[len('#InChiKey='):]
					if row.startswith('#InChI=') and need_fill_meta(meta_data, 'smiles_or_inchi'):
						meta_data['smiles_or_inchi'] = row[len('#'):]
					if row.startswith('#ExtractMass') and need_fill_meta(meta_data, 'compound_exact_mass'):
						meta_data['compound_exact_mass'] = float(row[len('#ExtractMass='):])
					if row.startswith('#PMass') and need_fill_meta(meta_data, 'precursor_mz'):
						meta_data['precursor_mz'] = float(row[len('#PMass='):])
					if row.startswith('#ChargeType') and need_fill_meta(meta_data, 'charge_type'):
						meta_data['charge_type'] = row[len('#ChargeType='):]
					if row.startswith('#Formula') and need_fill_meta(meta_data, 'formula'):
						meta_data['formula'] = row[len('#Formula='):]
					if row.startswith('#ID') and need_fill_meta(meta_data, 'compound_data_source'):
						meta_data['compound_data_source'] = row[len('#ID='):]

				elif row.lower() in ['energy0', 'energy1', 'energy2', 'low', 'med', 'high', '']:
					spectrum_title = row.lower()
					# parse the existing data
					if spectrum_title != 'low' and spectrum_title != 'energy0':
						spectrum_strs[spectrum_key] = peak_strs
						peak_strs = []

					# create new one
					if spectrum_title != '':
						spectrum_key = CFM._spectrum_key_map[spectrum_title]
					else :
						spectrum_end = True
				else:
					peak_strs.append(row)
			elif row != '':
				# read annotations
				annotation_items = row.split()
				if len(annotation_items) >= 3:
					fragment_id = annotation_items[0]
					fragment_structure = annotation_items[2]
					if fragment_id not in fragment_annotations:
						fragment_annotations[fragment_id] = fragment_structure

		for spectrum_key in spectrum_strs:
			spectrum = Spectrum()
			spectrum.collision_energy_unit = "ev"
			spectrum.collision_method = Standardization.CID
			spectrum.collision_energy = cls._get_ev_from_str(spectrum_key)

			cls._fill_peaks(spectrum, spectrum_strs[spectrum_key], fragment_annotations)
			#if spectrum.peaks is None or len(spectrum.peaks) == 0:
			#	raise AttributeError(f"Incomplete CFM Spectra, missing peaks for {spectrum_key} spectrum from input {input_strs}")
			if sort_peaks:
				spectrum.sort_by_mz()
			if normalize_peaks:
				spectrum.normalize()
			
			spectrum.set_mz_decimal_place()

			spectrums[spectrum_key] = spectrum

		return spectrums

	@classmethod
	def _fill_peaks(cls, spectrum: Spectrum, strs: List, fragment_annotations: Dict[str, str]):
		mzs = []
		intensities = []
		annotations = []
		for row in strs:
			if str != '':
				if '\t' in row:
					mz_intensity = row.split('\t')
				else:
					mz_intensity = row.split()
				#print(mz_intensity)
				mz, intensity = float(mz_intensity[0]), float(mz_intensity[1])
				mzs.append(mz)
				intensities.append(intensity)

				annotation = Standardization.NA
				if len(mz_intensity) > 2  and mz_intensity[2] in fragment_annotations:
					#print(mz_intensity[2])
					#print(fragment_annotations[mz_intensity[2]])
					annotation = fragment_annotations[mz_intensity[2]]
				annotations.append(annotation)

		spectrum.add_peaks(mzs, intensities, annotations)
