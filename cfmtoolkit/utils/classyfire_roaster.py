import json
import os.path

import requests
import time
import math

from typing import Dict
from itertools import islice

# use with Resolver as resolver to use cache
class ClassyfireRoaster(object):
	job_submission_url = 'http://classyfire.wishartlab.com/queries'
	job_status_url = 'http://classyfire.wishartlab.com/queries/{}/status.json'
	job_status_web_url = 'http://classyfire.wishartlab.com/queries/{}'
	job_results_url = 'http://classyfire.wishartlab.com/queries/{}.json?page={}'
	default_classyfire_paging_size = 10
	default_query_size = 200

	def __init__(self, cache_file_path: str = './classyfire_roaster_cache.json'):
		self._cached = {}
		self._cache_file_path = cache_file_path
		self._init_cache()

	def __enter__(self):
		return self

	def __exit__(self, exception_type, exception_value, traceback):
		# save cache on exit
		self._save_cache()

	def _init_cache(self):
		if os.path.isfile(self._cache_file_path):
			with open(self._cache_file_path) as infile:
				self._cached = json.load(infile)

	def _save_cache(self):
		with open(self._cache_file_path, "w+") as outfile:
			json.dump(self._cached, outfile, indent=2)

	def get_classyfire_by_structure(self, structures:Dict[str,str],label:str = '', sleep_interval = 6, verbose = False):
		if verbose:
			print(len(structures),"compound need Classyfire")

		results = {}
		query_structures = {}
		for id_str in structures:
			if id_str not in self._cached:
				query_structures[id_str] = structures[id_str]
			else:
				results[id_str] = self._cached[id_str]
		
		if verbose:
			print(len(query_structures),"compound need query Classyfire server")

		
		if len(query_structures) > 0:
			new_results = self._query_classyfire_by_structure(query_structures, label, sleep_interval, verbose)
			self._cached.update(new_results)
			results.update(new_results)
		return results

	@classmethod
	def _query_classyfire_by_structure(cls, structures:Dict[str,str],label:str = '', sleep_interval = 6, verbose = False):
		query_type = 'STRUCTURE'
		query_input = ''
		for id_str in structures:
			query_input += '{}\t{}\n'.format(id_str,structures[id_str])
		query_size =  len(structures)
		if verbose:
			print('Prepare query for', query_size, 'structures')

		headers = {'Content-Type': 'application/json', 'Accept':'application/json'}
		query_data = json.dumps({
			"query_type": query_type,
			"query_input": query_input,
			"label": label
		})

		# send request
		job_finished = False
		try:
			response = requests.post(cls.job_submission_url,headers=headers,data=query_data)
		except requests.exceptions.RequestException as e:
			# do something
			print(cls.job_submission_url, e)
		else:
			if response.status_code != 201:
				return None
			job_submission_response = response.json()
			job_id = job_submission_response['id']
			job_finished = job_submission_response['finished_at'] != None
			if verbose:
				print('Classyfire Job ID:', job_id, cls.job_status_web_url.format(job_id))

		# wait till job is finished
		while job_finished is False:
			#print(job_id)
			time.sleep(sleep_interval)
			try:
				#NOTE, For some reason return value is a json format while url does say json
				status_response = requests.get(cls.job_status_url.format(job_id))
			except requests.exceptions.RequestException as e:
				print(cls.job_status_url, e)
			else:
				job_finished = (status_response.text == 'Done')

		results = {}
		num_pages = int(math.ceil((query_size/cls.default_classyfire_paging_size)))
		if verbose:
			print("{} Classyfire queries required".format(num_pages))
		for page_id in range(1, num_pages + 1):
			time.sleep(1)
			if verbose:
				print('Get results data for page', page_id)
			#get results   
			try:
				#NOTE, For some reason return value is a json format while url does say json
				results_response = requests.get(cls.job_results_url.format(job_id,page_id)).json()
			except requests.exceptions.RequestException as e:
				print(cls.job_results_url, e)
			else:
				# parsing results
				# print(results_response)
				# if 'entities' not in results_response:
				#	print(results_response)
				if response.status_code != 201:
					print(response)
					return {}
				if 'entities' in results_response:
					entities = results_response['entities']
				else:
					print(results_response)
				if verbose:
					print('Process results for', len(entities), 'structures')

				for entity in entities:
					entity_id = entity['identifier']
					entity_kingdom = None if entity['kingdom'] == None else entity['kingdom']['name']
					entity_superclass = None if entity['superclass'] == None else entity['superclass']['name']
					entity_class = None if entity['class'] == None else entity['class']['name']
					entity_subclass = None if entity['subclass'] == None else entity['subclass']['name']

					results[entity_id] = {
						'kingdom':entity_kingdom,
						'superclass':entity_superclass,
						'class':entity_class,
						'subclass':entity_subclass,
					}
		if verbose:
			print('Got results for', len(results), 'structures')
		return results