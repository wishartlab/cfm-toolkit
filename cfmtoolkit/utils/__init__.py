from .resolvers import NameResolver, SmilesResolver, InchikeyResolver
from .utilities import Utilities
try:
	from .classyfire_roaster import ClassyfireRoaster
except ImportError as e:
	print("classyfire_roaster is not imported", e)