from cfmtoolkit.database import PgDBConfig

default_db_config = PgDBConfig(db_host="localhost", db_port="5432", db_usr="cfmdata", db_pwd="cfmdata",
							   db_name="cfmdata")
