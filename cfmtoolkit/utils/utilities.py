
from typing import Tuple, List
from re import findall

from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem.rdmolops import RemoveStereochemistry
from rdkit.Chem import  AllChem
from rdkit.DataStructs import FingerprintSimilarity

class Utilities:
	'''
	Static methods
	'''

	hydrogen_iso_mass = 1.007825
	proton_mass = 1.00727646677  #Corrected values (C. Amsler et al., "Review of Particle Physics" Physics Letters B667, 1 (2008))
	electron_iso_mass = 0.00054858
	ppm = 1e-6
	# https://fiehnlab.ucdavis.edu/staff/kind/metabolomics/ms-adduct-calculator/
	mass_to_positive_ion_mass = {
		"M+"			 : lambda mass:  mass - Utilities.electron_iso_mass,
		"M+3H"		   : lambda mass:  mass/3 + 1.007276 ,
		"M+2H+Na"		: lambda mass:  mass/3 + 8.334590 ,
		"M+H+2Na"		: lambda mass:  mass/3 + 15.7661904,
		"M+3Na"		  : lambda mass:  mass/3 + 22.989218,
		"M+2H"		   : lambda mass:  mass/2 + 1.007276 ,
		"M+H+NH4"		: lambda mass:  mass/2 + 9.520550 ,
		"M+H+Na"		 : lambda mass:  mass/2 + 11.998247,
		"M+H+K"		  : lambda mass:  mass/2 + 19.985217,
		"M+ACN+2H"	   : lambda mass:  mass/2 + 21.520550,
		"M+2Na"		  : lambda mass:  mass/2 + 22.989218,
		"M+2ACN+2H"	  : lambda mass:  mass/2 + 42.033823,
		"M+3ACN+2H"	  : lambda mass:  mass/2 + 62.547097,
		"M+H"			: lambda mass:  mass + 1.007276   ,
		"M+NH4"		  : lambda mass:  mass + 18.033823  ,
		"M+Na"		   : lambda mass:  mass + 22.989218  ,
		"M+CH3OH+H"	  : lambda mass:  mass + 33.033489  ,
		"M+K"			: lambda mass:  mass + 38.963158  ,
		"M+ACN+H"		: lambda mass:  mass + 42.033823  ,
		"M+IsoProp+H"	: lambda mass:  mass + 61.06534   ,
		"M+ACN+Na"	   : lambda mass:  mass + 64.015765  ,
		"M+2K+H"		 : lambda mass:  mass + 76.919040  ,
		"M+DMSO+H"	   : lambda mass:  mass + 79.02122   ,
		"M+2ACN+H"	   : lambda mass:  mass + 83.060370  ,
		"M+2Na-H"		: lambda mass:  mass + 44.971160  ,
		"M+IsoProp+Na+H" : lambda mass:  mass + 84.05511   ,
		"2M+H"		   : lambda mass:  2*mass + 1.007276 ,
		"2M+NH4"		 : lambda mass:  2*mass + 18.033823,
		"2M+Na"		  : lambda mass:  2*mass + 22.989218,
		"2M+3H2O+2H"	 : lambda mass:  2*mass + 28.02312 ,
		"2M+K"		   : lambda mass:  2*mass + 38.963158,
		"2M+ACN+H"	   : lambda mass:  2*mass + 42.033823,
		"2M+ACN+Na"	  : lambda mass:  2*mass + 64.015765,
		"M+Li"		   : lambda mass:  mass + 6.9400376247955,
		"M+H-H2O"		: lambda mass:  mass + 1.007276 - 18.010565 # Mass + H - H20
	}
	
	mass_to_negative_ion_mass = {
	"M-"			: lambda mass:  mass + Utilities.electron_iso_mass,
	"M-3H"		  : lambda mass:  mass/3 - 1.007276  ,
	"M-2H"		  : lambda mass:  mass/2 - 1.007276  ,
	"M-H20-H"	   : lambda mass:  mass - 19.01839	,
	"M-H"		   : lambda mass:  mass - 1.007276	,
	"M+Na-2H"	   : lambda mass:  mass + 20.974666   ,
	"M+Cl"		  : lambda mass:  mass + 34.969402   ,
	"M+K-2H"		: lambda mass:  mass + 36.948606   ,
	"M+FA-H"		: lambda mass:  mass + 44.998201   ,
	"M+Hac-H"	   : lambda mass:  mass + 59.013851   ,
	"M+Br"		  : lambda mass:  mass + 78.918885   ,
	"M+TFA-H"	   : lambda mass:  mass + 112.985586  ,
	"2M-H"		  : lambda mass:  2*mass - 1.007276  ,
	"2M+FA-H"	   : lambda mass:  2*mass + 44.998201 ,
	"2M+Hac-H"	  : lambda mass:  2*mass + 59.013851 ,
	"3M-H"		  : lambda mass:  3*mass - 1.007276  ,
	"M+HCOO"		: lambda mass:  mass + 44.998201  ,
	"M+CH3COO"	  : lambda mass:  mass + 59.0133
	}

	positive_ion_mass_to_mass = {
		"M+"			 : lambda mass:  mass + Utilities.electron_iso_mass,
		"M+3H"		   : lambda mass:  mass/3 - 1.007276 ,
		"M+2H+Na"		: lambda mass:  mass/3 - 8.334590 ,
		"M+H+2Na"		: lambda mass:  mass/3 - 15.7661904,
		"M+3Na"		  : lambda mass:  mass/3 - 22.989218,
		"M+2H"		   : lambda mass:  mass/2 - 1.007276 ,
		"M+H+NH4"		: lambda mass:  mass/2 - 9.520550 ,
		"M+H+Na"		 : lambda mass:  mass/2 - 11.998247,
		"M+H+K"		  : lambda mass:  mass/2 - 19.985217,
		"M+ACN+2H"	   : lambda mass:  mass/2 - 21.520550,
		"M+2Na"		  : lambda mass:  mass/2 - 22.989218,
		"M+2ACN+2H"	  : lambda mass:  mass/2 - 42.033823,
		"M+3ACN+2H"	  : lambda mass:  mass/2 - 62.547097,
		"M+H"			: lambda mass:  mass - 1.007276   ,
		"M+NH4"		  : lambda mass:  mass - 18.033823  ,
		"M+Na"		   : lambda mass:  mass - 22.989218  ,
		"M+CH3OH+H"	  : lambda mass:  mass - 33.033489  ,
		"M+K"			: lambda mass:  mass - 38.963158  ,
		"M+ACN+H"		: lambda mass:  mass - 42.033823  ,
		"M+IsoProp+H"	: lambda mass:  mass - 61.06534   ,
		"M+ACN+Na"	   : lambda mass:  mass - 64.015765  ,
		"M+2K+H"		 : lambda mass:  mass - 76.919040  ,
		"M+DMSO+H"	   : lambda mass:  mass - 79.02122   ,
		"M+2ACN+H"	   : lambda mass:  mass - 83.060370  ,
		"M+2Na-H"		: lambda mass:  mass - 44.971160  ,
		"M+IsoProp+Na+H" : lambda mass:  mass - 84.05511   ,
		"2M+H"		   : lambda mass:  2*mass - 1.007276 ,
		"2M+NH4"		 : lambda mass:  2*mass - 18.033823,
		"2M+Na"		  : lambda mass:  2*mass - 22.989218,
		"2M+3H2O+2H"	 : lambda mass:  2*mass - 28.02312 ,
		"2M+K"		   : lambda mass:  2*mass - 38.963158,
		"2M+ACN+H"	   : lambda mass:  2*mass - 42.033823,
		"2M+ACN+Na"	  : lambda mass:  2*mass - 64.015765,
		"M+Li"		   : lambda mass:  mass - 6.9400376247955,
		"M+H-H2O"		: lambda mass:  mass - 1.007276 + 18.010565 # Mass - H + H20
	}
	
	negative_ion_mass_to_mass = {
	"M-"			: lambda mass:  mass - Utilities.electron_iso_mass,
	"M-3H"		  : lambda mass:  mass/3 + 1.007276  ,
	"M-2H"		  : lambda mass:  mass/2 + 1.007276  ,
	"M-H20-H"	   : lambda mass:  mass + 19.01839	,
	"M-H"		   : lambda mass:  mass + 1.007276	,
	"M+Na-2H"	   : lambda mass:  mass - 20.974666   ,
	"M+Cl"		  : lambda mass:  mass - 34.969402   ,
	"M+K-2H"		: lambda mass:  mass - 36.948606   ,
	"M+FA-H"		: lambda mass:  mass - 44.998201   ,
	"M+Hac-H"	   : lambda mass:  mass - 59.013851   ,
	"M+Br"		  : lambda mass:  mass - 78.918885   ,
	"M+TFA-H"	   : lambda mass:  mass - 112.985586  ,
	"2M-H"		  : lambda mass:  2*mass + 1.007276  ,
	"2M+FA-H"	   : lambda mass:  2*mass - 44.998201 ,
	"2M+Hac-H"	  : lambda mass:  2*mass - 59.013851 ,
	"3M-H"		  : lambda mass:  3*mass + 1.007276  ,
	"M+HCOO"		: lambda mass:  mass - 44.998201  ,
	"M+CH3COO"	  : lambda mass:  mass - 59.0133}

	@staticmethod
	def neutral_mass_to_ion_mass(neutral_mass, adduct_type):
		"""[summary]

		Args:
			neutral_mass ([type]): [description]
			adduct_type ([type]): [description]

		Returns:
			[type]: [description]
		"""
		ion_name = adduct_type.split(']')[0][1:]
		ion_name_ext = adduct_type.split(']')[1]
		ion_mass = -1
		if ion_name_ext == '+' or adduct_type == '[M+]':
			if ion_name in Utilities.mass_to_positive_ion_mass:
				ion_mass =  Utilities.mass_to_positive_ion_mass[ion_name](neutral_mass)
		elif ion_name_ext == '-' or adduct_type == '[M-]':
			if ion_name in Utilities.mass_to_negative_ion_mass:
				ion_mass =  Utilities.mass_to_negative_ion_mass[ion_name](neutral_mass)
		else:
			print('Can not compute ion mass for adduct_type', adduct_type)
		#print(f"{neutral_mass} {adduct_type} {ion_mass}")
		return ion_mass
		
	@staticmethod
	def ion_mass_to_neutral_mass(ion_mass, adduct_type):
		"""[summary]

		Args:
			neutral_mass ([type]): [description]
			adduct_type ([type]): [description]

		Returns:
			[type]: [description]
		"""
		ion_name = adduct_type.split(']')[0][1:]
		ion_name_ext = adduct_type.split(']')[1]

		neutral_mass = -1
		if ion_name_ext == '+' or adduct_type == '[M+]':
			if ion_name in Utilities.positive_ion_mass_to_mass:
				neutral_mass =  Utilities.positive_ion_mass_to_mass[ion_name](ion_mass)
		elif ion_name_ext ==  '-'  or adduct_type == '[M-]':
			if ion_name in  Utilities.negative_ion_mass_to_mass: 
				neutral_mass =  Utilities.negative_ion_mass_to_mass[ion_name](ion_mass)
		else:
			print('Can not compute ion mass for adduct_type', adduct_type)
		#print(f"{neutral_mass} {adduct_type} {ion_mass}")
		return neutral_mass
	
	@staticmethod
	def escape_single_quote(json_str: str):
		return json_str.replace("'", "''")

	'''
	Method Create a Rdkit mol object by given Smiles or Inchi
	'''

	@staticmethod
	def create_mol_by_chemid(smiles_or_inchi: str, suppress_error = False):
		"""[summary]

		Args:
			chem_id (str): [description]

		Returns:
			[type]: [description]
		"""
		# get inchi and inchi keys
		mol = None
		if smiles_or_inchi.startswith("InChI"):
			# if we have a inchi or standard inchi
			mol = Chem.inchi.MolFromInchi(smiles_or_inchi)
		else:
			# if we have a smiles
			mol = Chem.MolFromSmiles(smiles_or_inchi)

		if mol is None and not suppress_error:
			raise ValueError('Can not create rdkit mol', smiles_or_inchi)
		return mol

	@staticmethod
	def get_inchikey_by_chemid(smiles_or_inchi: str):
		mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
		return Chem.inchi.MolToInchiKey(mol)

	@staticmethod
	def get_mol_mw_by_chemid(smiles_or_inchi: str):
		mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
		return Descriptors.ExactMolWt(mol)

	@staticmethod
	def get_formula_by_chemid(smiles_or_inchi: str):
		mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
		return Chem.rdMolDescriptors.CalcMolFormula(mol)

	@staticmethod
	def get_mass_tol_ppm(mass, ppm_tol=10):
		"""[summary]

		Args:
			mass ([type]): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.

		Returns:
			[type]: [description]
		"""
		#print(abs_tol, mass * Utilities.ppm * ppm_tol)
		allowed_tol = mass * Utilities.ppm * ppm_tol
		#print(allowed_tol)
		return allowed_tol
	
	@staticmethod
	def get_mass_tol(mass, abs_tol=0.01, ppm_tol=10):
		"""[summary]

		Args:
			mass ([type]): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.

		Returns:
			[type]: [description]
		"""
		#print(abs_tol, mass * Utilities.ppm * ppm_tol)
		allowed_tol = max(abs_tol, mass * Utilities.ppm * ppm_tol)
		#print(allowed_tol)
		return allowed_tol

	@staticmethod
	def get_min_mass_tol(mass, abs_tol=0.01, ppm_tol=10):
		"""get mass tol with whatever is smaller

		Args:
			mass ([type]): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.

		Returns:
			[type]: [description]
		"""
		allowed_tol = min(abs_tol, mass * Utilities.ppm * ppm_tol)
		return allowed_tol

	@staticmethod
	def get_mol_mass_tol(smiles_or_inchi: str, abs_tol=0.01, ppm_tol=10) -> Tuple[float, float]:
		"""[summary]

		Args:
			smiles_or_inchi (str): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.

		Returns:
			Tuple[float,float]: molecule mass, mass tolerance
		"""
		mass = Utilities.get_mol_mw_by_chemid(smiles_or_inchi)
		allowed_tol = Utilities.get_mass_tol(mass, abs_tol, ppm_tol)
		return mass, allowed_tol

	@staticmethod
	def get_mol_mass_tol_min(smiles_or_inchi: str, abs_tol=0.01, ppm_tol=10) -> Tuple[float, float]:
		"""[summary]

		Args:
			smiles_or_inchi (str): [description]
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.

		Returns:
			Tuple[float,float]: molecule mass, mass tolerance
		"""
		mass = Utilities.get_mol_mw_by_chemid(smiles_or_inchi)
		allowed_tol = Utilities.get_min_mass_tol(mass, abs_tol, ppm_tol)
		return mass, allowed_tol

	@staticmethod
	def has_formal_charge(mol) -> bool:
		"""[check each atom has formal charge, in this case zwitterion will get true]

		Args:
			mol ([type]): [description]

		Returns:
			bool: [description]
		"""
		for atom in mol.GetAtoms():
			formal_charge = atom.GetFormalCharge()
			if formal_charge != 0:
				return True
		return False

	@staticmethod
	def has_formal_charge_chemid(smiles_or_inchi: str) -> bool:
		"""[check each atom has formal charge, in this case zwitterion will get true]

		Args:
			mol ([type]): [description]

		Returns:
		"""
		mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
		return Utilities.has_formal_charge(mol)

	@staticmethod
	def create_flatten_mol_by_chemid(smiles_or_inchi: str) -> Tuple[str, str]:

		normalized_mol = Utilities.create_mol_by_chemid(smiles_or_inchi) 
		RemoveStereochemistry(normalized_mol)
		return normalized_mol
	
	@staticmethod
	def get_flatten_smiles_or_inchi(smiles_or_inchi: str) -> Tuple[str, str]:
		""" return smiles and inchikey after removing stereochemistry

		Args:
			smiles_or_inchi (str): [description]

		Returns:
			List[str, str, str]: normalized_smiles,normalized_inchikey
		"""
		normalized_mol = Utilities.create_flatten_mol_by_chemid(smiles_or_inchi)
		if normalized_mol is None:
			return None, None
		
		normalized_inchikey = Chem.inchi.MolToInchiKey(normalized_mol)
		normalized_smiles = Chem.MolToSmiles(normalized_mol)
		return normalized_smiles, normalized_inchikey

	
	@staticmethod
	def read_training_set_mol_list(file_path):
		"""[summary]

		Args:
			file_path ([type]): [description]

		Returns:
			[type]: [description]
		"""
		mol_list = []
		with open(file_path) as file_in:
			for idx,row in enumerate(file_in):
				if idx == 0:
					continue
				items = row.split()
				mol_list.append(items)

		return mol_list

	@staticmethod
	def filter_chem_input_by_tanimoto(target: str, candidate_list: List[str],threshold = 0.9) -> List[str]:
		"""Method to filter smiles by tanimoto score

		Args:
			target (str): target smiles
			candidate_list (List[str]): list of candidate smiles
			threshold (float, optional): threshold for tanimoto cutoff. Defaults to 0.9.

		Returns:
			List[str]: Return list of smiles
		"""

		target_mol = Utilities.create_mol_by_chemid(target)
		target_fp = AllChem.GetMorganFingerprintAsBitVect(target_mol, 3, nBits=1024)
		selected_candidates = [] 

		for candidate in candidate_list:
			candidate_mol = Utilities.create_mol_by_chemid(candidate)
			candidate_fp = AllChem.GetMorganFingerprintAsBitVect(candidate_mol, 3, nBits=1024)
			tanimoto_score = FingerprintSimilarity(target_fp, candidate_fp)
			if tanimoto_score > threshold:
				selected_candidates.append(candidate)

		return selected_candidates
	
	@staticmethod
	def get_tanimoto_score(chemid_one, chemid_two) -> float:
		first_mol = Utilities.create_mol_by_chemid(chemid_one)
		first_fp = AllChem.GetMorganFingerprintAsBitVect(first_mol, 3, nBits=1024)

		second_mol = Utilities.create_mol_by_chemid(chemid_two)
		second_fp = AllChem.GetMorganFingerprintAsBitVect(second_mol, 3, nBits=1024)

		return FingerprintSimilarity(first_fp, second_fp)

	@staticmethod
	def get_rdkit_canonical_smiles(smiles_or_inchi:str):
		mol = Utilities.create_mol_by_chemid(smiles_or_inchi)
		return Chem.MolToSmiles(mol)

	@staticmethod
	def get_nl_formula(parent_ion_formula:str, peak_formula:str) -> str:
		regex = r'([A-Z][a-z]?)([0-9]*)'
		parent_ion_pairs = findall(regex, parent_ion_formula)
		peak_ion_pairs = findall(regex, peak_formula)
	
		parent_element_dict = {}
		peak_element_dict = {}
		for element_dict,element_pairs in [(parent_element_dict, parent_ion_pairs),(peak_element_dict,peak_ion_pairs)]:
			for element, count_str in element_pairs:
				element_count = int(count_str)  if len(count_str) > 0 else 1
				element_dict[element] = element_count

		nl_str = ''
		for element in parent_element_dict:
			if element in peak_element_dict:
				remaining_count = parent_element_dict[element] - peak_element_dict[element]
				if remaining_count == 0:
					continue
				nl_str += element
				if remaining_count > 1:
					nl_str +=  str(parent_element_dict[element] - peak_element_dict[element])
			else:
				nl_str += element
				if parent_element_dict[element] > 1:
					nl_str += str(parent_element_dict[element])
		return nl_str

	@staticmethod
	def get_nl_formula_by_chemid(parent_ion_smiles_or_inchi:str, peak_smiles_or_inchi:str) -> str:
		parent_ion_formula = Utilities.get_formula_by_chemid(parent_ion_smiles_or_inchi)
		peak_formula = Utilities.get_formula_by_chemid(peak_smiles_or_inchi)
		return Utilities.get_nl_formula(parent_ion_formula, peak_formula)
	
	@staticmethod
	def get_exact_mass_from_formula(formula:str):
		parts = findall("[A-Z][a-z]?|[0-9]+", formula)
		mass = 0.0
		for index in range(len(parts)):
			if parts[index].isnumeric():
				continue
			
			try:
				atom = Chem.Atom(parts[index])
			except AttributeError:
				raise AttributeError(f"Unknown atom {parts[index]}")
			else:
				multiplier = int(parts[index + 1]) if len(parts) > index + 1 and parts[index + 1].isnumeric() else 1
				mass += atom.GetMass() * multiplier

		return mass
	
	@staticmethod
	def spectrum_list_to_df():
		pass