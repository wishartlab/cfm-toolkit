import json
import os.path




from cfmtoolkit.standardization import Standardization


# use with Resolver as resolver to use cache
class BaseResolver(object):
	def __init__(self, cache_file_path: str, pcp_query_type: str, nih_query_type: str):
		self._cached = {}
		self._cache_file_path = cache_file_path
		self._init_cache()
		# pcp query type indciated input query type, inchi , inchikey and etc
		self._pcp_query_type = pcp_query_type
		# nih_query_type indicate queried type, inchi means we want to get inchi
		self._nih_query_type = nih_query_type

	def __enter__(self):
		return self

	def __exit__(self, exception_type, exception_value, traceback):
		# save cache on exit
		self._save_cache()

	def _init_cache(self):
		if os.path.isfile(self._cache_file_path):
			with open(self._cache_file_path) as infile:
				self._cached = json.load(infile)

	def _save_cache(self):
		with open(self._cache_file_path, "w+") as outfile:
			json.dump(self._cached, outfile, indent=2)

	def get_inchi(self, query: str) -> str:
		# return cached
		if query in self._cached and \
				self._cached[query] is not None:
			return self._cached[query]

		inchi = self._query_pubchem(query, self._pcp_query_type)
		if inchi != Standardization.NA:
			self._cached[query] = inchi
			return inchi

		inchi = self._query_nih(query, self._nih_query_type)
		self._cached[query] = inchi
		return inchi


	def get_iupac_name(self, query: str) -> str:
		# return cached
		if query in self._cached and \
				self._cached[query] is not None:
			return self._cached[query]

		iupac_name = self._query_pubchem(query, self._pcp_query_type)
		if iupac_name != Standardization.NA:
			self._cached[query] = iupac_name
			return iupac_name

		iupac_name = self._query_nih(query, self._nih_query_type)
		self._cached[query] = iupac_name
		return iupac_name

	@classmethod
	def _query_pubchem(cls, query: str, query_type: str) -> str:
		# print(query, query_type)
		import pubchempy as pcp
		compounds = pcp.get_compounds(query, query_type)
		# print(compounds)
		if len(compounds) == 0:
			return Standardization.NA
		else:
			# We all going to trust pubchem for this
			compound = compounds[0]
			return compound.inchi

	@classmethod
	def _query_nih(cls, query: str, query_type: str) -> str:
		# print(query, query_type)
		import cirpy
		inchi = cirpy.resolve(query, query_type)
		inchi = Standardization.NA if inchi is None else inchi
		return inchi

class NameResolver(BaseResolver):
	def __init__(self, cache_file_path="./name_cache.json"):
		super(NameResolver, self).__init__(cache_file_path, 'name', 'stdinchi')

	def get_inchi_by_name(self, name: str) -> str:
		return self.get_inchi(name)


class SmilesResolver(BaseResolver):
	def __init__(self, cache_file_path="./smiles_cache.json"):
		super(SmilesResolver, self).__init__(cache_file_path, 'smiles', 'stdinchi')

	def get_inchi_by_smiles(self, smiles: str) -> str:
		return self.get_inchi(smiles)


class InchikeyResolver(BaseResolver):
	def __init__(self, cache_file_path="./inchikey_cache.json"):
		super(InchikeyResolver, self).__init__(cache_file_path, 'inchikey', 'stdinchi')

	def get_inchi_by_inchikey(self, inchikey: str) -> str:
		return self.get_inchi(inchikey)
