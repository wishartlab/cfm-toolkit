from typing import List, Optional

import jsonpickle

from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from cfmtoolkit.io import CFM
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities
from .database_utilities import DatabaseUtilities
from .pg_db_base import PgDbBase
from os import walk
from os.path import  join
from tqdm import tqdm
from rdkit import Chem

class CfmPgDB(PgDbBase):
	
	def create_tables(self, verbose=False):
		query = """
			CREATE TABLE IF NOT EXISTS spectrum (
				id SERIAL PRIMARY KEY,
				inchikey VARCHAR (27) NOT NULL,
				smiles_or_inchi text NOT NULL,
				formula text NOT NULL,
				compound_name text,
				compound_data_source VARCHAR(20),
				compound_data_source_id VARCHAR(20),
				compound_exact_mass double precision NOT NULL,
				chromatography_type VARCHAR (3),
				adduct_type VARCHAR (30),
				analyzer_type VARCHAR (30),
				ionization_type VARCHAR (5),
				charge_type VARCHAR (1),
				ms_level VARCHAR (3),
				collision_energy_prefix VARCHAR(5),
				collision_energy_unit VARCHAR(5),
				collision_method VARCHAR(5),
				collision_gas_type VARCHAR(5),
				collision_energy float,
				mz_decimal_place smallint,
				precursor_mz double precision,
				data_source VARCHAR(20),
				data_source_id VARCHAR(20),
				compound_sample_frequency integer,
				peak_data JSONB
			);
			CREATE INDEX ON spectrum (inchikey);
			CREATE INDEX ON spectrum (adduct_type);
			CREATE INDEX ON spectrum (compound_exact_mass);
			CREATE INDEX ON spectrum (precursor_mz);
			"""
		if verbose:
			print(query)
		self.execute_query(query, commit=True, fetch=False)

	def _get_add_spectrum_query(self, spectrum_list: List[Spectrum]):
		# print(molecule_id)
		insert_payloads = []
		for spectrum in spectrum_list:
			escaped_peak_json_str = DatabaseUtilities.escape_single_quote(
				jsonpickle.encode(spectrum.get_peaks(), unpicklable=False))

			escaped_smiles_or_inchi = DatabaseUtilities.escape_single_quote(spectrum.smiles_or_inchi)
			escaped_compound_name = DatabaseUtilities.escape_single_quote(spectrum.compound_name)
			# TODO FIX THIS, THIS is a hack
			data_source_id = spectrum.data_source_id if len(spectrum.data_source_id) <= 20 else Standardization.NA
			template = "(" + ','.join(["'{}'" for _ in range(24)]) + ")"
			insert_payloads.append(template.format(
				spectrum.inchikey,
				escaped_smiles_or_inchi,
				spectrum.formula,
				escaped_compound_name,
				spectrum.compound_data_source,
				spectrum.compound_data_source_id,
				spectrum.compound_exact_mass,
				spectrum.chromatography_type,
				spectrum.adduct_type,
				spectrum.analyzer_type,
				spectrum.ionization_type,
				spectrum.charge_type,
				spectrum.ms_level,
				spectrum.collision_energy_prefix,
				spectrum.collision_energy_unit,
				spectrum.collision_method,
				spectrum.collision_gas_type,
				spectrum.collision_energy,
				spectrum.mz_decimal_place,
				spectrum.precursor_mz,
				spectrum.data_source,
				data_source_id,
				spectrum.compound_sample_frequency,
				escaped_peak_json_str
			))

		# print(insert_payloads)
		query = """INSERT INTO spectrum(
				inchikey,
				smiles_or_inchi,
				formula,
				compound_name,
				compound_data_source,
				compound_data_source_id,
				compound_exact_mass,
				chromatography_type,
				adduct_type,
				analyzer_type ,
				ionization_type,
				charge_type,
				ms_level,
				collision_energy_prefix,
				collision_energy_unit,
				collision_method,
				collision_gas_type,
				collision_energy,
				mz_decimal_place,
				precursor_mz,
				data_source,
				data_source_id,
				compound_sample_frequency,
				peak_data
				) VALUES {} returning id;""".format(
			','.join(insert_payloads))
		return query

	def add_cfm_spectra(self, cfm_spectra_list: List[CfmSpectra], verbose=False):
		spectrum_list = []
		for cfm_spectra in cfm_spectra_list:
			spectrum_list += cfm_spectra.get_spectra_as_list()
		self.add_spectrum(spectrum_list, verbose)

	def add_spectrum(self, spectrum_list: List[Spectrum], verbose=False):
		query = self._get_add_spectrum_query(spectrum_list)
		if verbose:
			print(query)
		self.execute_query(query, commit=True)

	'''get spectra from database with the give mw'''

	def get_spectra_by_exact_mass(self, mw, abs_tol=0.01, ppm_tol=10, adduct_type=None,
								  data_source=None,
								  compound_data_source=None,
								  collision_energy=None,
								  collision_method=None,
								  csf_lower=None,
								  csf_upper=None,
								  verbose=False) -> List[Spectrum]:
		return self._get_spectra_by_mass(mw, None, abs_tol, ppm_tol, adduct_type, data_source, compound_data_source,
										 collision_energy, collision_method, csf_lower, csf_upper, verbose)

	def get_spectra_by_precursor_mz(self, precursor_mz, abs_tol=0.01, ppm_tol=10, adduct_type=None,
									data_source=None,
									compound_data_source=None,
									collision_energy=None,
									collision_method=None,
									csf_lower=None,
									csf_upper=None,
									verbose=False) -> List[Spectrum]:
		return self._get_spectra_by_mass(None, precursor_mz, abs_tol, ppm_tol, adduct_type, data_source,
										 compound_data_source, collision_energy, collision_method, csf_lower, csf_upper,
										 verbose)

	def _get_spectra_query(self, adduct_type=None,
						   data_source=None,
						   compound_data_source=None,
						   collision_energy=None,
						   collision_method=None,
						   csf_lower=None,
						   csf_upper=None):
		query = ""
		if adduct_type is not None:
			query += " AND s.adduct_type = '{}'".format(adduct_type)

		if data_source is not None:
			query += " AND s.data_source = '{}'".format(data_source)

		if compound_data_source is not None:
			query += " AND s.compound_data_source = '{}'".format(compound_data_source)

		if collision_energy is not None:
			query += " AND s.collision_energy = '{}'".format(collision_energy)

		if collision_method is not None:
			query += " AND s.collision_method = '{}'".format(collision_method)

		if csf_lower is not None:
			query += " AND s.compound_sample_frequency >= {}".format(csf_lower)

		if csf_upper is not None:
			query += " AND s.compound_sample_frequency <= {}".format(csf_upper)

		return query

	def _construct_spectra_from_results(self, results):
		spectrum_list = []
		for item in results:
			spectrum = Spectrum()
			spectrum.inchikey = item[1]
			spectrum.smiles_or_inchi = item[2]
			spectrum.formula = item[3]
			spectrum.compound_name = item[4]
			spectrum.compound_data_source = item[5]
			spectrum.compound_data_source_id = item[6]
			spectrum.compound_exact_mass = float(item[7])
			spectrum.chromatography_type = item[8]
			spectrum.adduct_type = item[9]
			spectrum.analyzer_type = item[10]
			spectrum.ionization_type = item[11]
			spectrum.charge_type = item[12]
			spectrum.ms_level = item[13]
			spectrum.collision_energy_prefix = item[14]
			spectrum.collision_energy_unit = item[15]
			spectrum.collision_method = item[16]
			spectrum.collision_gas_type = item[17]
			spectrum.collision_energy = float(item[18])
			spectrum.mz_decimal_place = int(item[19])
			spectrum.precursor_mz = float(item[20])
			spectrum.data_source = item[21]
			spectrum.data_source_id = item[22]
			spectrum.compound_sample_frequency = int(item[23])
			spectrum.reconstruct_peaks(item[24])
			spectrum_list.append(spectrum)
		return spectrum_list

	def _get_spectra_by_mass(self, compound_exact_mass=None, precursor_mz=None, abs_tol=0.01, ppm_tol=10,
							 adduct_type=None,
							 data_source=None,
							 compound_data_source=None,
							 collision_energy=None,
							 collision_method=None,
							 csf_lower=None,
							 csf_upper=None,
							 verbose=False) -> List[Spectrum]:

		if compound_exact_mass is not None:
			allowed_tol = Utilities.get_mass_tol(
				compound_exact_mass, abs_tol, ppm_tol)
			if verbose:
				print("allowed_tol: {} Da".format(allowed_tol))
			query = """Select * from spectrum s where s.compound_exact_mass > {} and s.compound_exact_mass < {}""".format(
				compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol)

		elif precursor_mz is not None:
			allowed_tol = Utilities.get_mass_tol(
				precursor_mz, abs_tol, ppm_tol)
			if verbose:
				print("allowed_tol: {} Da".format(allowed_tol))
			query = """Select * from spectrum s where s.precursor_mz > {} and s.precursor_mz < {}""".format(
				precursor_mz - allowed_tol, precursor_mz + allowed_tol)
		else:
			print(
				"Error, one of compound_exact_mass, precursor_mz should be not a None value")

		query += self._get_spectra_query(adduct_type,
										 data_source,
										 compound_data_source,
										 collision_energy,
										 collision_method,
										 csf_lower,
										 csf_upper)
		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		# print(spectra)
		return self._construct_spectra_from_results(results)


	def get_inchikey_count_by_mass(self, compound_exact_mass:float, data_source:str, compound_data_source:str,
		abs_tol=0.01, ppm_tol=10,verbose=False) -> int:

		allowed_tol = Utilities.get_mass_tol(
				compound_exact_mass, abs_tol, ppm_tol)
		query = """Select COUNT(*) from (SELECT DISTINCT inchikey From spectrum s where s.compound_exact_mass > {} and s.compound_exact_mass < {} and s.data_source = '{} and s.compound_data_source = {}') as A""".format(
				compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol, data_source, compound_data_source)

		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		# print(spectra)
		return results

	def _get_cfm_spectra_by_mass(self, compound_exact_mass=None, precursor_mz=None, abs_tol=0.01, ppm_tol=10,
								 adduct_type=None,
								 data_source=None,
								 compound_data_source=None,
								 collision_energy=None,
								 collision_method=None,
								 csf_lower=None,
								 csf_upper=None,
								 verbose=False) -> List[CfmSpectra]:
		"""[summary]
			Selection range is <abs_tol> Da or <ppm_tol> PPM whichever is smaller
		Args:
			compound_exact_mass ([type], optional): [description]. Defaults to None.
			precursor_mz ([type], optional): [description]. Defaults to None.
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			compound_data_source ([type], optional): [description]. Defaults to None.
			collision_energy ([type], optional): [description]. Defaults to None.
			collision_method ([type], optional): [description]. Defaults to None.
			csf_lower ([type], optional): [description]. Defaults to None.
			csf_upper ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Raises:
			ValueError: [description]

		Returns:
			List[CfmSpectra]: [description]
		"""

		spectrum_list = self._get_spectra_by_mass(
			compound_exact_mass, precursor_mz, abs_tol, ppm_tol, adduct_type, data_source, compound_data_source,
			collision_energy, collision_method, csf_lower, csf_upper, verbose)
		cfm_spectrum_dict = {}

		ev_to_keys = {10: 'low', 20: 'med', 40: 'high'}

		for spectrum in spectrum_list:
			inchikey = spectrum.inchikey
			if inchikey not in cfm_spectrum_dict:
				cfm_spectrum_dict[inchikey] = CfmSpectra()
				cfm_spectrum_dict[inchikey].fill_meta(spectrum)
			
			if spectrum.collision_energy not in ev_to_keys:
				raise ValueError('collision_energy does not match',inchikey)
			cfm_spectrum_dict[inchikey].add_spectrum(
				ev_to_keys[spectrum.collision_energy], spectrum)

		return list(cfm_spectrum_dict.values())

	def get_cfm_spectra_by_exact_mass(self, compound_exact_mass, abs_tol=0.01, ppm_tol=10, adduct_type=None,
									  data_source=None,
									  compound_data_source=None,
									  collision_method=None,
									  csf_lower=None,
									  csf_upper=None,
									  verbose=False) -> List[CfmSpectra]:
		"""get cfm spectra from database with the give compound_exact_mass mw
			Selection range is 0.01 Da or 10 PPM whichever is smaller

		Args:
			compound_exact_mass ([type], optional): [description].
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			compound_data_source ([type], optional): [description]. Defaults to None.
			collision_method ([type], optional): [description]. Defaults to None.
			csf_lower ([type], optional): [description]. Defaults to None.
			csf_upper ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""
		return self._get_cfm_spectra_by_mass(compound_exact_mass, None, abs_tol, ppm_tol, adduct_type, data_source,
											 compound_data_source, None, collision_method, csf_lower, csf_upper,
											 verbose)

	def get_cfm_spectra_by_precursor_mz(self, precursor_mz, abs_tol=0.01, ppm_tol=10, adduct_type=None,
										data_source=None,
										compound_data_source=None,
										collision_method=None,
										csf_lower=None,
										csf_upper=None,
										verbose=False) -> List[CfmSpectra]:
		"""get cfm spectra from database with the give compound_exact_mass mw
			Selection range is 0.01 Da or 10 PPM whichever is smaller

		Args:
			compound_exact_mass ([type], optional): [description].
			abs_tol (float, optional): [description]. Defaults to 0.01.
			ppm_tol (int, optional): [description]. Defaults to 10.
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			compound_data_source ([type], optional): [description]. Defaults to None.
			collision_energy ([type], optional): [description]. Defaults to None.
			collision_method ([type], optional): [description]. Defaults to None.
			csf_lower ([type], optional): [description]. Defaults to None.
			csf_upper ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""
		return self._get_cfm_spectra_by_mass(None, precursor_mz, abs_tol, ppm_tol, adduct_type, data_source,
											 compound_data_source, None, collision_method, csf_lower, csf_upper,
											 verbose)

	'''get cfm spectra from database with the give mw'''

	def get_spectrum_count(self, verbose=False) -> int:
		query = """Select count(*) from spectrum s"""
		if verbose:
			print(query)
		count = self.execute_query(query, commit=True)
		# print(count[0][0])
		return int(count[0][0])

	def get_spectra_by_inchikeys(self, inchikeys: List[str],
								 adduct_type=None,
								 data_source=None,
								 compound_data_source=None,
								 collision_energy=None,
								 collision_method=None,
								 verbose=False) -> List[Spectrum]:

		inchikeys_query = ','.join(["'{}'".format(i) for i in inchikeys])
		# for inchikey in  inchikeys:

		query = """Select * from spectrum s where s.inchikey in ({})""".format(
			inchikeys_query)

		query += self._get_spectra_query(adduct_type,
										 data_source,
										 compound_data_source,
										 collision_energy,
										 collision_method,
										 None,
										 None)
		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)
		# print(spectra)
		return self._construct_spectra_from_results(results)

	def _assemble_cfm_spectra(self, spectrum_list, ev_to_keys = None, check_complete = False):
		cfm_spectrum_dict = {}

		if ev_to_keys is None:
			ev_to_keys = {10.0: 'low', 20.0: 'med', 40.0: 'high'}

		for spectrum in spectrum_list:
			inchikey = spectrum.inchikey
			if inchikey not in cfm_spectrum_dict:
				cfm_spectrum_dict[inchikey] = CfmSpectra()
				cfm_spectrum_dict[inchikey].fill_meta(spectrum)
			
			ev_float = float(spectrum.collision_energy)
			if ev_float in ev_to_keys:
				cfm_spectrum_dict[inchikey].add_spectrum(
					ev_to_keys[spectrum.collision_energy], spectrum)
		
		if check_complete:
			cfm_spectra_list = [cfm_spectrum_dict[cfm_spectra] for cfm_spectra in cfm_spectrum_dict if cfm_spectrum_dict[cfm_spectra].is_complete()]
		else:
			cfm_spectra_list = list(cfm_spectrum_dict.values())
		return cfm_spectra_list
		
	def get_cfm_spectra_by_inchikeys(self, inchikeys: List[str],
									 adduct_type,
									 data_source=None,
									 compound_data_source=None,
									 check_complete=False,
									 verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""

		spectrum_list = self.get_spectra_by_inchikeys(
			inchikeys,
			adduct_type,
			data_source,
			compound_data_source,
			collision_energy=None,
			collision_method=None,
			verbose=verbose)

		return self._assemble_cfm_spectra(spectrum_list, check_complete)


	def get_spectra_by_formulas(self, formulas: List[str],
								 adduct_type=None,
								 data_source=None,
								 compound_data_source = None,
								 collision_energy=None,
								 collision_method=None,
								 verbose=False) -> List[Spectrum]:

		formulas_query = ','.join(["'{}'".format(i) for i in formulas])
		# for inchikey in  inchikeys:

		query = """Select * from spectrum s where s.formula in ({})""".format(
			formulas_query)

		query += self._get_spectra_query(adduct_type,
										 data_source,
										 compound_data_source,
										 collision_energy,
										 collision_method,
										 None,
										 None)
		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)
		# print(spectra)
		return self._construct_spectra_from_results(results)

	def get_cfm_spectra_by_formulas(self, formulas: List[str],
									 adduct_type,
									 data_source=None,
									 compound_data_source = None,
									 verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""

		spectrum_list = self.get_spectra_by_formulas(
			formulas,
			adduct_type,
			data_source,
			compound_data_source,
			collision_energy=None,
			collision_method=None,
			verbose=verbose)

		return self._assemble_cfm_spectra(spectrum_list)


	def get_all_unique_inchikeys(self, adduct_type:Optional[str]=None,
							data_source=None,
							compound_data_source = None,
							analyzer_type = None,
							chunk_size = 1,
							verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""
		query = """select distinct inchikey from spectrum s"""

		if adduct_type is not None or data_source is not None \
			or compound_data_source is not None or analyzer_type is not None:
			query += " WHERE"
		need_add_and = False

		if adduct_type is not None:
			query += """  s.adduct_type = '{}'""".format(adduct_type)
			need_add_and = True

		if analyzer_type is not None:
			if need_add_and:
				query += " AND"
			query += """  s.analyzer_type = '{}'""".format(analyzer_type)
			need_add_and = True

		if data_source is not None:
			if need_add_and:
				query += " AND"
			query += """  s.data_source = '{}'""".format(data_source)
			need_add_and = True

		if compound_data_source is not None:
			if need_add_and:
				query += " AND"
			query += """  s.compound_data_source = '{}'""".format(compound_data_source)
			need_add_and = True

		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)

		inchikeys = [ item[0] for item in results ] 
		if chunk_size > 1:
			inchikeys = [inchikeys[i:i + chunk_size] for i in range(0, len(inchikeys), chunk_size)]
		
		inchikeys = [i for i in inchikeys if i != Standardization.NA]
		return inchikeys

	def get_all_unique_smiles_or_inchi(self, adduct_type:Optional[str]=None,
							data_source=None,
							compound_data_source = None,
							analyzer_type = None,
							chunk_size = 1,
							check_rdkit_canonical_smiles = True,
							verbose=False) -> List[CfmSpectra]:
		"""_summary_

		Args:
			adduct_type (Optional[str], optional): _description_. Defaults to None.
			data_source (_type_, optional): _description_. Defaults to None.
			compound_data_source (_type_, optional): _description_. Defaults to None.
			analyzer_type (_type_, optional): _description_. Defaults to None.
			chunk_size (int, optional): _description_. Defaults to 1.
			check_rdkit_canonical_smiles (bool, optional): _description_. Defaults to True.
			check_inchikey (bool, optional): _description_. Defaults to False.
			verbose (bool, optional): _description_. Defaults to False.

		Returns:
			List[CfmSpectra]: _description_
		"""		
		query = """select distinct smiles_or_inchi from spectrum s"""

		if adduct_type is not None or data_source is not None \
			or compound_data_source is not None or analyzer_type is not None:
			query += " WHERE"
		need_add_and = False

		if adduct_type is not None:
			query += """  s.adduct_type = '{}'""".format(adduct_type)
			need_add_and = True

		if analyzer_type is not None:
			if need_add_and:
				query += " AND"
			query += """  s.analyzer_type = '{}'""".format(analyzer_type)
			need_add_and = True

		if data_source is not None:
			if need_add_and:
				query += " AND"
			query += """  s.data_source = '{}'""".format(data_source)
			need_add_and = True

		if compound_data_source is not None:
			if need_add_and:
				query += " AND"
			query += """  s.compound_data_source = '{}'""".format(compound_data_source)
			need_add_and = True

		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)

		smiles_or_inchi_l = [ item[0] for item in results]
		if chunk_size > 1:
			smiles_or_inchi_l = [smiles_or_inchi_l[i:i + chunk_size] for i in range(0, len(smiles_or_inchi_l), chunk_size)]
		smiles_or_inchi_l = [i for i in smiles_or_inchi_l if i != Standardization.NA]

		#seen = set([])
		if check_rdkit_canonical_smiles:
			canonical_smiles = []
			for smiles_or_inchi in smiles_or_inchi_l:
				mol = Utilities.create_mol_by_chemid(smiles_or_inchi, True)
				if mol is None:
					continue
				else:
					canonical_smiles.append(Chem.MolToSmiles(mol, canonical = True))
			smiles_or_inchi_l = list(set(canonical_smiles))

		return smiles_or_inchi_l
	
	def add_cfm_spectra_in_dir(self, spectra_dir, meta_data, chunk_size=10000, legacy_mode=False, mol_list_file=None,
							   filename_pattern = None, check_charges=True, freq_dict = None, dry_run = False, 
							   pre_process_spectra = False, intensity_threshold=1.0, centroid=False, mz_distance=0.1):
		"""[summary]

		Args:
			spectra_dir ([type]): [description]
			meta_data ([type]): [description]
			chunk_size (int, optional): [description]. Defaults to 100.
			legacy_mode (bool, optional): [description]. Defaults to False.
			mol_list_file ([type], optional): [description]. Defaults to None.
			filename_pattern ([type], optional): [description]. Defaults to None.
			check_charges (bool, optional): [description]. Defaults to False.
			verbose (bool, optional): [description]. Defaults to False.
		"""

		#if verbose:
		#	print('dir:', spectra_dir, 'chunks size:', chunk_size, 'legacy mode:',legacy_mode)

		if legacy_mode:
			cfm_reader = CFM()
			cfm_reader.get_mols_from_file(mol_list_file, skip_first_row=False)

		cfm_spectra_list = []
		added_count = 0
		for path, _, files in tqdm(walk(spectra_dir)):
			for name in  tqdm(files, desc = path, leave= False):
				if filename_pattern is not None and filename_pattern not in name:
					continue
				if not name.endswith('log') and not name.endswith('txt'):
					continue
			
				spectra_file = join(path, name)
				#if verbose:
				#	print('File path:', spectra_file)
				try:
					if legacy_mode:
						current_cfm_spectra_list = [cfm_reader.read_cfmid2_esi_spectra(spectra_file, meta_data)]
					else:
						current_cfm_spectra_list = CFM.read_esi_spectra_list_with_meta(spectra_file, meta_data)
				except Exception as e:
					print('Can not read,', spectra_file)
					print('Exception,', e)
				else:
					# add frequence info
					# used by deepmet and darknps

					for cfm_spectra in current_cfm_spectra_list:
						if pre_process_spectra:
							cfm_spectra.remove_noise( intensity_threshold, centroid, mz_distance)
						if freq_dict is not None and cfm_spectra.inchikey in freq_dict:
							freq = freq_dict[cfm_spectra.inchikey]
							cfm_spectra.set_compound_sample_frequency(freq)
						elif freq_dict is not None:
							print('Error, cannot find frequency data for', cfm_spectra.inchikey)

						if not check_charges:
							cfm_spectra_list.append(cfm_spectra)
						else:
							has_charge = Utilities.has_formal_charge_chemid(cfm_spectra.smiles_or_inchi)
							if has_charge:
								print('Error,', spectra_file, cfm_spectra.smiles_or_inchi ,'has charge')
							else:
								cfm_spectra_list.append(cfm_spectra)
				for idx in range(0, len(cfm_spectra_list), chunk_size):
					if not dry_run:
						self.add_cfm_spectra(cfm_spectra_list[idx:idx + chunk_size])
				added_count += len(cfm_spectra_list)
				cfm_spectra_list = []

		return added_count

	def get_cfm_spectra_for_hcd(self, adduct_type,
									 verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""

		# for inchikey in  inchikeys:
		query = """Select * from spectrum s where s.collision_method = 'hcd' AND s.adduct_type = '{}' AND collision_energy in (35,45,65) ORDER BY s.inchikey""".format(adduct_type)
   
		#print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)
		print(len(self._construct_spectra_from_results(results)))
		return self._assemble_cfm_spectra(self._construct_spectra_from_results(results), ev_to_keys = {35: 'low', 45: 'med', 65: 'high'}, check_complete = True)