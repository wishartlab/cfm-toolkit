
import sys

class DatabaseUtilities:
	# Database functions
	@staticmethod
	def print_psycopg2_exception(err):
		# get details about the exception
		err_type, err_obj, traceback = sys.exc_info()

		# get the line number when exception occured
		line_num = traceback.tb_lineno

		# print the connect() error
		print("\npsycopg2 ERROR:", err, "on line number:", line_num)
		print("psycopg2 traceback:", traceback, "-- type:", err_type)

	@staticmethod
	def escape_single_quote(string: str):
		return string.replace("'", "''")