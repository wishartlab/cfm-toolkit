from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities
from .db_config import SqliteDBConfig
import sqlite3
from tqdm import tqdm
class DeepMetCfmSqliteDB():

	_adduct_type_table_name_map = { 
		Standardization.M_H_pos: 'M_H_pos',
		Standardization.M_H_neg: 'M_H_neg',
	}

	def __init__(self, db_config: str|SqliteDBConfig):
		self.conn = None
		if isinstance(db_config,str):
			self.db_file = db_config
		else:
			self.db_file = db_config.db_file

	def __enter__(self):
		self.connect()
		return self

	def __exit__(self, exception_type, exception_value, traceback):
		# save cache on exit
		self.disconnect()

	def connect(self):
		self.disconnect()
		# Try to connect
		# conn = None
		try:
			self.conn = sqlite3.connect(self.db_file, isolation_level=None, detect_types=sqlite3.PARSE_DECLTYPES)
		except:
			print(f"Unable to connect to the database: {self.db_file} ")
		else:
			# print("Database Connection Established")
			# use wal model
			self.conn.execute('pragma journal_mode=wal2')

	def disconnect(self):
		if self.conn is not None:
			self.conn.cursor().close()
			self.conn.close()
			self.conn = None

	def check_connection(self):
		if self.conn is None:
			raise ValueError('No Connection')

	def execute_query(self, query: str, commit: bool = False, fetch: bool = True):
		self.check_connection()
		fetched = None
		try:
			cursor = self.conn.cursor()
			cursor.execute(query)
		except Exception as err:
			# pass exception to function
			#DatabaseUtilities.print_psycopg2_exception(err)
			print(f'{err}')
			self.conn.rollback()
			return None
		else:
			if fetch:
				fetched = cursor.fetchall()
			if commit:
				self.conn.commit()
		return fetched 
	
	def execute_many_query(self, query: str, data):
		self.check_connection()
		try:
			cursor = self.conn.cursor()
			cursor.executemany(query, data)
		except Exception as err:
			# pass exception to function
			#DatabaseUtilities.print_psycopg2_exception(err)
			print(f'{err}')
			self.conn.rollback()
			return None
		else:
			self.conn.commit()

	def _go_fast_at_all_cost(self):
		"""_summary_
		"""
		# Turning off journal_mode will result in no rollback journal
		self.conn.execute('PRAGMA journal_mode = OFF;')
		# SQLite will not care about writing to disk reliably and hands off that responsibility to the OS
		self.conn.execute('PRAGMA synchronous = 0;')
		# The cache_size specifies how many memory pages SQLite is allowed to hold in the memory
		self.conn.execute('PRAGMA cache_size = 4000000;')  # give it  4GB
		self.conn.execute('PRAGMA locking_mode = EXCLUSIVE;')
		self.conn.execute('PRAGMA temp_store = MEMORY;')

	def _create_compound_table(self):
		query = """
			CREATE TABLE IF NOT EXISTS compound (
				id INTEGER primary key autoincrement,
				inchikey TEXT,
				smiles TEXT,
				formula TEXT,
				compound_exact_mass FLOAT,
				timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
				UNIQUE(smiles));
			"""
		self.execute_query(query)

		for index_col in ['smiles','compound_exact_mass','inchikey']:
			self.execute_query(f"CREATE INDEX IF NOT EXISTS {index_col}_idx on compound ({index_col});")

	def _create_spectra_table(self):
		query =  """
			CREATE TABLE IF NOT EXISTS cfm_spectra (
							id INTEGER primary key autoincrement,
							compound_id INTEGER,
							adduct_type TEXT,
							ionization_type TEXT,
							data_source TEXT,
							precursor_mz FLOAT,
							low_peaks TEXT,
							med_peaks TEXT,
							high_peaks TEXT,
							timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
							UNIQUE(compound_id, adduct_type, ionization_type),
							FOREIGN KEY(compound_id) REFERENCES compound(id));
		"""
		self.execute_query(query)
		self.execute_query("CREATE INDEX IF NOT EXISTS adduct_type_index on cfm_spectra (adduct_type);")
	
	def _create_compound_meta_table(self):
		query = """
			CREATE TABLE IF NOT EXISTS compound_meta (
				id INTEGER primary key autoincrement,
				compound_id INTEGER,
				compound_data_source STRING,
				frequency_mean FLOAT DEFAULT 1,
				frequency_std FLOAT DEFAULT 0,
				timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
				UNIQUE(compound_id, compound_data_source),
				FOREIGN KEY(compound_id) REFERENCES compound(id));
			"""
		self.execute_query(query)

		
		for index_col in ['compound_id','compound_data_source','frequency_mean', 'frequency_std']:
			self.execute_query(f"CREATE INDEX IF NOT EXISTS {index_col}_idx on compound_meta ({index_col});")

	def create_tables(self):
		self._create_compound_table()
		self._create_spectra_table()
		self._create_compound_meta_table()

	def get_compounds_by_colname(self, col_name:str, values_list:list[str|int|float], verbose:bool=False):
		"""_summary_

		Args:
			smiles_list (list[str]): _description_

		Returns:
			_type_: return -1 for not found
		"""
		# TODO use fetch and yelid if there is too many returns
		if col_name in ['id','compound_exact_mass']:
			values_query = ','.join(["{}".format(i) for i in values_list])
		elif col_name in ['inchikey','smiles','formula']:
			values_query = ','.join(["'{}'".format(i) for i in values_list])
		else:
			raise AttributeError(f"col_name need to be one of 'id','compound_exact_mass','inchikey','smiles','formula' not {col_name} ")
		
		selection_query = f"SELECT COALESCE(id,-1),inchikey,smiles,formula,compound_exact_mass FROM compound WHERE {col_name} in ({values_query})"
		if verbose:
			print(f">get_compounds_by_colname {selection_query}")
		return self.execute_query(selection_query, fetch=True)
		
	def add_cfm_spectra(self, cfm_spectra_list: list[CfmSpectra], update_on_exist = True, verbose = False):

		# use upsert so we can get all id
		if update_on_exist:
			compound_query =  """INSERT INTO compound(inchikey, smiles, formula, compound_exact_mass) 
										VALUES (?,?,?,?) 
										ON CONFLICT(smiles) DO UPDATE SET
										inchikey=excluded.inchikey,
										formula=excluded.formula,
										compound_exact_mass=excluded.compound_exact_mass,
										timestamp=CURRENT_TIMESTAMP;"""
		else:
			compound_query = """INSERT OR IGNORE INTO compound(inchikey, smiles, formula, compound_exact_mass) VALUES (?,?,?,?) ; """

		if verbose:
			print(compound_query)

		compound_insert_data = []
		compound_smiles = []
		for cfm_spectra in cfm_spectra_list:
			if cfm_spectra.smiles_or_inchi.startswith("InChI"):
				raise TypeError ("DeepMetCfmSqliteDB require canonical smiles since Inchkey does not work on Deepmet predictions")
			#else:
			#	smiles = cfm_spectra.smiles_or_inchi
			compound_smiles.append(cfm_spectra.smiles_or_inchi)
			compound_insert_data.append((cfm_spectra.inchikey, cfm_spectra.smiles_or_inchi, cfm_spectra.formula,  cfm_spectra.compound_exact_mass))
		self.execute_many_query(compound_query, compound_insert_data)
		
		compound_results = self.get_compounds_by_colname('smiles',compound_smiles, False) 
		compound_db_ids_d = {element[2]: element[0] for element in compound_results if  element[0] != -1}

		# insert if exist update
		if update_on_exist:
			spectra_query = """INSERT INTO cfm_spectra(compound_id, adduct_type, ionization_type, 
									data_source, precursor_mz, low_peaks, med_peaks, high_peaks) 
									VALUES (?,?,?,?,?,?,?,?)		
									ON CONFLICT (compound_id, adduct_type, ionization_type) DO UPDATE SET
									data_source=excluded.data_source,
									precursor_mz=excluded.precursor_mz,
									low_peaks=excluded.low_peaks,
									med_peaks=excluded.med_peaks,
									high_peaks=excluded.high_peaks,
									timestamp=CURRENT_TIMESTAMP;"""
		else:
			spectra_query = """INSERT OR IGNORE INTO cfm_spectra(compound_id, adduct_type, ionization_type, 
									data_source, precursor_mz, low_peaks, med_peaks, high_peaks) 
									VALUES (?,?,?,?,?,?,?,?); """
		
		if verbose:
			print(spectra_query)
		spectra_query_data = []
		for cfm_spectra in cfm_spectra_list:
			compound_db_id = compound_db_ids_d[cfm_spectra.smiles_or_inchi]
			# skip if id == -1
			if compound_db_id == -1:
				continue
			spectra_query_data.append((compound_db_id, cfm_spectra.adduct_type, 
									   cfm_spectra.ionization_type, 
									   cfm_spectra.data_source, 
									   cfm_spectra.precursor_mz,
									   cfm_spectra.get_spectrum("low").get_peak_as_json_str(),
									   cfm_spectra.get_spectrum("med").get_peak_as_json_str(),
									   cfm_spectra.get_spectrum("high").get_peak_as_json_str()))
		self.execute_many_query(spectra_query, spectra_query_data)

	def update_cfm_spectra(self, cfm_spectra_list: list[CfmSpectra], verbose = False):
		self.add_cfm_spectra(cfm_spectra_list, update_on_exist=True, verbose = verbose)
		
	def _construct_spectra_from_results(self, compound_results, meta_results, spectra_results,
										include_no_spectra = True) -> list[CfmSpectra]:
		
		compound_results_d =  {element[0]: element[1:] for element in compound_results if element[0] != -1}
		cfm_spectra_list_d = {}

		for row in spectra_results:
			cid = row[1]
			cfm_spectra = CfmSpectra()
			cfm_spectra.inchikey = compound_results_d[cid][0]
			cfm_spectra.smiles_or_inchi = compound_results_d[cid][1]
			cfm_spectra.formula = compound_results_d[cid][2]
			cfm_spectra.compound_exact_mass = float(compound_results_d[cid][3])
			cfm_spectra.adduct_type = row[2]
			cfm_spectra.ionization_type = row[3]
			cfm_spectra.data_source = row[4]
			cfm_spectra.precursor_mz = row[5]
			
			for spectrum_key in cfm_spectra.get_spectra_keys():
				cfm_spectra.get_spectrum(spectrum_key).adduct_type = cfm_spectra.adduct_type
				cfm_spectra.get_spectrum(spectrum_key).data_source = cfm_spectra.data_source
				#cfm_spectra.get_spectrum(spectrum_key).compound_data_source = cfm_spectra.compound_data_source
				cfm_spectra.get_spectrum(spectrum_key).collision_energy_unit = Standardization.EV
				cfm_spectra.get_spectrum(spectrum_key).precursor_mz = cfm_spectra.precursor_mz
				cfm_spectra.get_spectrum(spectrum_key).compound_exact_mass = cfm_spectra.compound_exact_mass
				cfm_spectra.get_spectrum(spectrum_key).ms_level = Standardization.MS2
				cfm_spectra.get_spectrum(spectrum_key).ionization_type = Standardization.ESI
				cfm_spectra.get_spectrum(spectrum_key).analyzer_type = Standardization.QTOF
				cfm_spectra.get_spectrum(spectrum_key).chromatography_type = Standardization.LC
				cfm_spectra.get_spectrum(spectrum_key).inchikey = cfm_spectra.inchikey
				cfm_spectra.get_spectrum(spectrum_key).smiles_or_inchi = cfm_spectra.smiles_or_inchi
				
			cfm_spectra.get_spectrum('low').collision_energy = 10
			cfm_spectra.get_spectrum('low').reconstruct_peaks_json_str(row[6])
			cfm_spectra.get_spectrum('med').collision_energy = 20
			cfm_spectra.get_spectrum('med').reconstruct_peaks_json_str(row[7])
			cfm_spectra.get_spectrum('high').collision_energy = 40
			cfm_spectra.get_spectrum('high').reconstruct_peaks_json_str(row[8])
			#cfm_spectra_list.append(cfm_spectra)
			if cid not in cfm_spectra_list_d:
				cfm_spectra_list_d[cid] = []

			cfm_spectra_list_d[cid].append(cfm_spectra)

		#cid_data_dict = {}
		cfm_spectra_list = []
		if meta_results is not None:
			for row in meta_results:
				cid = row[1]
				compound_data_source = row[2]
				compound_sample_frequency = row[3]
				if cid in cfm_spectra_list_d:
					for cfm_spectra in cfm_spectra_list_d[cid]:
						#cfm_spectra = copy.deepcopy(s)
						cfm_spectra.compound_data_source = compound_data_source
						for spectrum_key in cfm_spectra.get_spectra_keys():
							cfm_spectra.get_spectrum(spectrum_key).compound_data_source = cfm_spectra.compound_data_source
						cfm_spectra.set_compound_sample_frequency(float(compound_sample_frequency))
						cfm_spectra_list.append(cfm_spectra)
				elif include_no_spectra:
					#print(f"Warning: No spectra for {cid}")
					#print(compound_results_d[cid])
					#assert default_adduct_type is not None, default_adduct_type
					cfm_spectra = CfmSpectra()
					cfm_spectra.inchikey = compound_results_d[cid][0]
					cfm_spectra.smiles_or_inchi = compound_results_d[cid][1]
					cfm_spectra.formula = compound_results_d[cid][2]
					cfm_spectra.compound_exact_mass = compound_results_d[cid][3]
					cfm_spectra.compound_data_source = compound_data_source
     
					for spectrum_key in cfm_spectra.get_spectra_keys():
						cfm_spectra.get_spectrum(spectrum_key).data_source = cfm_spectra.data_source
						cfm_spectra.get_spectrum(spectrum_key).inchikey = cfm_spectra.inchikey
						cfm_spectra.get_spectrum(spectrum_key).smiles_or_inchi = cfm_spectra.smiles_or_inchi

					#cfm_spectra.adduct_type = default_adduct_type
					#cfm_spectra.compute_precursor_mz()
					cfm_spectra.set_compound_sample_frequency(float(compound_sample_frequency))
					cfm_spectra_list.append(cfm_spectra)
					
		else:
			for cid in cfm_spectra_list_d:
				cfm_spectra_list += cfm_spectra_list_d[cid]
				
		return cfm_spectra_list

	def _get_spectra_query_add_on(self,adduct_type, data_source, compound_data_source, csf_lower=None, csf_upper=None):
		
		query = ''
		if not self.table_split_by_adduct and adduct_type is not None:
			query += " AND s.adduct_type = '{}'".format(adduct_type)
		
		if not self.table_split_by_data_source and data_source is not None:
			query += " AND s.data_source  = '{}'".format(data_source)
		
		if not self.table_split_by_compound_data_source and compound_data_source is not None:
			query += " AND s.compound_data_source  = '{}'".format(compound_data_source)

		if csf_lower is not None:
			query += " AND s.compound_sample_frequency >= {}".format(csf_lower)

		if csf_upper is not None:
			query += " AND s.compound_sample_frequency <= {}".format(csf_upper)

		return query
	
	@classmethod
	def _list_to_sqlite(cls, data: list, quote = True):
		if isinstance(data, list):
			if quote:
				return "('" + "','".join([str(d) for d in data]) + "')"
			else:
				return f"({','.join([str(d) for d in data])})"
		else:
			if quote:
				return f"('{data}')"
			else:
				return f"({data})"
		
	def get_cfm_spectra_by_exact_mass(self,
							 compound_exact_mass:float,
							 adduct_type:str,
							 data_source:str|list[str],
							 compound_data_source:str,
							 abs_tol=0.01, ppm_tol=10,
							 mass_tol_by_adduct_type_b: bool = False,
							 min_mass_tol_b: bool = False,
							 include_no_spectra:bool = False,						   
							 verbose=False) -> list[CfmSpectra]:
		"""
		Get spectra by exact mass
		"""
		mass_tol_base = compound_exact_mass if not mass_tol_by_adduct_type_b else Utilities.neutral_mass_to_ion_mass(compound_exact_mass, adduct_type)
		if min_mass_tol_b:
			allowed_tol = Utilities.get_min_mass_tol(mass_tol_base, abs_tol, ppm_tol)
		else:
			allowed_tol = Utilities.get_mass_tol(mass_tol_base, abs_tol, ppm_tol)

		if verbose:
			print("allowed_tol: {} Da".format(allowed_tol))
		
		compounds_query = f"Select * from compound s where s.compound_exact_mass > {compound_exact_mass - allowed_tol} and s.compound_exact_mass < {compound_exact_mass + allowed_tol} "

		# get compound ids
		compounds_results = self.execute_query(compounds_query)
		
		compounds_ids = [r[0] for r in compounds_results]
		cid_str = self._list_to_sqlite(compounds_ids, quote=False)
		if cid_str is None or cid_str == "()":
			return []
			
		if verbose:
			print(compounds_query)
			print(compounds_results)

		meta_query = f"Select * from compound_meta where compound_id in {cid_str}"
		if compound_data_source is not None:
			meta_query += f" AND compound_data_source in {self._list_to_sqlite(compound_data_source)}"
		
		meta_results = self.execute_query(meta_query)
		meta_cid_list  = [r[1] for r in meta_results]

		spectra_query = f" Select * from cfm_spectra where compound_id in {self._list_to_sqlite(meta_cid_list, quote=False)}"
		if adduct_type is not None:
			spectra_query += f" AND adduct_type in {self._list_to_sqlite(adduct_type)}"
		if data_source is not None:
			spectra_query += f" AND data_source in {self._list_to_sqlite(data_source)}"
 
		spectra_results = self.execute_query(spectra_query)

		if verbose:
			print(spectra_query)
			print(meta_query)

			print(meta_results)
			print(spectra_results)

		return self._construct_spectra_from_results(compounds_results, meta_results, spectra_results, include_no_spectra)

	def get_cfm_spectra_by_precursor_mz(self,
							 precursor_mz:float,
							 adduct_type:str,
							 data_source:str|list[str],
							 compound_data_source:str,
							 abs_tol=0.01, ppm_tol=10,
							 min_mass_tol_b: bool = False,							 
							 verbose=False) -> list[Spectrum]:
		"""
		Get spectra by pmass
		"""
		return self.get_cfm_spectra_by_exact_mass(precursor_mz, adduct_type, data_source, compound_data_source, 
												  abs_tol, ppm_tol, True, min_mass_tol_b, verbose)

	def get_cfm_spectra_by_compound(self,
						col_name:str, 
						value_list: list[str],
						adduct_type:str = None,
						verbose=False) -> list[CfmSpectra]:

		"""Method to get cfm spectra by compound colnames

		Args:
			col_name (str): one of 'inchikey','smiles','formula'
			value_list (list[str]): list of values
			adduct_type (str, optional): adduct_type. None to get all Defaults to None.
			verbose (bool, optional): _description_. Defaults to False.

		Raises:
			AttributeError: _description_

		Returns:
			list[CfmSpectra]: _description_
		"""
		if col_name not in ['inchikey','smiles','formula']:
			raise AttributeError(f"colname has to be one of [inchikey,smiles,formula] not {col_name}")
		
		compounds_results = self.get_compounds_by_colname(col_name, value_list, verbose) 
		cid_query = ','.join(["'{}'".format(element[0]) for element in compounds_results])
		query = f"Select * from cfm_spectra s where s.compound_id in ({cid_query})"

		if adduct_type:
			query += f" AND s.adduct_type = '{adduct_type}'"

		if verbose:
			print(query)
		spectra_results = self.execute_query(query)
		if verbose:
			print(spectra_results)
		return self._construct_spectra_from_results(compounds_results, None, spectra_results)

	def get_compounds_by_compound_data_source(self, compound_data_source, verbose=False):

		meta_query = f"Select * from compound_meta Where compound_data_source = '{compound_data_source}'"	   
		meta_results = self.execute_query(meta_query)

		
		if verbose:
			print(meta_query)
			print(meta_results)
		return meta_results
	
	def get_cfm_spectra_by_compound_data_source(self, 
												compound_data_source:str, 
												adduct_type:str = None, 
												data_source:str = None, 
												verbose=False) -> list[CfmSpectra]:

		meta_results  = self.get_compounds_by_compound_data_source(compound_data_source)
		meta_cid_list  = [r[1] for r in meta_results]

		batch_size = 100000
		cfm_spectra_list = []
		for i in tqdm(range(0, len(meta_cid_list), batch_size)):
			batch_cid_list = meta_cid_list[i:i + batch_size]
			cid_str = self._list_to_sqlite(batch_cid_list, quote=False)
			
			#print(cid_str)
			compounds_query = f"Select * from compound s where s.id in {cid_str}"
			# get compound ids
			compounds_results = self.execute_query(compounds_query)

			if verbose:
				print(compounds_query)
				print(compounds_results)
				
			spectra_query = f" Select * from cfm_spectra where compound_id in {cid_str}"
			if adduct_type is not None:
				spectra_query += f" AND adduct_type in {self._list_to_sqlite(adduct_type)}"
			if data_source is not None:
				spectra_query += f" AND data_source in {self._list_to_sqlite(data_source)}"
	
			spectra_results = self.execute_query(spectra_query)

			if verbose:
				print(spectra_query)
				print(spectra_results)
				
			cfm_spectra_list += self._construct_spectra_from_results(compounds_results, meta_results, spectra_results)
		return cfm_spectra_list

	def add_compound_meta(self, meta_data: list[list], 
						  chunk_size:int = 10000,
						  update_on_exist: bool = False,
						  show_progress_bar:bool = False):
		"""
		add compound meta data to compound meta table
		It is caller's responsibility to get provide rdkit canonical smiles 
		"""
		
		for meta_data_chunk in tqdm([meta_data[x:x+chunk_size] for x in range(0, len(meta_data), chunk_size)], desc="Processing", disable=not show_progress_bar):

			compound_smiles = [row[0] for row in meta_data_chunk]
			compound_results = self.get_compounds_by_colname('smiles',compound_smiles, False)
			compound_db_ids_d = {element[2]: element[0] for element in compound_results if  element[0] != -1}

			# use upsert so we can get all id
			if update_on_exist:
				compound_meta_query =  """INSERT INTO compound_meta(compound_id, compound_data_source, frequency_mean, frequency_std) 
											VALUES (?,?,?,?) 
											ON CONFLICT(compound_id, compound_data_source) DO UPDATE SET
											frequency_mean=excluded.frequency_mean,
											frequency_std=excluded.frequency_std,
											timestamp=CURRENT_TIMESTAMP;"""
			else:
				compound_meta_query = """INSERT OR IGNORE INTO compound_meta(compound_id, compound_data_source, frequency_mean, frequency_std) 
											VALUES (?,?,?,?); """

			meta_data = []

			# TODO make this faster
			for row in tqdm(meta_data_chunk, desc="Assemble data for this chunk", disable=not show_progress_bar, leave=False):
				if row[0] in compound_db_ids_d:
					cid = compound_db_ids_d[row[0]]
					row_payload = [cid]
					if len(row) == 2:
						row_payload += row[1:] + [0,0]
					elif len(row) == 3:
						row_payload += row[1:] + [0]
					elif len(row) == 4:
						row_payload += row[1:]
					meta_data.append(row_payload)
			#print("add execute_many_query")
			self.execute_many_query(compound_meta_query, meta_data)
	
	def add_compounds(self, compound_insert_data: list[list], 
					  chunk_size:int = 10000,
					  update_on_exist = False, 
					  show_progress_bar=False):
		"""
		compound_insert_data: [(inchikey, smiles, formula, compound_exact_mass), ...]
		"""
		if update_on_exist:
			compound_query =  """INSERT INTO compound(inchikey, smiles, formula, compound_exact_mass) 
										VALUES (?,?,?,?) 
										ON CONFLICT(smiles) DO UPDATE SET
										inchikey=excluded.inchikey,
										formula=excluded.formula,
										compound_exact_mass=excluded.compound_exact_mass,
										timestamp=CURRENT_TIMESTAMP;"""
		else:
			compound_query = """INSERT OR IGNORE INTO compound(inchikey, smiles, formula, compound_exact_mass) VALUES (?,?,?,?) ; """

		for compound_insert_data_chunk in tqdm([compound_insert_data[x:x+chunk_size] for x in range(0, len(compound_insert_data), chunk_size)], desc="Processing", disable=not show_progress_bar):
			self.execute_many_query(compound_query, compound_insert_data_chunk)
	
	def delete_cfm_spectra_by_cids(self, cids, 
								   batch_size = 10000):
		
		for i in tqdm(range(0, len(cids), batch_size)):
			batch_cid_list = cids[i:i + batch_size]
			cid_str = self._list_to_sqlite(batch_cid_list, quote=False)
			spectra_query = f" Delete from cfm_spectra where compound_id in {cid_str}"
			self.execute_query(spectra_query)
			
	def delete_compound_by_cids(self, cids, 
								   batch_size = 10000):
		
		for i in tqdm(range(0, len(cids), batch_size)):
			batch_cid_list = cids[i:i + batch_size]
			cid_str = self._list_to_sqlite(batch_cid_list, quote=False)
			spectra_query = f" Delete from compound where id in {cid_str}"
			self.execute_query(spectra_query)
			
	def delete_compound_meta_by_compound_data_source(self, compound_data_source):
		
		spectra_query = f" DELETE from compound_meta  WHERE compound_data_source = '{compound_data_source}'"
		self.execute_query(spectra_query)