from typing import List, Optional

from cfmtoolkit.utils import Utilities
from .pg_db_base import PgDbBase


class PubChemRecord(object):
	def __init__(self, cid, smiles) -> None:
		self.cid = cid
		self.smiles = smiles
		self.inchikey = Utilities.get_inchikey_by_chemid(smiles)
		self.exact_mass = Utilities.get_mol_mw_by_chemid(smiles)

	
class PubChemPgDB(PgDbBase):
	def create_tables(self, verbose=False):
		query = """
			CREATE TABLE IF NOT EXISTS molecule (
				cid integer PRIMARY KEY UNIQUE,
				inchikey VARCHAR (27) NOT NULL UNIQUE,
				smiles text NOT NULL,
				exact_mass double precision NOT NULL
			);
			CREATE INDEX ON molecule (inchikey);
			CREATE INDEX ON molecule (exact_mass);
			"""
		if verbose:
			print(query)
		self.execute_query(query, commit=True, fetch=False)

	def add_records(self, record_list: List[PubChemRecord], verbose=False):
		"""
		"""
		query = ""
		for record in record_list:
			template = "(" + ','.join(["'{}'" for _ in range(4)]) + ")"
			query += """INSERT INTO molecule(
				cid,
				inchikey,
				smiles,
				exact_mass
				) VALUES {} 
				ON CONFLICT("inchikey") DO UPDATE SET inchikey=EXCLUDED.inchikey 
				returning molecule.cid;\n
				""".format(template.format(
				record.cid,
				record.inchikey,
				record.smiles,
				record.exact_mass
			))
		# print(insert_payloads)

		if verbose:
			print(query)
		self.execute_query(query, commit=True)

	def get_inchikey_smiles_by_mass(self, compound_exact_mass:float, abs_tol:float = 0.01, ppm_tol:float = 10) -> List[List[str]]:
		allowed_tol = Utilities.get_mass_tol(
			compound_exact_mass, abs_tol, ppm_tol)
		query = """Select inchikey,smiles from molecule m where m.exact_mass > {} and m.exact_mass < {}""".format(
			compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol)
		results = self.execute_query(query)
		smiles_list = [[x[0],x[1]] for x in results]
		
		return smiles_list

	def get_inchikey_smiles_by_cids_and_mass(self, cids: List[int], compound_exact_mass:float, abs_tol:float = 0.01, ppm_tol:float = 10) -> List[List[str]]:

		allowed_tol = Utilities.get_mass_tol(
			compound_exact_mass, abs_tol, ppm_tol)
		query = """Select inchikey,smiles from molecule m where m.cid in ({}) and m.exact_mass > {} and m.exact_mass < {}""".format(','.join([str(cid) for cid in cids]), compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol)
		results = self.execute_query(query)
		smiles_list = [[x[0],x[1]] for x in results]
		
		return smiles_list


	def is_inchikey_in_pubchem(self, inchikey):

		query = """select exists(select 1 from molecule where inchikey='{}')""".format(inchikey)
		#print(query)
		results = self.execute_query(query)
		#print(type(results[0][0]))
		return results[0][0]
	
	def get_cid_by_inchikey(self, inchikey) -> Optional[int]:

		query = """select cid from molecule where inchikey='{}'""".format(inchikey)
		#print(query)
		results = self.execute_query(query)
		#print(type(results[0][0]))
		cid = results[0][0] if len(results) == 1 and len(results[0]) == 1 else None
		return cid