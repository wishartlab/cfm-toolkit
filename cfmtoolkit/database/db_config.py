from typing import Optional

class PgDBConfig(object):
	def __init__(self, db_host: str, db_port: str, db_usr: str, db_pwd: str, db_name: str, 
		table_split_by_adduct:Optional[bool] =False, 
		table_split_by_data_source:Optional[bool] =False,
		table_split_by_compound_data_source:Optional[bool] =False):
		self.db_host: str = db_host
		self.db_port: str = db_port
		self.db_usr: str = db_usr
		self.db_pwd: str = db_pwd
		self.db_name: str = db_name

		self.table_split_by_adduct = table_split_by_adduct
		self.table_split_by_data_source = table_split_by_data_source
		self.table_split_by_compound_data_source = table_split_by_compound_data_source
		self.db_type = 'Postgresql'
		
	def get_connection_str(self) -> str:
		return "host='{}' port='{}' dbname='{}' user='{}' password='{}'".format(self.db_host, self.db_port,
																				self.db_name, self.db_usr, self.db_pwd)

	def get_dsn_connection_str(self) -> str:
		return "postgresql://{}:{}@{}/{}".format(self.db_usr, self.db_pwd, self.db_host + ':' + self.db_port,
												 self.db_name)

class SqliteDBConfig(object):
	def __init__(self, db_file:str):
		self.db_file =  db_file
		self.db_type = 'Sqlite'
		
