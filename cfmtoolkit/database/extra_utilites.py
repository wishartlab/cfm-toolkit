from cfmtoolkit.io import CFM
from cfmtoolkit.utils import Utilities

from os import  listdir
from os.path import isfile, join
from copy import deepcopy

from concurrent.futures import ProcessPoolExecutor
from tqdm import tqdm


from cfmtoolkit.standardization import Standardization

class ExtraUtilities:

	@staticmethod
	def add_cfm_spectra_batch(db_config, spectra_dir, meta_data, num_processor = 3, chunk_size=100, \
							  check_charges=True,
							allowed_adducts = [Standardization.M_H_pos,Standardization.M_H_neg], \
							   pre_process_spectra = False, intensity_threshold=1.0, 
							   centroid=False, mz_distance=0.1, update_instead_of_add = False, verbose = False):

		cfm_spectra_files = [join(spectra_dir, f) for f in listdir(spectra_dir) if isfile(join(spectra_dir, f)) and (f.endswith('log') or f.endswith('txt'))] 
		added_count = 0
		num_tasks = len(cfm_spectra_files)
		
		if verbose:
			for spectra_file in cfm_spectra_files:
				print('Found spectra file: {}'.format(spectra_file))

	   
		with ProcessPoolExecutor(max_workers=num_processor) as executor:
			added_count = sum(list(tqdm(executor.map(
				ExtraUtilities._process_file,[db_config] * num_tasks, cfm_spectra_files, [meta_data] * num_tasks, [chunk_size] * num_tasks, [check_charges ]* num_tasks,\
					  [allowed_adducts] * num_tasks, [pre_process_spectra]* num_tasks,\
					  [intensity_threshold] * num_tasks, [centroid] * num_tasks,[mz_distance] * num_tasks,\
					  [update_instead_of_add] * num_tasks), total= num_tasks)))
		return added_count
	
	
	@staticmethod
	def add_cfm_spectra_seq(db_config, spectra_dir, meta_data, chunk_size=100, \
							  check_charges=True,
							allowed_adducts = [Standardization.M_H_pos,Standardization.M_H_neg], \
							   pre_process_spectra = False, intensity_threshold=1.0, 
							   centroid=False, mz_distance=0.1, update_instead_of_add = False):

		cfm_spectra_files = [join(spectra_dir, f) for f in listdir(spectra_dir) if isfile(join(spectra_dir, f)) and (f.endswith('log') or f.endswith('txt'))] 
		added_count = 0		
		for spectra_file in tqdm(cfm_spectra_files, desc= spectra_dir):
			added_count += ExtraUtilities._process_file(db_config, spectra_file, meta_data, chunk_size, check_charges, allowed_adducts,
							   pre_process_spectra, intensity_threshold, centroid, mz_distance, update_instead_of_add)
		return added_count
		
	@staticmethod
	def _process_file(db_config, spectra_file, meta_data, chunk_size, check_charges, allowed_adducts,
							   pre_process_spectra, intensity_threshold, centroid, mz_distance, update_on_exist = False):

		def process_block(input_strs, meta_data):
			sort_peaks = False
			normalize_peaks = False

			if meta_data is None:
				current_meta_data = {}
			else:
				current_meta_data = deepcopy(meta_data)


			cfm_spectra = CFM.parse_esi_spectra_str_with_meta(input_strs, current_meta_data, sort_peaks, normalize_peaks)
			if not cfm_spectra.is_complete():
				return None
			
			if cfm_spectra.adduct_type not in allowed_adducts:
				#print(cfm_spectra.adduct_type, 'is not allowed')
				return None
 
			if pre_process_spectra:
				cfm_spectra.remove_noise( intensity_threshold, centroid, mz_distance)

			if check_charges:
				has_charge = Utilities.has_formal_charge_chemid(cfm_spectra.smiles_or_inchi)
				if has_charge:
					return None

			return cfm_spectra

		added_count = 0 

		if db_config.db_type == 'Sqlite':
			from cfmtoolkit.database import DeepMetCfmSqliteDB
			db = DeepMetCfmSqliteDB(db_config)
		else:
			from cfmtoolkit.database import CfmPgDB2
			db = CfmPgDB2(db_config)

		with open(spectra_file, 'r') as cfm_file:
			db.connect()
			if db_config.db_type == 'Sqlite':
				db._go_fast_at_all_cost()
				
			input_strs = []
			cfm_spectra_list = []
			for row in cfm_file:
				if len(input_strs) != 0 and "spectra" in row.lower():
					cfm_spectra = process_block(input_strs, meta_data)
					if cfm_spectra is not None:
						cfm_spectra_list.append(cfm_spectra)
					input_strs = []
				input_strs.append(row.rstrip())
				
				if len(cfm_spectra_list) == chunk_size:
					if update_on_exist:
						db.update_cfm_spectra(cfm_spectra_list)
					else:
						db.add_cfm_spectra(cfm_spectra_list)
					added_count += len(cfm_spectra_list)
					cfm_spectra_list = []


			cfm_spectra = process_block(input_strs, meta_data)
			
			if cfm_spectra is not None:
				cfm_spectra_list.append(cfm_spectra)
				if update_on_exist:
					db.update_cfm_spectra(cfm_spectra_list)
				else:
					db.add_cfm_spectra(cfm_spectra_list)
				added_count += len(cfm_spectra_list)
				cfm_spectra_list = []
			db.disconnect()
			
		return added_count