from typing import List, Optional

from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from cfmtoolkit.io import CFM
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities
from .database_utilities import DatabaseUtilities
from .pg_db_base import PgDbBase
from .db_config import PgDBConfig

from os import walk
from os.path import  join
from tqdm import tqdm

class CfmPgDB2(PgDbBase):

	_adduct_type_table_name_map = { 
		Standardization.M_H_pos: 'M_H_pos',
		Standardization.M_H_neg: 'M_H_neg',
	}

	def __init__(self, db_config: PgDBConfig):
		super().__init__(db_config)
		self.table_split_by_adduct = db_config.table_split_by_adduct
		self.table_split_by_data_source = db_config.table_split_by_data_source
		self.table_split_by_compound_data_source = db_config.table_split_by_compound_data_source
	

	def _get_create_table_query(self, table_name):
		
		adduct_str = "" if self.table_split_by_adduct else "adduct_type text,"
		data_source_str  = "" if self.table_split_by_data_source else "data_source text,"
		compound_data_source_str = "" if self.table_split_by_compound_data_source else "compound_data_source text,"
		
		query = """
			CREATE TABLE IF NOT EXISTS {} (
				id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
				inchikey text,
				smiles_or_inchi text,
				formula text,
				compound_exact_mass double precision,
				compound_sample_frequency float,
				{} {} {}
				low_peaks_mz double precision[],
				low_peaks_intensity double precision[],
				low_peaks_annotation text[],
				med_peaks_mz double precision[],
				med_peaks_intensity double precision[],
				med_peaks_annotation text[],
				high_peaks_mz double precision[],
				high_peaks_intensity double precision[],
				high_peaks_annotation text[],
				time_stamp TIMESTAMP default now()
			);
			CREATE INDEX ON {} (compound_exact_mass);
			CREATE INDEX ON {} (inchikey);
			""".format(table_name,adduct_str,data_source_str,compound_data_source_str,table_name, table_name)

		# if not split table we need to add index on compound_exact_mass
		if not self.table_split_by_adduct:
			query += """CREATE INDEX ON {} (adduct_type);""".format(table_name)
		if not self.table_split_by_data_source:
			query += """CREATE INDEX ON {} (data_source);""".format(table_name)
		if not self.table_split_by_compound_data_source:
			query += """CREATE INDEX ON {} (compound_data_source);""".format(table_name)

		#print(query)
		return query
		

	def create_tables(self, adduct_types:Optional[List[str]] = None, data_sources:Optional[List[str]] = None, compound_data_sources:Optional[List[str]] = None):
		"""
		Create tables for each adduct type and/or compound data source and/or data source
		"""		
		table_names = ['cfm_spectra']
		split_flags = [self.table_split_by_adduct,self.table_split_by_data_source,self.table_split_by_compound_data_source]
		split_sources = [[self._adduct_type_table_name_map[t] for t in adduct_types] , data_sources, compound_data_sources]

		for split_flag, split_source in zip (split_flags, split_sources):
			if split_flag:
				new_table_name = []
				for table_name in table_names:
					for split_source_name in split_source:
						new_table_name.append(table_name + '_' + self._convert_to_legal_table_str(split_source_name))
				table_names = new_table_name
				
		for table_name in tqdm(table_names):
			query = self._get_create_table_query(table_name)
			print(query)
			self.execute_query(query, commit=True, fetch=False)
		   

	def _convert_to_legal_table_str(self, input_str):
		legal_str = input_str.lower().replace('-', '_').replace(' ', '')
		return legal_str


	def _get_table_name(self, adduct_type, data_source, compound_data_source):
		table_name = 'cfm_spectra'
		if self.table_split_by_adduct:
			table_name += "_" + self._convert_to_legal_table_str(self._adduct_type_table_name_map[adduct_type])
		if self.table_split_by_data_source:
			table_name += "_" +  self._convert_to_legal_table_str(data_source)
		if self.table_split_by_compound_data_source:
			table_name += "_" +  self._convert_to_legal_table_str(compound_data_source)

		return table_name

	def _get_insert_update_payload(self, cfm_spectra_list: List[CfmSpectra]):
		insert_payloads = {}
		
		payload_template = "('{}','{}','{}',{},{}"
		text_cell_template = ",'{}'"
		payload_template +=  text_cell_template if not self.table_split_by_adduct else ""
		payload_template +=  text_cell_template if not self.table_split_by_data_source else ""
		payload_template +=  text_cell_template if not self.table_split_by_compound_data_source else ""

		#payload_template = "(" + ','.join(["'{}'" for _ in range(3)]) + ",{},{}," + ','.join(["'{}'" for _ in range(num_cols - 11)]) 
		payload_template += ",'{}'::double precision[],'{}'::double precision[],'{}'::text[]" * 3 + ", NOW())"

		#print(payload_template)
		for cfm_spectra in cfm_spectra_list:
			escaped_smiles_or_inchi = DatabaseUtilities.escape_single_quote(cfm_spectra.smiles_or_inchi)
			table_name =  self._get_table_name(cfm_spectra.adduct_type, cfm_spectra.data_source, cfm_spectra.compound_data_source)

			if table_name not in insert_payloads:
				insert_payloads[table_name] = []
			
			payload_values = [cfm_spectra.inchikey,
				escaped_smiles_or_inchi,
				cfm_spectra.formula,
				cfm_spectra.compound_exact_mass,
				cfm_spectra.compound_sample_frequency]


			if not self.table_split_by_adduct:
				payload_values.append(cfm_spectra.adduct_type)
			if not self.table_split_by_data_source:
				payload_values.append(cfm_spectra.data_source)
			if not self.table_split_by_compound_data_source:
				payload_values.append(cfm_spectra.compound_data_source)

			for spectrum_key in cfm_spectra.get_spectra_keys():
				payload_values += ["{" + ','.join([str(peak.mz)  for peak in cfm_spectra.get_spectrum(spectrum_key).peaks]) + "} ",
					"{" + ','.join([str(peak.intensity) for peak in cfm_spectra.get_spectrum(spectrum_key).peaks]) + "}",
					"{" + ','.join([peak.annotation if peak.annotation != Standardization.NA else '""' for peak in cfm_spectra.get_spectrum(spectrum_key).peaks]) + "}"]

			#print(payload_values)
			insert_payloads[table_name].append(payload_template.format(*payload_values))
		return insert_payloads

	def _get_add_cfm_spectrum_query(self, cfm_spectra_list: List[CfmSpectra]):

		insert_payloads = self._get_insert_update_payload(cfm_spectra_list)

		queries = []
		adduct_str = "" if self.table_split_by_adduct else "adduct_type,"
		data_source_str  = "" if self.table_split_by_data_source else "data_source,"
		compound_data_source_str = "" if self.table_split_by_compound_data_source else "compound_data_source,"
		
		query_template = """INSERT INTO {} (
					inchikey,
					smiles_or_inchi,
					formula,
					compound_exact_mass,
					compound_sample_frequency,""" \
					+ adduct_str\
					+ data_source_str\
					+ compound_data_source_str\
					+ """low_peaks_mz,
					low_peaks_intensity,
					low_peaks_annotation,
					med_peaks_mz,
					med_peaks_intensity,
					med_peaks_annotation,
					high_peaks_mz,
					high_peaks_intensity,
					high_peaks_annotation,
					time_stamp
					) VALUES {};"""

		for table_name in insert_payloads:
			query = query_template.format(table_name, ','.join(insert_payloads[table_name]))

			queries.append(query)
		return queries

   

	def _get_update_cfm_spectrum_query(self, cfm_spectra_list: List[CfmSpectra]):

		payloads = self._get_insert_update_payload(cfm_spectra_list)

		queries = []
		adduct_update_str = "" if self.table_split_by_adduct else "adduct_type = c.adduct_type,"
		data_source_update_str  = "" if self.table_split_by_data_source else "data_source = c.data_source,"
		compound_data_source_update_str = "" if self.table_split_by_compound_data_source else "compound_data_source = c.compound_data_source,"
		
		adduct_str = "" if self.table_split_by_adduct else "adduct_type,"
		data_source_str  = "" if self.table_split_by_data_source else "data_source,"
		compound_data_source_str = "" if self.table_split_by_compound_data_source else "compound_data_source,"	

		query_template = """Update {} as t SET
					inchikey = c.inchikey,
					smiles_or_inchi = c.smiles_or_inchi,
					formula = c.formula,
					compound_exact_mass = c.compound_exact_mass,
					compound_sample_frequency = c.compound_sample_frequency,""" \
					+ adduct_update_str\
					+ data_source_update_str\
					+ compound_data_source_update_str\
					+ """low_peaks_mz = c.low_peaks_mz,
					low_peaks_intensity = c.low_peaks_intensity,
					low_peaks_annotation = c.low_peaks_annotation,
					med_peaks_mz = c.med_peaks_mz,
					med_peaks_intensity = c.med_peaks_intensity,
					med_peaks_annotation = c.med_peaks_annotation,
					high_peaks_mz = c.high_peaks_mz,
					high_peaks_intensity = c.high_peaks_intensity,
					high_peaks_annotation = c.high_peaks_annotation,
					time_stamp = c.time_stamp
					FROM ( VALUES {}) as c(
					inchikey,
					smiles_or_inchi,
					formula,
					compound_exact_mass,
					compound_sample_frequency,""" \
					+ adduct_str\
					+ data_source_str\
					+ compound_data_source_str\
					+ """low_peaks_mz,
					low_peaks_intensity,
					low_peaks_annotation,
					med_peaks_mz,
					med_peaks_intensity,
					med_peaks_annotation,
					high_peaks_mz,
					high_peaks_intensity,
					high_peaks_annotation,
					time_stamp)
					where c.inchikey = t.inchikey;"""

		for table_name in payloads:
			query = query_template.format(table_name, ','.join(payloads[table_name]))
			queries.append(query)

		return queries

	def add_cfm_spectra(self, cfm_spectra_list: List[CfmSpectra], verbose=False):
		queries = self._get_add_cfm_spectrum_query(cfm_spectra_list)
		for query in queries:
			if verbose:
				print(query)
			self.execute_query(query, commit=True, fetch=False)

	def update_cfm_spectra(self, cfm_spectra_list: List[CfmSpectra], verbose=False):
		queries = self._get_update_cfm_spectrum_query(cfm_spectra_list)
		for query in queries:
			if verbose:
				print(query)
			self.execute_query(query, commit=True, fetch=False)

	def _construct_spectra_from_results(self, spectra_results, adduct_type = Standardization.NA, data_source= Standardization.NA, compound_data_source= Standardization.NA):

		cfm_spectra_list = []
		# Some hard code
		# not the world best code 
		# but it works
		for row in spectra_results:
			cfm_spectra = CfmSpectra()
			cfm_spectra.inchikey = row[1]
			cfm_spectra.smiles_or_inchi = row[2]
			cfm_spectra.formula = row[3]
			cfm_spectra.compound_exact_mass = float(row[4])
			cfm_spectra.compound_sample_frequency = int(row[5])
			
			next_id = 6
			if not self.table_split_by_adduct:
				cfm_spectra.adduct_type = row[next_id]
				next_id += 1
			else:
				cfm_spectra.adduct_type = adduct_type

			if not self.table_split_by_data_source:
				cfm_spectra.data_source = row[next_id]
				next_id += 1
			else:
				cfm_spectra.data_source = data_source

			if not self.table_split_by_compound_data_source:
				cfm_spectra.compound_data_source = row[next_id]
				next_id += 1
			else:
				cfm_spectra.compound_data_source = compound_data_source

			cfm_spectra.precursor_mz = Utilities.neutral_mass_to_ion_mass(cfm_spectra.compound_exact_mass, cfm_spectra.adduct_type)
			
			for spectrum_key in cfm_spectra.get_spectra_keys():
				cfm_spectra.get_spectrum(spectrum_key).add_peaks(row[next_id], row[next_id + 1], 
					[ Standardization.NA if anno == '' else anno  for anno in row[next_id + 2]])
				cfm_spectra.get_spectrum(spectrum_key).adduct_type = cfm_spectra.adduct_type
				cfm_spectra.get_spectrum(spectrum_key).data_source = cfm_spectra.data_source
				cfm_spectra.get_spectrum(spectrum_key).compound_data_source = cfm_spectra.compound_data_source
				cfm_spectra.get_spectrum(spectrum_key).collision_energy_unit = Standardization.EV
				cfm_spectra.get_spectrum(spectrum_key).precursor_mz = cfm_spectra.precursor_mz
				cfm_spectra.get_spectrum(spectrum_key).compound_exact_mass = cfm_spectra.compound_exact_mass
				cfm_spectra.get_spectrum(spectrum_key).ms_level = Standardization.MS2
				cfm_spectra.get_spectrum(spectrum_key).ionization_type = Standardization.ESI
				cfm_spectra.get_spectrum(spectrum_key).analyzer_type = Standardization.QTOF
				cfm_spectra.get_spectrum(spectrum_key).chromatography_type = Standardization.LC
				cfm_spectra.get_spectrum(spectrum_key).inchikey = cfm_spectra.inchikey
				cfm_spectra.get_spectrum(spectrum_key).smiles_or_inchi = cfm_spectra.smiles_or_inchi
				
				#cfm_spectra.get_spectrum(spectrum_key).compound_data_source = cfm_spectra.compound_data_source

				next_id+=3

			cfm_spectra.get_spectrum('low').collision_energy = 10
			cfm_spectra.get_spectrum('med').collision_energy = 20
			cfm_spectra.get_spectrum('high').collision_energy = 40
			


			cfm_spectra_list.append(cfm_spectra)			   
		return cfm_spectra_list

	def _get_spectra_query_add_on(self,adduct_type, data_source, compound_data_source, csf_lower=None, csf_upper=None):
		
		query = ''
		if not self.table_split_by_adduct and adduct_type is not None:
			query += " AND s.adduct_type = '{}'".format(adduct_type)
		
		if not self.table_split_by_data_source and data_source is not None:
			query += " AND s.data_source  = '{}'".format(data_source)
		
		if not self.table_split_by_compound_data_source and compound_data_source is not None:
			query += " AND s.compound_data_source  = '{}'".format(compound_data_source)

		if csf_lower is not None:
			query += " AND s.compound_sample_frequency >= {}".format(csf_lower)

		if csf_upper is not None:
			query += " AND s.compound_sample_frequency <= {}".format(csf_upper)

		return query

	def get_cfm_spectra_by_exact_mass(self,
							 compound_exact_mass:float,
							 adduct_type:str,
							 data_source:str,
							 compound_data_source:str,
							 abs_tol=0.01, ppm_tol=10,
							 csf_lower=None,
							 csf_upper=None,
							 verbose=False) -> List[Spectrum]:
		"""
		Get spectra by exact mass
		"""
		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		allowed_tol = Utilities.get_mass_tol(compound_exact_mass, abs_tol, ppm_tol)
		if verbose:
			print("allowed_tol: {} Da".format(allowed_tol))
		query = """Select * from {} s where s.compound_exact_mass > {} and s.compound_exact_mass < {}""".format(table_name,
			compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol)

		query += self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source, csf_lower, csf_upper)

		if verbose:
			print(query)
		results = self.execute_query(query)
		return self._construct_spectra_from_results(results, adduct_type, data_source, compound_data_source)

	def get_cfm_spectra_by_precursor_mz(self,
							 precursor_mz:float,
							 adduct_type:str,
							 data_source:str,
							 compound_data_source:str,
							 abs_tol=0.01, ppm_tol=10,
							 csf_lower=None,
							 csf_upper=None,
							 verbose=False) -> List[CfmSpectra]:
		"""
		Get spectra by exact mass
		"""

		compound_exact_mass = Utilities.ion_mass_to_neutral_mass(precursor_mz, adduct_type)
		#if verbose:
		#	print("precursor_mz: {}".format(precursor_mz))
		#	print("adduct_type: {}".format(adduct_type))
		#	print("compound_exact_mass: {}".format(compound_exact_mass))
		return self.get_cfm_spectra_by_exact_mass(compound_exact_mass, adduct_type, data_source, compound_data_source, abs_tol, ppm_tol, csf_lower, csf_upper, verbose)
		
	def get_inchikey_count_by_mass(self, 
							 compound_exact_mass:float,
							 adduct_type:str,
							 data_source:str,
							 compound_data_source:str,
		abs_tol=0.01, ppm_tol=10,verbose=False) -> int:

		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		allowed_tol = Utilities.get_mass_tol(compound_exact_mass, abs_tol, ppm_tol)
		if verbose:
			print("allowed tol: {} Da, mass range: {}Da, {} Da".format(allowed_tol, compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol))

		query = """Select COUNT(*)  from {} s where s.compound_exact_mass > {} and s.compound_exact_mass < {}""".format(table_name,
			compound_exact_mass - allowed_tol, compound_exact_mass + allowed_tol)

		query += self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source)


		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)[0][0]
		return results


	def get_cfm_spectrum_count(self,
							 adduct_type:str,
							 data_source:str,
							 compound_data_source:str,
							 verbose=False) -> int:

		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		query = """Select count(*)  from {} s""".format(table_name)
		query += self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source)

		if verbose:
			print(query)
		count = self.execute_query(query, commit=True)
		# print(count[0][0])
		return int(count[0][0])

	def get_cfm_spectra_by_inchikeys(self, inchikeys: List[str],
									  adduct_type:str,
									  data_source:str,
									  compound_data_source:str,
									 verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""

		inchikeys_query = ','.join(["'{}'".format(i) for i in inchikeys])
		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		#print(table_name)
		query = """Select * from {} s where s.inchikey in ({})""".format(table_name, inchikeys_query)
		query += self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source)
		# print(query)
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)

		return self._construct_spectra_from_results(results, adduct_type, data_source, compound_data_source)

	def get_cfm_spectra_by_formulas(self, formulas: List[str], 
				adduct_type:str,
				data_source:str,
				compound_data_source:str,
				verbose: bool = False) -> List[CfmSpectra]:

		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		
		formulas_query = ','.join(["'{}'".format(i) for i in formulas])
		query = """Select * from {} s where s.formula in ({})""".format(table_name, formulas_query)
		query += self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source)
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)

		return self._construct_spectra_from_results(results, adduct_type, data_source, compound_data_source)

	def get_all_unique_inchikeys(self, adduct_type:str,
							data_source:str,
							compound_data_source:str,
							chunk_size = 1,
							verbose=False) -> List[CfmSpectra]:
		"""Method to get cfm spectra by inchikeys

		Args:
			inchikeys (List[str]): [description]
			adduct_type ([type], optional): [description]. Defaults to None.
			data_source ([type], optional): [description]. Defaults to None.
			verbose (bool, optional): [description]. Defaults to False.

		Returns:
			List[CfmSpectra]: [description]
		"""
		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)

		query = """select distinct inchikey from {} s """.format(table_name)
		add_on_query = self._get_spectra_query_add_on(adduct_type, data_source, compound_data_source)
		if add_on_query != '':
			query += ' WHERE ' + add_on_query[4:]
			
		if verbose:
			print(query)
		results = self.execute_query(query)
		if verbose:
			print(results)

		inchikeys = [ item[0] for item in results]
		if chunk_size > 1:
			inchikeys = [inchikeys[i:i + chunk_size] for i in range(0, len(inchikeys), chunk_size)]
		return inchikeys
	

	def get_cfm_spectra(self,
						adduct_type:str,
						data_source:str,
						compound_data_source:str,
						limited:int = 100,
						offset:int = 0,
						verbose=False) -> List[Spectrum]:
		table_name = self._get_table_name(adduct_type, data_source, compound_data_source)
		query = f"""Select * from {table_name} s LIMIT {limited} OFFSET {offset} """
		if verbose:
			print(query)
		results = self.execute_query(query)
		return self._construct_spectra_from_results(results, adduct_type, data_source, compound_data_source)