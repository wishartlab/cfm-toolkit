import psycopg2

from .database_utilities import DatabaseUtilities
from .db_config import PgDBConfig

class PgDbBase(object):
	def __init__(self, config: PgDBConfig):
		self.config = config
		self.conn = None

	def __enter__(self):
		self.connect()
		return self

	def __exit__(self, exception_type, exception_value, traceback):
		# save cache on exit
		self.disconnect()

	def connect(self, connection_str=None):
		self.disconnect()
		if connection_str is None:
			connection_str = self.config.get_connection_str()

		# Try to connect
		# conn = None
		try:
			conn = psycopg2.connect(connection_str)
		except:
			print("Unable to connect to the database with connection command: {} ".format(
				connection_str))
		else:
			# print("Database Connection Established")
			self.conn = conn

	def disconnect(self):
		if self.conn is not None:
			self.conn.cursor().close()
			self.conn.close()

	def check_connection(self):
		if self.conn is None:
			raise ValueError('No Connection')

	def execute_query(self, query: str, commit: bool = False, fetch: bool = True):
		#print('>check_connection')
		self.check_connection()
		try:
			#print('>cursor')
			cursor = self.conn.cursor()
			#print('>execute')
			cursor.execute(query)
		except Exception as err:
			# pass exception to function
			DatabaseUtilities.print_psycopg2_exception(err)
			print(query)
			self.conn.rollback()
			return None
		else:
			if commit:
				self.conn.commit()
			if fetch:
				return cursor.fetchall()
