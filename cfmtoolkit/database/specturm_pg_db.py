from typing import List, Optional

from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from cfmtoolkit.io import CFM
from cfmtoolkit.standardization import Standardization
from cfmtoolkit.utils import Utilities
from .database_utilities import DatabaseUtilities
from .pg_db_base import PgDbBase
from os import walk
from os.path import  join
from tqdm import tqdm
import time

class SpectrumPgDB(PgDbBase):
	def create_tables(self, verbose=False):
		query = """
			CREATE TABLE IF NOT EXISTS spectrum (
				id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
				inchikey text NOT NULL,
				smiles_or_inchi text NOT NULL,
				formula text NOT NULL,
				compound_name text,
				compound_data_source text,
				compound_data_source_id text,
				compound_exact_mass double precision NOT NULL,
				chromatography_type text,
				adduct_type  text,
				analyzer_type  text,
				ionization_type text,
				charge_type text,
				ms_level text,
				collision_energy_prefix text,
				collision_energy_unit text,
				collision_method text,
				collision_gas_type text,
				collision_energy float,
				mz_decimal_place smallint,
				precursor_mz double precision,
				data_source text,
				data_source_id text,
				compound_sample_frequency integer,
				peaks_mz double precision[],
				peaks_intensity double precision[],
				peaks_annotation text[]
			);
			CREATE INDEX ON spectrum (adduct_type);
			CREATE INDEX ON spectrum (compound_exact_mass);
			CREATE INDEX ON spectrum (precursor_mz);
			"""
		# CREATE INDEX ON spectrum (inchikey);
		if verbose:
			print(query)
		self.execute_query(query, commit=True, fetch=False)

	def _get_add_spectrum_query_batch(self, spectrum_list: List[Spectrum]):
		# print(molecule_id)
		insert_payloads = []
		payload_template = "(" + ','.join(["'{}'" for _ in range(26)]) +  ")"
		for spectrum in spectrum_list:
			escaped_smiles_or_inchi = DatabaseUtilities.escape_single_quote(spectrum.smiles_or_inchi)
			escaped_compound_name = DatabaseUtilities.escape_single_quote(spectrum.compound_name)
			# TODO FIX THIS, THIS is a hack
			insert_payloads.append(payload_template.format(
				spectrum.inchikey,
				escaped_smiles_or_inchi,
				spectrum.formula,
				escaped_compound_name,
				spectrum.compound_data_source,
				spectrum.compound_data_source_id,
				spectrum.compound_exact_mass,
				spectrum.chromatography_type,
				spectrum.adduct_type,
				spectrum.analyzer_type,
				spectrum.ionization_type,
				spectrum.charge_type,
				spectrum.ms_level,
				spectrum.collision_energy_prefix,
				spectrum.collision_energy_unit,
				spectrum.collision_method,
				spectrum.collision_gas_type,
				spectrum.collision_energy,
				spectrum.mz_decimal_place,
				spectrum.precursor_mz,
				spectrum.data_source,
				spectrum.data_source_id,
				spectrum.compound_sample_frequency,
				"{" + ','.join([str(peak.mz)  for peak in spectrum.peaks]) + "}",
				"{" + ','.join([str(peak.intensity) for peak in spectrum.peaks]) + "}",
				"{" + ','.join([peak.annotation for peak in spectrum.peaks]) + "}"
			))

		# print(insert_payloads)
		query = """INSERT INTO spectrum(
				inchikey,
				smiles_or_inchi,
				formula,
				compound_name,
				compound_data_source,
				compound_data_source_id,
				compound_exact_mass,
				chromatography_type,
				adduct_type,
				analyzer_type ,
				ionization_type,
				charge_type,
				ms_level,
				collision_energy_prefix,
				collision_energy_unit,
				collision_method,
				collision_gas_type,
				collision_energy,
				mz_decimal_place,
				precursor_mz,
				data_source,
				data_source_id,
				compound_sample_frequency,
				peaks_mz,
				peaks_intensity,
				peaks_annotation
				) VALUES {};""".format(
			','.join(insert_payloads))
		return query

	def add_cfm_spectra(self, cfm_spectra_list: List[CfmSpectra], verbose=False):
		spectrum_list = []
		for cfm_spectra in cfm_spectra_list:
			spectrum_list += cfm_spectra.get_spectra_as_list()
		self.add_spectrum(spectrum_list, verbose)

	def add_spectrum(self, spectrum_list: List[Spectrum], verbose=False):
		query = self._get_add_spectrum_query_batch(spectrum_list)
		if verbose:
			print(query)
		self.execute_query(query, commit=True, fetch=False)

		'''
		query = self._get_add_spectrum_query_batch(spectrum_list)
		ids = self.execute_query(query, commit=True, fetch=True)

		spectrum_ids = [i[0] for i in ids]
		query = self._get_add_peak_query(spectrum_ids, spectrum_list)
		if verbose:
			print(query)
		self.execute_query(query, commit=True, fetch=False)
		'''
		
	'''get spectra from database with the give mw'''

	def _get_spectra_query(self, adduct_type=None,
						   data_source=None,
						   compound_data_source=None,
						   collision_energy=None,
						   collision_method=None,
						   csf_lower=None,
						   csf_upper=None):
		query = ""
		if adduct_type is not None:
			query += " AND s.adduct_type = '{}'".format(adduct_type)

		if data_source is not None:
			query += " AND s.data_source = '{}'".format(data_source)

		if compound_data_source is not None:
			query += " AND s.compound_data_source = '{}'".format(compound_data_source)

		if collision_energy is not None:
			query += " AND s.collision_energy = '{}'".format(collision_energy)

		if collision_method is not None:
			query += " AND s.collision_method = '{}'".format(collision_method)

		if csf_lower is not None:
			query += " AND s.compound_sample_frequency >= {}".format(csf_lower)

		if csf_upper is not None:
			query += " AND s.compound_sample_frequency <= {}".format(csf_upper)

		return query

	def _construct_spectra_from_results(self, spectra_results):
		spectrum_dict = {}
		spectrum_ids = []
		for item in spectra_results:
			spectrum = Spectrum()
			spectrum.inchikey = item[1]
			spectrum.smiles_or_inchi = item[2]
			spectrum.formula = item[3]
			spectrum.compound_name = item[4]
			spectrum.compound_data_source = item[5]
			spectrum.compound_data_source_id = item[6]
			spectrum.compound_exact_mass = float(item[7])
			spectrum.chromatography_type = item[8]
			spectrum.adduct_type = item[9]
			spectrum.analyzer_type = item[10]
			spectrum.ionization_type = item[11]
			spectrum.charge_type = item[12]
			spectrum.ms_level = item[13]
			spectrum.collision_energy_prefix = item[14]
			spectrum.collision_energy_unit = item[15]
			spectrum.collision_method = item[16]
			spectrum.collision_gas_type = item[17]
			spectrum.collision_energy = float(item[18])
			spectrum.mz_decimal_place = int(item[19])
			spectrum.precursor_mz = float(item[20])
			spectrum.data_source = item[21]
			spectrum.data_source_id = item[22]
			spectrum.compound_sample_frequency = int(item[23])
			spectrum.add_peaks(item[24], item[25, item[26]])
			spectrum_dict[item[0]] = spectrum
			spectrum_ids.append(item[0])

		return list(spectrum_dict.values())
		

	

	