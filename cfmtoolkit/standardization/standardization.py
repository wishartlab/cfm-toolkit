'''
Standardization
'''

class Standardization:
	# chorm
	CE = 'CE'
	DI = 'DI'
	GC = 'GC'
	LC = 'LC'
	FI = 'FI'
	IC = 'IC'

	# ionlization
	APCI = 'APCI'
	APPI = 'APPI'
	CI = 'CI'
	EI = 'EI'
	ESI = 'ESI'
	FAB = 'FAB'
	LSIMS = 'LSIMS'
	In_source_CID = 'In-source CID'
	PI = 'PI'

	# adduct

	# ms_level
	MS1 = 'MS1'
	MS2 = 'MS2'

	#
	QqQ = 'QqQ'
	QTOF = 'QTOF'
	BEqQ = 'BEqQ'
	Orbitrap = 'Orbitrap'
	QqLIT = 'QqLIT'
	API = 'API'
	ICR = 'ICR'
	EBEqEBE = 'EBEqEBE'
	EBE = 'EBE'
	BE = 'BE'
	IT = 'IT'
	QFT = 'QFT '  # Q Exactive Plus Orbitrap

	Pos = 'P'
	Neg = 'N'

	HCD = 'hcd'
	CID = 'cid'

	NA = 'n/a'

	# Adduct
	M_H_pos = '[M+H]+'
	M_H_neg = '[M-H]-'
	M_pos = '[M]+'

	EV = 'eV'

	POSITIVE_ADDUCTS = [

	]

	NEGATIVE_ADDUCTS = [
		
	]
	
	#RT Unit
	RT_MIN = 'min'
	RT_SEC = 'sec'