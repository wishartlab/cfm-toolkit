from typing import Dict, List, Tuple
from cfmtoolkit.datastructures import Spectrum, CfmSpectra

from scipy.stats import entropy
import numpy as np
from numpy.linalg import norm
from numpy import dot
import warnings

class Comparators:

	# DICE
	@staticmethod
	def _get_dice(ref_spectrum: Spectrum, query_spectrum: Spectrum, matched_peaks_count: int,
				  verbose: bool = False) -> float:
		dice = 2.0 * matched_peaks_count / \
			   (ref_spectrum.peaks_count + query_spectrum.peaks_count) if ref_spectrum.peaks_count + \
																			  query_spectrum.peaks_count != 0 else 0.0
		if verbose:
			print("Dice: 2.0 * {} / ( {} + {} ) = {}".format(matched_peaks_count,
															 ref_spectrum.peaks_count, query_spectrum.peaks_count,
															 dice))
		return dice

	@staticmethod
	def get_dice(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol: float = 0.01, ppm_tol: float = 10, verbose: bool = False) -> float:
		""" Method compute dice between two spectrum
		Args:
			ref_spectrum (Spectrum): reference spectrum
			query_spectrum (Spectrum): predicted spectrum
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			float: dice
		"""
		matched_peaks_count, _ = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches = False)
		return Comparators._get_dice(ref_spectrum, query_spectrum, matched_peaks_count, verbose)

	@staticmethod
	def get_dices_cfm(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra, verbose: bool = False) -> List[
		float]:
		""" Method compute dices between two cfm spectra
		Args:
			ref_cfm_spectra (Spectrum): reference cfm spectra
			predicted_cfm_spectra (Spectrum): predicted cfm spectra
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			List[float]: dices, one per each ev level
		"""
		dices = []
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			dice = Comparators.get_dice(ref_spectrum, query_spectrum, verbose)
			dices.append(dice)
		return dices

	# DOT PRODUCT
	@staticmethod
	def _get_adjusted_intensity(peak: Tuple[float,float,], intensity_weight, mz_weight, verbose: bool = False) -> float:
		result = pow(peak[1], intensity_weight) * \
				 pow(peak[0], mz_weight)
		if verbose:
			print("Adjusted Intensity: {}^{} * {}^{} = {} ".format(peak[1],
																   intensity_weight, peak[0], mz_weight, result))
		return result

	@staticmethod
	def _get_total_peak_sum(spectrum: Spectrum, intensity_weight: float, mz_weight: float) -> float:
		result = 0.0
		for peak in spectrum.peaks:
			peak_value = Comparators._get_adjusted_intensity(
				peak, intensity_weight, mz_weight)
			result += peak_value * peak_value
		return result

	@staticmethod
	def _compute_weighed_dot_product(matched_peaks: np.ndarray, intensity_weight: float, mz_weight: float, verbose: bool = False) -> float:

		"""
		num = 0.0
		for matches in matched_peaks:
			p1, p2 = matches[0], matches[1]
			num += Comparators._get_adjusted_intensity(p1, intensity_weight, mz_weight, verbose) * \
				   Comparators._get_adjusted_intensity(
					   p2, intensity_weight, mz_weight, verbose)

		denomp = Comparators._get_total_peak_sum(
			ref_spectrum, intensity_weight, mz_weight)
		denomq = Comparators._get_total_peak_sum(
			query_spectrum, intensity_weight, mz_weight)

		"""
		if len(matched_peaks) == 0:
			return 0
		# \sqrt{\frac{(\sum{Q_iP_i})^2}{\sum{Q_i^2\sum P_i^2}}}
		q_mz = matched_peaks[:, 0]
		q_int = matched_peaks[:, 1]
		p_mz = matched_peaks[:, 2]
		p_int = matched_peaks[:, 3]
		warnings.simplefilter("error", category=RuntimeWarning)

		try:
			weighted_q = np.power(q_int,intensity_weight) * np.power(q_mz, mz_weight)
			weighted_p = np.power(p_int,intensity_weight) * np.power(p_mz, mz_weight)
		except RuntimeWarning as e:
			print("Caught RuntimeWarning:", e)
			print("q_mz", q_mz)
			print("q_int", q_mz)
			print("p_mz", p_mz)
			print("p_int", p_int)

		score =  0.0
		dim = np.sum(np.power(weighted_q, 2)) * np.sum(np.power(weighted_p, 2))
		if dim != 0:
			score = np.power(np.sum(weighted_q * weighted_p), 2) / dim
		return float(score)

	@staticmethod
	def _compute_cos_sim(matched_peaks: np.ndarray,  verbose: bool = False) -> float:

		"""

		"""
		if len(matched_peaks) == 0:
			return 0
		
		q_int = matched_peaks[:, 1]
		p_int = matched_peaks[:, 3]
		
		dim = (norm(q_int)*norm(p_int))
		cos_sim = float(dot(q_int, p_int)/ dim) if dim != 0 else 0.0
		return cos_sim
	
	@staticmethod
	def _compute_cfm_weighted_dot_product(matched_peaks,
						 verbose: bool = False) -> float:
		"""cfm balanced weighted dot product
			\frac{(\sum{Q^{'}_{i} P^{'}_{i}})^2}{\sum{Q_{i}^{'2}\sum P_{i}^{'2}}}
			P^{'}_{i} = M_{p,i}^{0.5}I_{p,i}^{0.5}, Q^{'}_{i} = M_{q,i}^{0.5}I_{q,i}^{0.5}
		Args:
			matched_peaks (_type_): matched_peaks, need missing peaks set to True for current (np) implementation,
			 as we don't have a separate peak sum function
			verbose (bool, optional): _description_. Defaults to False.
			
		Returns:
			float: _description_
		"""
		score = Comparators._compute_weighed_dot_product(matched_peaks, 0.5, 0.5,verbose)
		return score
	
	@staticmethod
	def get_dot_product(ref_spectrum: Spectrum, query_spectrum: Spectrum, 
						abs_tol: float = 0.01, ppm_tol: float = 10,
						verbose: bool = False) -> float:
		""" Method compute cfm weighted (0.5,0.5) dot product between two spectrum
		Args:
			ref_spectrum (Spectrum): reference spectrum
			query_spectrum (Spectrum): predicted spectrum
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			float: dot product 
		"""
		# we don't need include_miss_matches because 0 int will create 0 term any way
		_, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches = True)
		 
		dt = Comparators._compute_cfm_weighted_dot_product(matched_peaks, verbose)
		return dt

	@staticmethod
	def get_cos_sim(ref_spectrum: Spectrum, query_spectrum: Spectrum, 
						abs_tol: float = 0.01, ppm_tol: float = 10,
						verbose: bool = False) -> float:
		""" Method compute dot product between two spectrum
		Args:
			ref_spectrum (Spectrum): reference spectrum
			query_spectrum (Spectrum): predicted spectrum
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			float: dot product 
		"""
		# we don't need include_miss_matches because 0 int will create 0 term any way
		_, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches = True)
		dt = Comparators._compute_cos_sim(matched_peaks, verbose)
		return dt
	
	@staticmethod
	def get_dot_products_cfm(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra, verbose: bool = False) -> \
			List[float]:
		""" Method compute dot_products between two cfm spectra
		Args:
			ref_cfm_spectra (Spectrum): reference cfm spectra
			predicted_cfm_spectra (Spectrum): predicted cfm spectra
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			List[float]: dot_products, one per each ev level
		"""
		dot_products = []
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			dot_product = Comparators.get_dot_product(ref_spectrum, query_spectrum, verbose)
			dot_products.append(dot_product)
		return dot_products

	@staticmethod
	def _get_stein_dot_product(ref_spectrum: Spectrum, query_spectrum: Spectrum,
							   matched_peaks,
							   verbose: bool = False) -> float:
		return Comparators._compute_weighed_dot_product(matched_peaks, 0.6, 3.0, verbose)

	@staticmethod
	def get_stein_dot_product(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol: float = 0.01, ppm_tol: float = 10, 
							  verbose: bool = False) -> float:
		# we don't need include_miss_matches because 0 int will create 0 term any way
		_, matched_peaks = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, abs_tol, ppm_tol, abs_tol, ppm_tol, include_miss_matches = True)
		return Comparators._get_stein_dot_product(ref_spectrum, query_spectrum, matched_peaks, verbose)

	@staticmethod
	def _get_recall(ref_peak_count: int, matched_peaks_count: int, verbose: bool = False) -> float:
		# tp + fp : total number of predicted peak
		# tp : macthed_peaks

		recall = matched_peaks_count / ref_peak_count if ref_peak_count != 0 else 0.0

		if verbose:
			print("Recall : {} / {} = {}".format(ref_peak_count,
												 matched_peaks_count, recall))

		return recall

	@staticmethod
	def get_recall(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol: float = 0.01, ppm_tol: float = 10, 
				   verbose: bool = False) -> float:
		""" Method compute recall between two spectrum
		Args:
			ref_spectrum (Spectrum): reference spectrum
			query_spectrum (Spectrum): predicted spectrum
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			float: recall
		"""
		# we don't need include_miss_matches because 0 int will create 0 term any way
		matched_peaks_count, _ = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches = False)
		ref_peak_count = ref_spectrum.peaks_count
		return Comparators._get_recall(ref_peak_count, matched_peaks_count, verbose)

	@staticmethod
	def get_recalls_cfm(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra, abs_tol: float = 0.01, ppm_tol: float = 10,
						 verbose: bool = False) -> List[
		float]:
		""" Method compute recalls between two cfm spectra
		Args:
			ref_cfm_spectra (Spectrum): reference cfm spectra
			predicted_cfm_spectra (Spectrum): predicted cfm spectra
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			List[float]: recalls, one per each ev level
		"""
		recalls = []
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			recall = Comparators.get_recall(ref_spectrum, query_spectrum, abs_tol, ppm_tol, verbose)
			recalls.append(recall)
		return recalls

	@staticmethod
	def _get_precision(pred_peak_count: int, matched_peaks_count: int, verbose: bool = False) -> float:
		# tp + fp : total number of predicted peak
		# tp : macthed_peaks
		precision = matched_peaks_count / pred_peak_count if pred_peak_count != 0 else 0.0

		if verbose:
			print("Precision: {} / {} = {}".format(pred_peak_count,
												   matched_peaks_count, precision))

		return precision

	@staticmethod
	def get_precision(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol: float = 0.01, ppm_tol: float = 10, verbose: bool = False) -> float:
		""" Method compute precision  between two spectrum
		Args:
			ref_spectrum (Spectrum): reference spectrum
			query_spectrum (Spectrum): predicted spectrum
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			float: precision
		"""
		matched_peaks_count, _ = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches = False)
		pred_peak_count = query_spectrum.peaks_count
		return Comparators._get_precision(pred_peak_count, matched_peaks_count, verbose)

	@staticmethod
	def get_precisions_cfm(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra, abs_tol: float = 0.01, ppm_tol: float = 10, verbose: bool = False) -> \
			List[float]:
		""" Method compute precisions between two cfm spectra
		Args:
			ref_cfm_spectra (Spectrum): reference cfm spectra
			predicted_cfm_spectra (Spectrum): predicted cfm spectra
			verbose (bool, optional): flag for verbose. Defaults to False.

		Returns:
			List[float]: precisions, one per each ev level
		"""
		precisions = []
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			precision = Comparators.get_precision(ref_spectrum, query_spectrum, abs_tol, ppm_tol, verbose)
			precisions.append(precision)
		return precisions

	@staticmethod
	def get_metrics(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol: float = 0.01, ppm_tol: float = 10, 
					verbose: bool = False) -> Dict[str, float]:

		# we don't need include_miss_matches because 0 int will create 0 term any way

		matched_peaks_count, matched_peaks = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, abs_tol, ppm_tol, include_miss_matches=True)
		pred_peak_count = query_spectrum.peaks_count
		ref_peak_count = ref_spectrum.peaks_count

		precision = Comparators._get_precision(
			pred_peak_count, matched_peaks_count, verbose)
		recall = Comparators._get_recall(
			ref_peak_count, matched_peaks_count, verbose)
		dice = Comparators._get_dice(
			ref_spectrum, query_spectrum, matched_peaks_count, verbose)
		dot_product = Comparators._compute_cfm_weighted_dot_product(matched_peaks, verbose)
		metrics = {'Dice': dice, 'Dot Product': dot_product, 'Precision': precision, 'Recall': recall}

		return metrics

	@staticmethod
	def get_cfm_stein_dot_product(ref_cfm_spectrum: CfmSpectra, predicted_cfm_spectrum: CfmSpectra, weights:Dict =None,
								  abs_tol=0.01, ppm_tol=10, verbose: bool = False) -> float:

		if weights is None:
			weights = {}
			for key in CfmSpectra.get_spectra_keys():
				weights[key] = 1.0
		total_weights = 0.0
		for key in weights:
			total_weights += weights[key]

		weighted_dot_product = 0.0
		for spectrum_ev in ref_cfm_spectrum.get_spectra():
			if weights[spectrum_ev] > 0.0:
				ref_spectrum = ref_cfm_spectrum.get_spectra()[spectrum_ev]
				query_spectrum = predicted_cfm_spectrum.get_spectra()[spectrum_ev]

				# special case when one of the spectrum is empty
				if ref_spectrum.peaks_count == 0 or query_spectrum.peaks_count == 0:
					total_weights -= weights[spectrum_ev]
					continue
				
				_, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol=abs_tol,
															ppm_tol=ppm_tol, include_miss_matches = True)

				dot_product_value = Comparators._get_stein_dot_product(ref_spectrum, query_spectrum, matched_peaks,
																	verbose)
				weighted_dot_product += dot_product_value * weights[spectrum_ev]

		weighted_dot_product /= total_weights
		return weighted_dot_product

	@staticmethod
	def get_cfm_dot_product(ref_cfm_spectrum: CfmSpectra, predicted_cfm_spectrum: CfmSpectra, weights=None,
							abs_tol=0.01, ppm_tol=10, verbose: bool = False) -> float:

		if weights is None:
			weights = {}
			for key in CfmSpectra.get_spectra_keys():
				weights[key] = 1.0
		total_weights = 0.0
		for key in weights:
			total_weights += weights[key]

		weighted_dot_product = 0.0
		for spectrum_ev in ref_cfm_spectrum.get_spectra():
			if weights[spectrum_ev] >  0.0:
				ref_spectrum = ref_cfm_spectrum.get_spectra()[spectrum_ev]
				query_spectrum = predicted_cfm_spectrum.get_spectra()[spectrum_ev]
				
				# special case when one of the spectrum is empty
				if ref_spectrum.peaks_count == 0 or query_spectrum.peaks_count == 0:
					total_weights -= weights[spectrum_ev]
					continue
				
				_, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol=abs_tol,
															ppm_tol=ppm_tol, include_miss_matches = True)

				dot_product_value = Comparators._compute_cfm_weighted_dot_product(matched_peaks, verbose)
				weighted_dot_product += dot_product_value * weights[spectrum_ev]

		weighted_dot_product /= total_weights
		return weighted_dot_product

	@staticmethod
	def get_cfm_dice(ref_cfm_spectrum: CfmSpectra, predicted_cfm_spectrum: CfmSpectra, weights=None, abs_tol=0.01,
					 ppm_tol=10, verbose: bool = False) -> float:

		if weights is None:
			weights = {}
			for key in CfmSpectra.get_spectra_keys():
				weights[key] = 1.0
		total_weights = 0.0
		for key in weights:
			total_weights += weights[key]

		weighted_dice_value = 0.0
		for spectrum_ev in ref_cfm_spectrum.get_spectra():
			if weights[spectrum_ev]  > 0.0:
				ref_spectrum = ref_cfm_spectrum.get_spectra()[spectrum_ev]
				query_spectrum = predicted_cfm_spectrum.get_spectra()[spectrum_ev]

				# special case when one of the spectrum is empty
				if ref_spectrum.peaks_count == 0 or query_spectrum.peaks_count == 0:
					total_weights -= weights[spectrum_ev]
					continue
				
				matched_peaks_count, _ = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol=abs_tol,
																	ppm_tol=ppm_tol, include_miss_matches = False)

				dice_value = Comparators._get_dice(ref_spectrum, query_spectrum, matched_peaks_count, verbose)
				weighted_dice_value += dice_value * weights[spectrum_ev]

		weighted_dice_value /= total_weights
		return weighted_dice_value

	@staticmethod
	def get_cfm_dot_product_and_dice(ref_cfm_spectrum: CfmSpectra, predicted_cfm_spectrum: CfmSpectra, weights=None,
									 abs_tol=0.01, ppm_tol=10, verbose: bool = False) -> float:

		if weights is None:
			weights = {}
			for key in CfmSpectra.get_spectra_keys():
				weights[key] = 1.0

		total_weights = 0.0
		for key in weights:
			total_weights += weights[key]

		weighted_metrics = 0.0
		for spectrum_ev in ref_cfm_spectrum.get_spectra():
			if weights[spectrum_ev] > 0.0:
				ref_spectrum = ref_cfm_spectrum.get_spectra()[spectrum_ev]
				query_spectrum = predicted_cfm_spectrum.get_spectra()[spectrum_ev]

				# special case when one of the spectrum is empty
				if ref_spectrum.peaks_count == 0 or query_spectrum.peaks_count == 0:
					total_weights -= weights[spectrum_ev]
					continue
				
				matched_peaks_count, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum,
																				abs_tol=abs_tol, ppm_tol=ppm_tol)

				dot_product_value = Comparators._compute_cfm_weighted_dot_product(matched_peaks, verbose)
				dice_value = Comparators._get_dice(ref_spectrum, query_spectrum, matched_peaks_count, verbose)
				#Normalize to 0-1
				weighted_metrics += (dot_product_value + dice_value)/2 * weights[spectrum_ev]

		weighted_metrics /= total_weights
		return weighted_metrics

	@staticmethod
	def get_num_matched(ref_spectrum: Spectrum, query_spectrum: Spectrum) -> Tuple[int, int, int]:
		num_matched, _ = Spectrum.get_matched_peaks(
			ref_spectrum, query_spectrum, include_miss_matches = False)
		return num_matched, ref_spectrum.get_num_peaks() - num_matched, query_spectrum.get_num_peaks() - num_matched

	@staticmethod
	def get_num_matched_percent(ref_spectrum: Spectrum, query_spectrum: Spectrum) -> Tuple[float,float]:
		num_matched, _ = Spectrum.get_num_matched(ref_spectrum, query_spectrum)

		return num_matched/ref_spectrum.get_num_peaks(), num_matched/query_spectrum.get_num_peaks()

	@staticmethod
	def get_num_matched_percent_cfm(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra) -> List[
		Tuple[int, int, int]]:
		rev = []
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			data = Comparators.get_num_matched_percent(ref_spectrum, query_spectrum)
			rev.append(data)
		return rev

	@staticmethod
	def get_cfm_num_matched_percent(ref_cfm_spectra: CfmSpectra, predicted_cfm_spectra: CfmSpectra, weights=None,
									 abs_tol=0.01, ppm_tol=10, verbose: bool = False) -> int:
		num_matched_percent = 0
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
													predicted_cfm_spectra.get_spectra_as_list()):
			num_matched_percent += Comparators.get_num_matched_percent(ref_spectrum, query_spectrum)

		return num_matched_percent


	@staticmethod
	def get_cfm_entropy_similarity(ref_cfm_spectra: Spectrum, predicted_cfm_spectra: Spectrum, abs_tol=0.01, ppm_tol=10, weights = None):
		"""
		NOTE Weight is not used
		"""
		entropy_similarity = 0.0
		for ref_spectrum, query_spectrum in zip(ref_cfm_spectra.get_spectra_as_list(),
											predicted_cfm_spectra.get_spectra_as_list()):

			entropy_similarity += Comparators.get_entropy_similarity(ref_spectrum, query_spectrum, abs_tol=abs_tol, ppm_tol=ppm_tol)
			#print("entropy_similarity", entropy_similarity)
		return entropy_similarity / 3

	@staticmethod
	def get_entropy_similarity(ref_spectrum: Spectrum, query_spectrum: Spectrum, abs_tol=0.01, ppm_tol=10):
	
		_, matched_peaks = Spectrum.get_matched_peaks(ref_spectrum, query_spectrum, abs_tol = abs_tol, ppm_tol = ppm_tol, include_miss_matches=True)
		ref_matched_peak = [p[0][1]/100 for p in matched_peaks]
		predicted_matched_peak = [p[1][1]/ 100 for p in matched_peaks]

		return Comparators._entropy_similarity(ref_matched_peak, predicted_matched_peak)

	@staticmethod
	def _entropy_similarity(a, b):
		entropy_a, a = Comparators._get_entropy_and_weighted_intensity(a)
		entropy_b, b = Comparators._get_entropy_and_weighted_intensity(b)

		entropy_merged = entropy(a + b)
		return 1 - (2 * entropy_merged - entropy_a - entropy_b) / np.log(4)

	@staticmethod
	def _get_entropy_and_weighted_intensity(intensity):
		spectral_entropy = entropy(intensity)
		if spectral_entropy < 3:
			weight = 0.25 + 0.25 * spectral_entropy
			weighted_intensity = np.power(intensity, weight)
			intensity_sum = np.sum(weighted_intensity)
			weighted_intensity /= intensity_sum
			spectral_entropy = entropy(weighted_intensity)
			return spectral_entropy, weighted_intensity
		else:
			return spectral_entropy, intensity