from collections import OrderedDict
from typing import List

import numpy as np
import scipy.stats
import pandas as pd

from cfmtoolkit.datastructures import Spectrum, CfmSpectra
from .comparators import Comparators

class Ranking:

	@classmethod
	def get_similarity_score(cls, normalized_cfmid_score: float, reference_count: int, normalized_ancestor_score: float) -> float:

		ESI_SIMILARITY_WEIGHT = 0.60
		ESI_REFERENCE_WEIGHT = 0.30
		ESI_ANCESTOR_WEIGHT = 0.10
		REFERENCE_CAP = 156 

		SIMILARITY_THRESHOLD = 0.95
		if normalized_cfmid_score >= SIMILARITY_THRESHOLD:
			combined_score =  normalized_cfmid_score
		else:
			combined_score = normalized_cfmid_score
			normalized_reference_count = reference_count / REFERENCE_CAP
			combined_score = combined_score * ESI_SIMILARITY_WEIGHT + normalized_reference_count * ESI_REFERENCE_WEIGHT + normalized_ancestor_score * ESI_ANCESTOR_WEIGHT
		return combined_score

	@classmethod
	def init_priors(cls, candidate_spectrum_list, use_freq_prior):

		# crash if any of the sample frequency is 0
		for spectrum in candidate_spectrum_list:
			if np.isnan(spectrum.compound_sample_frequency) or spectrum.compound_sample_frequency <= 0:
				raise ValueError(f'Error: {spectrum.inchikey} Sample frequency {spectrum.compound_sample_frequency} is invalid. Need to be > 0.0')
			
		if use_freq_prior == 'Linear':
			total = sum(
				[spectrum.compound_sample_frequency for spectrum in candidate_spectrum_list])
			priors = [spectrum.compound_sample_frequency /
						total for spectrum in candidate_spectrum_list]

		elif use_freq_prior == 'LogLinear':
			total = sum(
				[float(np.log(spectrum.compound_sample_frequency)) + 1 for spectrum in candidate_spectrum_list])
			
			priors = [(float(np.log(spectrum.compound_sample_frequency)) + 1) /
						total for spectrum in candidate_spectrum_list]
		else:
			raise ValueError('Unknown freq prior type {}'.format(use_freq_prior))

		return priors

	@classmethod
	def rank_spectrum(cls, input_spectrum: Spectrum, candidate_spectrum_list: List[Spectrum], ranking_function='DotProduct',
					  abs_tol: float = 0.01, ppm_tol: float = 10,
					  use_freq_prior=None):

		results = []
		if use_freq_prior in ['Linear', 'LogLinear']:
			priors = cls.init_priors(candidate_spectrum_list, use_freq_prior)
		else:
			priors = [0] * len(candidate_spectrum_list)

		for candidate_spectrum, prior in zip(candidate_spectrum_list, priors):
			if ranking_function == 'Dice':
				similarity_score = Comparators.get_dice(
					input_spectrum, candidate_spectrum, abs_tol, ppm_tol)
			elif ranking_function == 'DotProduct':
				similarity_score = Comparators.get_dot_product(
					input_spectrum, candidate_spectrum, abs_tol, ppm_tol)
			elif ranking_function == 'SteinDotProduct':
				similarity_score = Comparators.get_stein_dot_product(
					input_spectrum, candidate_spectrum, abs_tol, ppm_tol)
			elif ranking_function == 'DotProduct+Dice':
				similarity_score = Comparators.get_dot_product(input_spectrum, candidate_spectrum, abs_tol, ppm_tol) +\
					Comparators.get_dice(input_spectrum, candidate_spectrum, abs_tol, ppm_tol)
				similarity_score *= 0.5
			elif ranking_function == 'NumMatched':
				similarity_score,_,_ = Comparators.num_matched(input_spectrum, candidate_spectrum, abs_tol, ppm_tol)
			else:
				print(f'error, unknown ranking function {ranking_function}')
				similarity_score = None
				
			spectra_similarity_score = similarity_score
			if use_freq_prior in ['Linear', 'LogLinear'] and similarity_score is not None:
				similarity_score *= prior

			results.append([similarity_score, candidate_spectrum, spectra_similarity_score, prior])
		results = sorted(results, key=lambda x: x[0], reverse=True)
		return results

	@classmethod
	def rank_spectrum_fast(cls, input_spectrum: Spectrum, candidate_spectrum_list: List[Spectrum], 
						   ranking_functions=['DotProduct', 'Dice', 'DotProduct+Dice'],
					  abs_tol: float = 0.01, ppm_tol: float = 10,
					  use_freq_prior=['Linear', 'LogLinear']):
			
		result_data = []
		for candidate_spectrum in candidate_spectrum_list:

			matched_peaks_count, matched_peaks = Spectrum.get_matched_peaks(input_spectrum, candidate_spectrum, abs_tol = abs_tol, ppm_tol = ppm_tol, include_miss_matches=True)
			data_row = [input_spectrum.data_source_id, candidate_spectrum, candidate_spectrum.inchikey, candidate_spectrum.smiles_or_inchi, candidate_spectrum.compound_data_source, candidate_spectrum.compound_exact_mass, input_spectrum.compound_exact_mass]
			for ranking_function in ranking_functions:
				if ranking_function == 'Dice':
					similarity_score = Comparators._get_dice( input_spectrum, candidate_spectrum, matched_peaks_count)
					data_row.append(similarity_score)
				elif ranking_function == 'DotProduct':
					similarity_score =  Comparators._compute_cfm_weighted_dot_product(matched_peaks)
					data_row.append(similarity_score)
				elif ranking_function == 'DotProduct+Dice':
					similarity_score = Comparators._get_dice( input_spectrum, candidate_spectrum, matched_peaks_count) +\
						Comparators._compute_cfm_weighted_dot_product(matched_peaks)
					similarity_score *= 0.5
					data_row.append(similarity_score)
				elif ranking_function == 'NumMatched':
					data_row.append(matched_peaks_count)
				else:
					print('error, unknown ranking function {}'.format(ranking_function))
					similarity_score = None
			result_data.append(data_row)
		
		result_data_df = pd.DataFrame(result_data, columns=['specturm_id', 'candidate_spectrum','inchikey','smiles_or_inchi','compound_data_source','candidate_exact_mass', 'exact_mass'] + ranking_functions)

		for prior_type in use_freq_prior:
			priors = cls.init_priors(candidate_spectrum_list, prior_type)
			#print(priors)
			result_data_df['prior_{}'.format(prior_type)] = priors
			for ranking_function in ranking_functions:
				if ranking_function == 'NumMatched':
					continue
				result_data_df[f"{ranking_function}_{prior_type}"] = result_data_df[ranking_function] * result_data_df['prior_{}'.format(prior_type)]
		return result_data_df
	
	@classmethod
	def rank_cfm_spectra(cls, input_cfm_spectra: CfmSpectra, candidate_cfm_spectra_list: List[CfmSpectra],
						 ranking_function='DotProduct', 
						abs_tol: float = 0.01, ppm_tol: float = 10,
						 use_freq_prior=None, weights=None):
		"""[summary]

		Args:
			input_cfm_spectra (CfmSpectra): [description] 
			candidate_cfm_spectra_list (List[CfmSpectra]): [description]
			ranking_function (str, optional): One of  'Dice', ''DotProduct', 'DotProduct+Dice', 'SteinDotProduct'. Defaults to 'DotProduct'.
			use_freq_prior ([type], optional): One of 'Linear', 'LogLinear'. Defaults to None.
			weights ([type], optional): [description]. Defaults to None.

		Returns:
			List[List[]]: [[similarity_score, candidate_spectrum, spectra_similarity_score, prior]]
		"""

		if use_freq_prior in ['Linear', 'LogLinear']:
			priors = cls.init_priors(candidate_cfm_spectra_list, use_freq_prior)
		else:
			priors = [1.0] * len(candidate_cfm_spectra_list)

		results = []
		for candidate_spectrum, prior in zip(candidate_cfm_spectra_list, priors):
			if ranking_function == 'Dice':
				similarity_score = Comparators.get_cfm_dice(
					input_cfm_spectra, candidate_spectrum, weights, abs_tol, ppm_tol)
			elif ranking_function == 'DotProduct':
				similarity_score = Comparators.get_cfm_dot_product(
					input_cfm_spectra, candidate_spectrum, weights, abs_tol, ppm_tol)
			elif ranking_function == 'DotProduct+Dice':
				similarity_score = Comparators.get_cfm_dot_product_and_dice(
					input_cfm_spectra, candidate_spectrum, weights, abs_tol, ppm_tol)
			elif ranking_function == 'SteinDotProduct':
				similarity_score = Comparators.get_cfm_stein_dot_product(
					input_cfm_spectra, candidate_spectrum, weights)
			elif ranking_function == 'NumMatched':
				similarity_score = Comparators.get_cfm_num_matched_percent(input_cfm_spectra, candidate_spectrum, abs_tol, ppm_tol)
			elif ranking_function == 'EntropySimilarity':
				similarity_score = Comparators.get_cfm_entropy_similarity(input_cfm_spectra, candidate_spectrum, abs_tol, ppm_tol)
			else:
				print('error, unknown ranking function {}'.format(ranking_function))
				similarity_score = None

			spectra_similarity_score = similarity_score
			if use_freq_prior in ['Linear', 'LogLinear'] and similarity_score is not None:
				similarity_score *= prior

			results.append([similarity_score, candidate_spectrum, spectra_similarity_score, prior])
			
		results = sorted(results, key=lambda x: x[0], reverse=True)
		return results

	@staticmethod
	def compute_cost_score(target_id, data, total_candidates=None):
		score = 0.0
		rank_counts = OrderedDict()

		target_rank_value = None
		for row_data in data:
			rank_value = row_data[0]
			predicted_id = row_data[1]
			if rank_value not in rank_counts:
				rank_counts[rank_value] = 0
			rank_counts[rank_value] += 1

			if target_id == predicted_id:
				target_rank_value = rank_value

		# print(rank_counts)
		if target_rank_value is not None:
			for key in rank_counts:
				if key > target_rank_value:
					score += rank_counts[key]
				elif key == target_rank_value:
					score += 0.5 * (rank_counts[key] + 1)
				else:
					break
		else:
			if total_candidates is None:
				total_candidates = len(data)
			score = total_candidates + 1

		return score

	@staticmethod
	def compute_strict_top_n_score(target_id, data, total_candidates=None):
		score = 0.0
		rank_counts = OrderedDict()

		target_rank_value = None
		for row_data in data:
			rank_value = row_data[0]
			predicted_id = row_data[1]
			if rank_value not in rank_counts:
				rank_counts[rank_value] = 0
			rank_counts[rank_value] += 1

			if target_id == predicted_id:
				target_rank_value = rank_value

		# print(rank_counts)
		if target_rank_value is not None:
			for key in rank_counts:
				if key >= target_rank_value:
					score += rank_counts[key]
				else:
					break
		elif total_candidates is None:
			score = len(data)

			
		else:
			score = total_candidates

		return score
	
	@staticmethod
	def compute_ranking(target_id, data):
		rank = 0
		for row_data in data:
			predicted_id = row_data[1]
			# print(predicted_id)
			rank += 1
			if predicted_id == target_id:
				return rank
		return -1

	@staticmethod
	def mean_confidence_interval(data, confidence=0.95):
		a = np.array(data)
		n = len(a)
		m, se = np.mean(a), scipy.stats.sem(a)
		h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
		return m, h
