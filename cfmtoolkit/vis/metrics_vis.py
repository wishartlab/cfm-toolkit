from typing import Dict, List

import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from .vis_utils import *


class MetricsVis(object):
	_default_figure_height = 10
	_default_figure_width = 16
	_default_summary_font_size = 12
	_default_font_size = 14
	_default_xtick_font_size = 16
	_default_ax_tile_font_size = 20
	_default_font_family = 'sans-serif'
	_default_fonts = ['Arial', 'DejaVu Sans']
	_title_fonts_size = 36
	_sub_title_fonts_size = 24
	_bar_width = 10
	_bar_font_size = 18
	_bar_err_font_size = 8
	_bar_margin = 4
	_bar_cluster_margin = 20
	_bar_cap_size = 4
	_bar_cap_thick = 1
	_bar_value_text_alpha = 1.0
	_default_bar_alpha = 1.0
	_colors =  ['#0077BB','#33BBEE','#009988','#EE7733','#CC3311','#EE3377','#BBBBBB']
	#["#E69F00", "#1982C4", "#FF595E", "#8AC926",
			  # "#6A4C93", "#0EAD69", "#8C8B88", "#F2BB9B"]
	_baseline_colors = ["#56B4E970", '#009E7370']
	#_cfm_colors = []

	@classmethod
	def _create_subplot(cls, ax, chart_data, colors: List, ax_title: str, show_err: bool, show_bar_value: bool,
						adjust_ylim: bool = True):
		fig = plt.gcf()
		bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
		_, ax_height = bbox.width * fig.dpi, bbox.height * fig.dpi

		width = cls._bar_width
		margin = cls._bar_margin

		# Get x_labels
		x_labels = list(chart_data.keys())

		# set y axis

		ax.yaxis.grid(False)
		y_max = 0.0
		# set tile
		ax.set_title(ax_title, fontsize=cls._default_ax_tile_font_size)

		xlabelticks = []

		# set x axis, this is important to do here
		# because we try to fit text with bar width
		x_max = 0
		for _, metric in enumerate(chart_data):
			# print(metric, (width + margin) * len(chart_data[metric]) - margin)
			x_max += cls._bar_cluster_margin + \
					 (width + margin) * len(chart_data[metric]) - margin

		ax.set_xlim(0, x_max)

		# set x_pos_base
		curr_x_pos_base = -cls._bar_cluster_margin / 2 + width / 2
		for _, metric in enumerate(chart_data):
			# compute x positions
			x_pos = [curr_x_pos_base + cls._bar_cluster_margin + x *
					 (width + margin) for x in range(len(chart_data[metric]))]
			curr_x_pos_base = x_pos[-1] + width
			# compute y values
			y_values = [chart_data[metric][data_point][0]
						for data_point in chart_data[metric]]

			y_max = max(y_max, max(y_values))
 
			# draw bar
			bars = ax.bar(x_pos,
						  y_values,
						  alpha = cls._default_bar_alpha,
						  width=cls._bar_width,
						  color=colors,
						  align='center')

			# each pt is 1/72 of inchi		  
			bar_width = bars[0].get_window_extent(find_renderer(fig)).width
			bar_width_points = bar_width / fig.dpi * 72

			# create an empty y_errs
			y_errs = [None for _ in y_values]
			# show error bar
			if show_err:
				y_errs = [chart_data[metric][data_point][1]
						  for data_point in chart_data[metric]]
				for y_value, y_err in zip(y_values, y_errs):
					y_max = max(y_max, y_value + y_err + 0.01)
					# if not adjust_ylim:
					#	y_bar_text = max(y_bar_text, y_value + y_err)
					# else:

				for pos, y_pos, y_err in zip(x_pos, y_values, y_errs):
					ax.errorbar(pos, y_pos, y_err,
								capsize=bar_width_points * 0.45,
								capthick=cls._bar_cap_thick,
								ecolor='black')

			# print(fig.dpi)

			# add text for each bar
			if show_bar_value:
				# each pt is 1/72 of inchi
				# 0.8 is the scalar
				scalar = 0.98
				y_bar_text = 0.01

				bar_text_font = bar_width_points * scalar

				for x, y_value, y_err in zip(x_pos, y_values, y_errs):
					# print(x, y_value, y_err)

					if y_value == 0.0:
						continue

					x_offset = 1
					text_str = "{:.2f}".format(y_value)
					# print(text_str, x + x_offset, y_bar_text)
					text = ax.text(x + x_offset, y_bar_text,
								   text_str,
								   horizontalalignment='center',
								   verticalalignment='bottom',
								   rotation='vertical',
								   color='white',
								   weight='normal',
								   stretch='ultra-condensed',
								   fontsize=bar_text_font,
								   alpha=cls._bar_value_text_alpha
								   )

					text_bbox = text.get_window_extent(find_renderer(fig))
					y_max = max(text_bbox.height / ax_height +
								y_bar_text + 0.1, y_max)

					# print(text_bbox.height)
					line_y_pos = y_value + 0.01
					if y_err is not None and y_err > 0:
						line_y_pos += y_err

					if not adjust_ylim:
						line = mlines.Line2D([x, x], [
							line_y_pos, y_bar_text - 0.01], color='black', alpha=cls._bar_value_text_alpha,
											 dashes=(1, 1, 1, 1))
						ax.add_line(line)

			xlabelticks.append(x_pos[0] + (x_pos[-1] - x_pos[0]) / 2)

		# print('ymax', y_max)
		if not adjust_ylim:
			ax.set_ylim(0, 1.0)
		else:
			if y_max == 0:
				y_max = 1.0
			ax.set_ylim(0, y_max)

		ax.get_xaxis().set_ticks(xlabelticks)
		ax.set_xticklabels([x.replace(" ", '\n') for x in x_labels])
		for tick in ax.xaxis.get_major_ticks():
			tick.label.set_fontsize(cls._default_xtick_font_size)

		y_ticks = ax.get_yticks()
		ax.set_yticks(y_ticks[(0 <= y_ticks) & (1 >= y_ticks)])

	@classmethod
	def _extract_legend_strs_and_colors(cls, chart_data):
		# get legende_strs
		legend_strs = []
		subchart_key = next(iter(chart_data))
		metric_key = next(iter(chart_data[subchart_key]))
		for data_point_name in chart_data[subchart_key][metric_key]:
			legend_strs.append(data_point_name)

		# print(legend_strs)
		# create colors
		colors = []
		baseline_idx = 0
		new_method_idx = 0
		for legend in legend_strs:
			if legend.startswith('Baseline'):
				colors.append(cls._baseline_colors[baseline_idx])
				baseline_idx += 1
			else:
				colors.append(cls._colors[new_method_idx])
				new_method_idx += 1

		# use blue if there is one bar
		# if new_method_idx == 1 and baseline_idx == 0:
		#	colors[0] == cls._colors[1]

		return legend_strs, colors

	@classmethod
	def _create_cfm_charts(cls, chart_data: Dict, fig_title: str = '<Fig Title>', sub_title='<Sub Title>',
						   show_err_bar: bool = True, error_bar_str: str = 'Err Bar : 95% CI', show_bar_value=False,
						   num_row=3,
						   num_col=2, figure_height=13.5, figure_width=24, has_overall=False, colors=None):

		colors, fig, gs, legend_strs = cls._config_fig(chart_data, colors, figure_height, figure_width, num_col,
													   num_row)

		# create subplot
		if has_overall:
			for idx, chart_type in enumerate(chart_data):
				if idx == 0:
					ax = fig.add_subplot(gs[0:, 0])
				else:
					ax = fig.add_subplot(gs[idx - 1, 1])
				cls._create_subplot(
					ax, chart_data[chart_type], colors, chart_type, show_err_bar, show_bar_value)
		else:
			for idx, chart_type in enumerate(chart_data):
				ax = fig.add_subplot(gs[idx // num_col, idx % num_col])
				cls._create_subplot(
					ax, chart_data[chart_type], colors, chart_type, show_err_bar, show_bar_value)

		# apply layout
		fig.tight_layout(w_pad=1.0, h_pad=2.0)

		# add title
		next_available_y = 1.0 - 0.03
		left_margin = 0.03
		y_margin = 0.01
		legend_left_margin = 0.025

		fig_height = (fig.get_size_inches() * fig.dpi)[1]
		next_available_y = cls._add_titles(fig, fig_height, fig_title, left_margin, next_available_y, sub_title,
										   y_margin)
		# add legend
		# print(next_available_y)
		# next_available_y = min(0.9, next_available_y)
		next_available_y = cls._add_legends(colors, error_bar_str, fig, fig_height, legend_left_margin, legend_strs,
											next_available_y, show_err_bar, y_margin)

		plt.subplots_adjust(top=next_available_y - y_margin)
		return fig

	@classmethod
	def _config_fig(cls, chart_data, colors, figure_height, figure_width, num_col, num_row):
		plt.rcParams['font.size'] = cls._default_font_size
		plt.rcParams['font.family'] = cls._default_font_family
		plt.rcParams['font.' + cls._default_font_family] = cls._default_fonts
		fig = plt.figure(figsize=(figure_width, figure_height), dpi=256,
						 facecolor='w', edgecolor='k', constrained_layout=False)
		gs = GridSpec(num_row, num_col, figure=fig)
		legend_strs, default_colors = cls._extract_legend_strs_and_colors(
			chart_data)
		if colors is None:
			colors = default_colors
		return colors, fig, gs, legend_strs

	@classmethod
	def _add_legends(cls, colors, error_bar_str, fig, fig_height, legend_left_margin, legend_strs, next_available_y,
					 show_err_bar, y_margin):
		# add legend
		patches = [mpatches.Patch(color=colors[i], label="{:s}".format(
			legend_strs[i]), alpha = cls._default_bar_alpha) for i in range(len(legend_strs))]

		if error_bar_str is not None and show_err_bar:
			patches.append(mpatches.Patch(
				label="{:s}".format(error_bar_str), color='#FFFFFF'))
		patch_per_row = 4
		legend = fig.legend(handles=patches, loc="upper left", ncol=patch_per_row, frameon=False,
							borderpad=0.0, bbox_to_anchor=(legend_left_margin, next_available_y))
		legend._legend_box.align = "left"
		legend_bbox = legend.get_tightbbox(find_renderer(fig))
		next_available_y = legend_bbox.y0 / fig_height - y_margin * 3.0
		return next_available_y

	@classmethod
	def _add_titles(cls, fig, fig_height, fig_title, left_margin, next_available_y, sub_title, y_margin):
		if fig_title != '' and fig_title is not None:
			text = fig.text(left_margin, next_available_y, fig_title,
							horizontalalignment='left',
							verticalalignment='top',
							fontsize=cls._title_fonts_size)
			text_bbox = text.get_window_extent(find_renderer(fig))
			next_available_y = text_bbox.y0 / fig_height - y_margin
			# print(text_bbox.y0, text_bbox.y1, fig_height, next_available_y)
		if sub_title != '' and sub_title is not None:
			text = fig.text(left_margin, next_available_y, sub_title,
							horizontalalignment='left',
							verticalalignment='top',
							fontsize=cls._sub_title_fonts_size)
			text_bbox = text.get_window_extent(find_renderer(fig))
			next_available_y = text_bbox.y0 / fig_height - y_margin
		return next_available_y

	'''
	Create a overall performence chart with following input
	chart_data = {"Overall":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "10ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "20ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "40ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}}
			  }
	'''

	@classmethod
	def create_cfm_overview_charts(cls, chart_data: Dict, fig_title: str = '<Fig Title>', sub_title='<Sub Title>',
								   show_err_bar: bool = True, error_bar_str: str = 'Err Bar : 95% CI',
								   show_bar_value: bool = False, num_row: int = 3,
								   figure_height: float = 12, figure_width: float = 16, colors: List = None):
		now_col = 2
		fig = MetricsVis._create_cfm_charts(chart_data, fig_title, sub_title,
											show_err_bar, error_bar_str, show_bar_value, num_row,
											now_col, figure_height, figure_width, has_overall=True, colors=colors)

		return fig

	'''
	Create a overall performence chart with following input
	chart_data = {"Overall":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "10ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "20ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "40ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}}
			  }
	'''

	@classmethod
	def create_cfm_charts(cls, chart_data: Dict, fig_title: str = '<Fig Title>', sub_title='<Sub Title>',
						  show_err_bar: bool = True, error_bar_str: str = 'Err Bar : 95% CI',
						  show_bar_value: bool = False, num_row: int = 3,
						  num_col: int = 2, figure_height: float = 12, figure_width: float = 16, colors: List = None):
		# set font
		fig = MetricsVis._create_cfm_charts(chart_data, fig_title, sub_title,
											show_err_bar, error_bar_str, show_bar_value, num_row,
											num_col, figure_height, figure_width, has_overall=False)
		return fig

	'''
	Create a overall performence chart with following input
	chart_data = {'Col Name' : { "10ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "20ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}},
			  "40ev":{'Dice':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}, 'Dot Product':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Precision':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]},'Recall':{'CFM-ID 4':[0.0,0.0],'MSRB':[0.0,0.0]}}
			  }}
	'''

	@classmethod
	def create_cfm_charts_col_major(cls, chart_data: Dict, fig_title: str = '<Fig Title>', sub_title='<Sub Title>',
									show_err_bar: bool = True, error_bar_str: str = 'Err Bar : 95% CI',
									show_bar_value: bool = False, num_row: int = 3,
									num_col: int = 2, figure_height: float = 12, figure_width: float = 16,
									colors: List = None, append_col_name_to_ax=False):
		# print('hello world')
		# config fig
		colors, fig, gs, legend_strs = cls._config_fig(next(iter(chart_data.values())), colors, figure_height,
													   figure_width, num_col, num_row)
		# create subplots by cols
		col_name_offset = 0.1
		for col_idx, col_name in enumerate(chart_data):
			for row_idx, chart_name in enumerate(chart_data[col_name]):
				ax = fig.add_subplot(gs[row_idx, col_idx])
				ax_title = chart_name
				if append_col_name_to_ax:
					ax_title = '{} {}'.format(chart_name, col_name)
				cls._create_subplot(ax, chart_data[col_name][chart_name], colors, ax_title, show_err_bar,
									show_bar_value)

		# apply layout
		fig.tight_layout(w_pad=1.0, h_pad=2.0)

		# add title
		next_available_y = 1.0 - 0.03
		left_margin = 0.03
		y_margin = 0.01
		legend_left_margin = 0.025

		fig_height = (fig.get_size_inches() * fig.dpi)[1]
		next_available_y = cls._add_titles(fig, fig_height, fig_title, left_margin, next_available_y, sub_title,
										   y_margin)

		# add legend
		next_available_y = cls._add_legends(colors, error_bar_str, fig, fig_height, legend_left_margin, legend_strs,
											next_available_y, show_err_bar, y_margin)
		plt.subplots_adjust(top=next_available_y - col_name_offset / num_row)
		return fig

	@classmethod
	def create_cfm_charts_row_major(cls, chart_data: Dict, fig_title: str = '<Fig Title>', sub_title='<Sub Title>',
									show_err_bar: bool = True, error_bar_str: str = 'Err Bar : 95% CI',
									show_bar_value: bool = False, num_row: int = 2,
									num_col: int = 1, figure_height: float = 12, figure_width: float = 16,
									colors: List = None, append_col_name_to_ax=False):
		# print('hello world')
		# config fig
		colors, fig, gs, legend_strs = cls._config_fig(next(iter(chart_data.values())), colors, figure_height,
													   figure_width, num_col, num_row)
		# create subplots by cols
		col_name_offset = 0.1
		for row_idx, row_name in enumerate(chart_data):
			for col_idx, chart_name in enumerate(chart_data[row_name]):
				ax = fig.add_subplot(gs[row_idx, col_idx])
				ax_title = chart_name
				if append_col_name_to_ax:
					ax_title = '{} {}'.format(chart_name, row_name)
				# print(show_bar_value)
				cls._create_subplot(ax, chart_data[row_name][chart_name], colors, ax_title, show_err_bar,
									show_bar_value)
				# ax.invert_yaxis()

		# apply layout
		fig.tight_layout(w_pad=1.0, h_pad=2.0)

		# add title
		next_available_y = 1.0 - 0.03
		left_margin = 0.03
		y_margin = 0.01
		legend_left_margin = 0.025

		fig_height = (fig.get_size_inches() * fig.dpi)[1]
		next_available_y = cls._add_titles(fig, fig_height, fig_title, left_margin, next_available_y, sub_title,
										   y_margin)

		# add legend
		next_available_y = cls._add_legends(colors, error_bar_str, fig, fig_height, legend_left_margin, legend_strs,
											next_available_y, show_err_bar, y_margin)
		plt.subplots_adjust(top=next_available_y - col_name_offset / num_row)
		return fig
